﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using NVL.DataAccess;

namespace NVL.API_R.Helper
{
    public static class ConfigHelper
    {
        public static string GetConnectionString()
        {
#if DEBUG
            return  ConfigurationManager.ConnectionStrings["ConstrDevBOTME"].ConnectionString;
#else
            return ConfigurationManager.ConnectionStrings["ConstrBOTME"].ConnectionString;
#endif
        }
    }
}