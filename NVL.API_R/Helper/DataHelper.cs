﻿using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Web.Script.Serialization;
using NVL.API_R.Models;
using System.Text.RegularExpressions;

namespace NVL.API_R.Helper
{
    public static class DataHelper
    {
        #region Convert DataTable To String
        public static string ConvertDataTableToString(DataTable dt, bool bolIsUpperColumnName = false)
        {
            var result = "";
            var serializer = new JavaScriptSerializer { MaxJsonLength = int.MaxValue };
            var rows = new List<Dictionary<string, object>>();

            foreach (DataRow dr in dt.Rows)
            {
                if (bolIsUpperColumnName)
                {
                    var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName.ToUpper(), col => dr[col]);
                    rows.Add(row);
                }
                else
                {
                    var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => dr[col]);
                    rows.Add(row);
                }
            }

            result = serializer.Serialize(rows);
            return result;
        }

        #endregion Convert DataTable To String

        #region Convert Object To DataTable
        public static DataTable ToDataTable<TEntity>(this IList<TEntity> data)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(TEntity));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                Type t = prop.PropertyType;
                t = Nullable.GetUnderlyingType(t) ?? t;
                table.Columns.Add(prop.Name, t);
            }
            object[] values = new object[props.Count];
            foreach (TEntity item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    var v = props[i].GetValue(item);
                    if (v != null && !Convert.IsDBNull(v))
                        values[i] = v;
                }
                table.Rows.Add(values);
            }
            return table;
        }
        #endregion Convert Object To DataTable

        #region Conver Object to Excel File
        //public static void ToFile<TEntity>(this IList<TEntity> data, string FileName)
        //{
        //    Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
        //    Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);
        //    Microsoft.Office.Interop.Excel._Worksheet worksheet = null;

        //    try
        //    {
        //        PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(TEntity));

        //        worksheet = workbook.ActiveSheet;
        //        worksheet.Name = "ExportFile";

        //        for (int i = 0; i < props.Count; i++)
        //        {
        //            PropertyDescriptor prop = props[i];
        //            worksheet.Cells[1, i + 1] = prop.Name;
        //        }

        //        for (int i = 0; i < data.Count; i++)
        //        {
        //            for (int j = 0; j < props.Count; j++)
        //            {
        //                var v = props[j].GetValue(data[i]);
        //                worksheet.Cells[i + 2, j + 1] = v;
        //            }
        //        }

        //        workbook.SaveAs(FileName);
        //    }
        //    catch (System.Exception ex)
        //    {

        //    }
        //    finally
        //    {
        //        excel.Quit();
        //        workbook = null;
        //        excel = null;

        //    }
        //}
        #endregion

        #region Convert DataTable To List Object
        // Convert DataTable To List Object Function
        public static List<T> DataTableToList<T>(this DataTable table) where T : class, new()
        {
            try
            {
                List<T> list = new List<T>();

                foreach (var row in table.AsEnumerable())
                {
                    var obj = row.ToObject<T>();
                    list.Add(obj);
                }

                return list;
            }
            catch
            {
                return null;
            }
        }

        //SubFunction
        public static T ToObject<T>(this DataRow row) where T : class, new()
        {
            T obj = new T();

            foreach (var prop in obj.GetType().GetProperties())
            {
                try
                {
                    if (prop.PropertyType.IsGenericType && prop.PropertyType.Name.Contains("Nullable"))
                    {
                        if (!string.IsNullOrEmpty(row[prop.Name].ToString()))
                            prop.SetValue(obj, Convert.ChangeType(row[prop.Name],
                            Nullable.GetUnderlyingType(prop.PropertyType), null));
                        //else do nothing
                    }
                    else
                        prop.SetValue(obj, Convert.ChangeType(row[prop.Name], prop.PropertyType), null);
                }
                catch
                {
                    continue;
                }
            }
            return obj;
        }
        #endregion Convert DataTable To Object

        #region Copy Object

        /// <summary>
        /// Copy an object to destination object, only matching fields will be copied
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sourceObject">An object with matching fields of the destination object</param>
        /// <param name="destObject">Destination object, must already be created</param>
        /// 
        public static void CopyObject<T>(object sourceObject, ref T destObject)
        {
            //  If either the source, or destination is null, return
            if (sourceObject == null || destObject == null)
                return;

            //  Get the type of each object
            Type sourceType = sourceObject.GetType();
            Type targetType = destObject.GetType();

            //  Loop through the source properties
            foreach (System.Reflection.PropertyInfo p in sourceType.GetProperties())
            {
                //  Get the matching property in the destination object
                System.Reflection.PropertyInfo targetObj = targetType.GetProperty(p.Name);
                //  If there is none, skip
                if (targetObj == null)
                    continue;

                //  Set the value in the destination
                targetObj.SetValue(destObject, p.GetValue(sourceObject, null), null);
            }
        }
        #endregion

        #region Create MD5 String
        public static string getMd5Hash(string input)
        {
            MD5 md5Hash = MD5.Create();
            // Convert the input string to a byte array and compute the hash.
            byte[] theByteArray = md5Hash.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            foreach (var cha in theByteArray)
            {
                sBuilder.Append(cha.ToString("x2"));
            }
            return sBuilder.ToString(); 
        }
        #endregion Create MD5 String

        #region Respose Message
        public static Response<object> OK()
        {
            return OK(null);
        }
        public static Response<object> OK(object obj)
        {
            Response<object> res = new Response<object>();
            res.payload = obj??"";
            res.success = true;
            res.error = new errorMD()
            {
                code = (int)errCode.Created,
                message = "Done",
                description = Enum.GetName(typeof(errCode), errCode.OK)
            };
            return res;
        }

        public static Response<object> LoadSuccess()
        {
            return LoadSuccess(null);
        }
        public static Response<object> LoadSuccess(object obj)
        {
            Response<object> res = new Response<object>();
            res.payload = obj ?? "";
            res.success = true;
            res.error = new errorMD()
            {
                code = (int)errCode.LoadSuccess,
                message = "OK",
                description = Enum.GetName(typeof(errCode), errCode.LoadSuccess)
            };
            return res;
        }

        public static Response<object> Created()
        {
            return Created(null);
        }
        public static Response<object> Created(object obj)
        {
            Response<object> res = new Response<object>();
            res.payload = obj??"";
            res.success = true;
            res.error = new errorMD() {
                code = (int)errCode.Created,
                message = "CREATED",
                description = Enum.GetName(typeof(errCode), errCode.Created)
            };
            return res;
        }

        public static Response<object> InternalError(Exception ex)
        {
            return InternalError(ex, null);
        }

        public static Response<object> InternalError(string Message)
        {
            return InternalError(null, Message);
        }

        private static Response<object> InternalError(Exception ex, string Message)
        {
            Response<object> res = new Response<object>();
            res.success = false;
            res.payload = "";
            if (Message != null)
            {
                res.error.code = (int)errCode.InternalError;
                res.error.message = Message;
                return res;
            }
            if (ex.Message.Contains("ORA-20001"))
            {
                Regex regex = new Regex(@"(?<=\().*?(?=\))");
                Match match = regex.Match(ex.Message);
                string tmp = "INTERNAL ERROR";
                res.success = false;
                if (match.Success)
                {
                    tmp = match.Value;
                    res.error = new errorMD()
                    {
                        code = (int)errCode.OracleCustomExp,
                        message = tmp,
                        description = Enum.GetName(typeof(errCode), errCode.OracleCustomExp)
                    };
                }
                else
                {
                    res.error = new errorMD()
                    {
                        code = (int)errCode.InternalError,
                        message = ex.Message,
                        description = Enum.GetName(typeof(errCode), errCode.InternalError)
                    };
                }
            }
            else
            {
                res.error = new errorMD()
                {
                    code = (int)errCode.InternalError,
                    message = ex.Message,
                    description = Enum.GetName(typeof(errCode), errCode.InternalError)
                };
            }
            return res;
        }

        public static Response<string> NoDataFound()
        {
            return NoDataFound(null);
        }
        public static Response<string> NoDataFound(string msg)
        {
            Response<string> res = new Response<string>();
            res.payload = "";
            res.success = true;
            res.error = new errorMD() {
                code = (int)errCode.NoRowFound,
                message = msg??"NO DATA FOUND",
                description = Enum.GetName(typeof(errCode), errCode.NoRowFound)
            };
            return res;
        }

        public static Response<string> NotFound()
        {
            return NotFound(null);
        }
        public static Response<string> NotFound(string msg)
        {
            Response<string> res = new Response<string>();
            res.payload = "";
            res.success = false;
            res.error = new errorMD()
            {
                code = (int)errCode.NotFound,
                message = msg ?? "NOT FOUND",
                description = Enum.GetName(typeof(errCode), errCode.NotFound)
            };
            return res;
        }

        public static Response<string> ParameterError()
        {
            return ParameterError(null);
        }
        public static Response<string> ParameterError(string msg)
        {
            Response<string> res = new Response<string>();
            res.payload = "";
            res.success = false;
            res.error = new errorMD()
            {
                code = (int)errCode.ParameterError,
                message = msg ?? "PARAMETER ERROR",
                description = Enum.GetName(typeof(errCode), errCode.ParameterError)
            };
            return res;
        }

        public static Response<string> OracleAppError(string msg)
        {
            Response<string> res = new Response<string>();
            res.payload = "";
            res.success = false;
            res.error = new errorMD()
            {
                code = (int)errCode.OracleCustomExp,
                message = msg,
                description = Enum.GetName(typeof(errCode), errCode.OracleCustomExp)
            };
            return res;
        }

        #endregion Respose Message
    }
}