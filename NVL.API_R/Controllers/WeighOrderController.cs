﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NVL.API_R.Base;
using NVL.API_R.Models;
using NVL.API_R.Helper;
using Oracle.ManagedDataAccess.Client;
using System.Text.RegularExpressions;

namespace NVL.API_R.Controllers
{
    [RoutePrefix("api/weighorder")]
    public class WeighOrderController : BaseController
    {
        /// <summary>
        /// TẠO LỆNH CÂN
        /// </summary>

        #region 1 - GET BATCH STATUS BY DATE - Tạo Lệnh Cân Module
        [Route("get-batch-status-by-date")]
        [HttpGet]
        public HttpResponseMessage Get_BATCH_STATUS_BY_DATE(HttpRequestMessage request, uint ORGANIZATION_ID, string PROCESS_CODE, DateTime FROM_DATE, DateTime TO_DATE)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_ORGANIZATION_ID", ORGANIZATION_ID);
                parameters.Add("P_PROCESS_CODE", PROCESS_CODE);
                parameters.Add("P_FROM_DATE", FROM_DATE);
                parameters.Add("P_TO_DATE", TO_DATE);
                DataSet ds = ExecSPToListOracle("PR_C1_GET_BA_STATUS_BY_DATE", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<BATCH_STATUS> Received = DataHelper.DataTableToList<BATCH_STATUS>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound());
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion Tạo Lệnh Cân

        #region 2 - GET BATCH STATUS BY BATCH CODE - Tạo Lệnh Cân Module
        [Route("get-batch-status-by-code")]
        [HttpGet]
        public HttpResponseMessage Get_BATCH_STATUS_BY_CODE(HttpRequestMessage request, int ORGANIZATION_ID, string PROCESS_CODE, string BATCH_CODE)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_ORGANIZATION_ID", ORGANIZATION_ID);
                parameters.Add("P_PROCESS_CODE", PROCESS_CODE);
                parameters.Add("P_BATCH_CODE", BATCH_CODE);
                DataSet ds = ExecSPToListOracle("PR_C1_GET_BA_STATUS_BY_CODE", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<BATCH_STATUS> Received = DataHelper.DataTableToList<BATCH_STATUS>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion Tạo Lệnh Cân

        #region 3 - UPDATE BATCH STATUS BY BATCH CODE - Tạo Lệnh Cân Module
        [Route("update-batch-status-by-code")]
        [HttpPost]
        public HttpResponseMessage UPDATE_BATCH_STATUS_BY_CODE(HttpRequestMessage request, BATCH_STATUS obj)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_ORGANIZATION_ID", obj.ORGANIZATION_ID);
                parameters.Add("P_PROCESS_CODE", obj.PROCESS_CODE);
                parameters.Add("P_BATCH_CODE", obj.BATCH_CODE);
                parameters.Add("P_STATUS", obj.STATUS ? 1 : 0);
                parameters.Add("P_CREATE_BY", obj.CREATE_BY);
                DataSet ds = ExecSPToListOracle("PR_C1_UPDATE_BA_STATUS_BY_CODE", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<BATCH_STATUS> Received = DataHelper.DataTableToList<BATCH_STATUS>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.Created(Received.FirstOrDefault()));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound());
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion Tạo Lệnh Cân

        #region 4 - GET NUMBER OF WOs INFO OF BATCH - Tạo Lệnh Cân Module
        [Route("get-vertical-weigh-limit")]
        [HttpGet]
        public HttpResponseMessage Get_Vertical_Weigh_Limit(HttpRequestMessage request, int BATCH_ID, string BATCH_CODE, string PROCESS_CODE)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_BATCH_ID", BATCH_ID);
                parameters.Add("P_BATCH_CODE", BATCH_CODE);
                parameters.Add("P_PROCESS_CODE", PROCESS_CODE);

                DataSet ds = ExecSPToListOracle("PR_C1_GET_VER_WEIGH_LIMIT", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<VERTICAL_WEIGH_LIMIT> Received = DataHelper.DataTableToList<VERTICAL_WEIGH_LIMIT>(ds.Tables[0]);
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received.FirstOrDefault()));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound());
                }
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }

        #endregion Tạo Lệnh Cân - Lấy Thông Tin Giới Hạn Cân Nganh của BatchID

        #region 5 - UPDATE NUMBER OF WOs VERTCIAL WEIGHING - Tạo Lệnh Cân Module
        [Route("create-vertical-weigh-limit")]
        [HttpPost]
        public HttpResponseMessage Create_Vertical_Weigh_Limit(HttpRequestMessage request, VERTICAL_WEIGH_LIMIT obj)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_BATCH_ID", obj.BATCH_ID);
                parameters.Add("P_BATCH_CODE", obj.BATCH_CODE);
                parameters.Add("P_PROCESS_CODE", obj.PROCESS_CODE);
                parameters.Add("P_VERTICAL_LIMIT", obj.VER_WEIGH_LIMIT);
                parameters.Add("P_USERNAME", obj.USERNAME);

                DataSet ds = ExecSPToListOracle("PR_C1_CREATE_VER_WEIGH_LIMIT", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<VERTICAL_WEIGH_LIMIT> Received = DataHelper.DataTableToList<VERTICAL_WEIGH_LIMIT>(ds.Tables[0]);
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.Created(Received.FirstOrDefault()));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound());
                }
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion Tạo Lệnh Cân - Tạo Mới Giới Hạn Cân Nganh cho BatchID

        #region 6 - GET TO CREATE BATCH LIST BY ORG ID - Tạo Lệnh Cân Module
        [Route("get-to-create-batch-list-by-orgid")]
        [HttpGet]
        public HttpResponseMessage Get_To_Create_WO_Batch_List_By_OrgID(HttpRequestMessage request, string ORGANIZATION_ID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_ORGANIZATION_ID", ORGANIZATION_ID);
                DataSet ds = ExecSPToListOracle("PR_C1_GET_CRE_WO_BA_LST", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<BATCH_INFO> Received = DataHelper.DataTableToList<BATCH_INFO>(ds.Tables[0]);
                    #region LinQ statement
                    var tmp = Received
                            .GroupBy(i => new
                            {
                                batch_id = i.BATCH_ID,
                                batch_code = i.BATCH_CODE,
                                product_name = i.PRODUCT_NAME,
                                plan_start_date = i.PLAN_START_DATE,
                                khsx = i.KHSX
                            })
                            .Select(p => new
                            {
                                BATCH_ID = p.Key.batch_id,
                                BATCH_CODE = p.Key.batch_code,
                                PRODUCT_NAME = p.Key.product_name,
                                KHSX_DATE = p.Key.plan_start_date,
                                KHSX = p.Key.khsx,
                                VOLUME_OF_A_BATCH = p.Select(c => c.QTY).Sum(),
                                RECIPE = p.GroupBy(i => new
                                {
                                    recipe_vers = i.RECIPE_VERS,
                                    recipe_no = i.RECIPE_NO
                                })
                                .Select(rp => new
                                {
                                    VERSION = rp.Key.recipe_vers,
                                    RECIPE_CODE = rp.Key.recipe_no
                                }).First(),
                                FORMULA = p.GroupBy(i => new
                                {
                                    formula_vers = i.FORMULA_VERS,
                                    formula_no = i.FORMULA_NO,
                                    creation_date = i.CREATION_DATE
                                })
                                .Select(fp => new
                                {
                                    VERSION = fp.Key.formula_vers,
                                    CODE = fp.Key.formula_no,
                                    VALIDATION_DATE = fp.Key.creation_date
                                }).First(),
                                INGREDIENTS = p.GroupBy(i => new
                                {
                                    line_no = i.LINE_NO,
                                    inventory_item_id = i.INVENTORY_ITEM_ID,
                                    master_code = i.MASTER_CODE,
                                    item_name = i.ITEM_NAME,
                                    qty = i.QTY,

                                    subInventoryItemId = i.SUB_INVENTORY_ITEM_ID,
                                    subMasterCode = i.SUB_MASTER_CODE,
                                    subItemName = i.SUB_ITEM_NAME,
                                    subQty = i.SUB_QTY,

                                    uom = i.UOM,
                                    subStartDate = i.SUB_START_DATE,
                                    subEndDate = i.SUB_END_DATE
                                })
                                .Select(ip => new
                                {
                                    LINE_NO = ip.Key.line_no,
                                    INVENTORY_ITEM_ID = ip.Key.inventory_item_id,
                                    MASTER_CODE = ip.Key.master_code,
                                    ITEM_NAME = ip.Key.item_name,
                                    QTY = ip.Key.qty,

                                    SUB_INVENTORY_ITEM_ID = ip.Key.subInventoryItemId,
                                    SUB_MASTER_CODE = ip.Key.subMasterCode,
                                    SUB_ITEM_NAME = ip.Key.subItemName,
                                    SUB_QTY = ip.Key.subQty,
                                    UOM = ip.Key.uom,
                                    START_DATE_SUB = ip.Key.subStartDate,
                                    END_DATE_SUB = ip.Key.subEndDate
                                }).ToList()
                            }).ToList();
                    #endregion
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.Created(tmp));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound());
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion Tạo Lệnh Cân - Lấy Thông Tin Batch để tạo lệnh cân

        #region 7 - GET TO CREATE WO BATCH INFO BY BATCH ID - Tạo Lệnh Cân Module
        [Route("get-to-create-wo-batch-info-by-id")]
        [HttpGet]
        public HttpResponseMessage Get_To_Create_Batch_Info_by_Id(HttpRequestMessage request, string BATCH_ID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_BATCH_ID", BATCH_ID);
                DataSet ds = ExecSPToListOracle("PR_C1_GET_CREATE_WO_BY_BA_ID", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<WO_DATA> Received = DataHelper.DataTableToList<WO_DATA>(ds.Tables[0]);
                    #region LinQ statement
                    var tmp = Received.GroupBy(i => new {
                        orderID = i.ORDER_ID,
                        processCode = i.PROCESS_CODE,
                        batchId = i.BATCH_ID,
                        batchCode = i.BATCH_CODE,
                        batchesQty = i.BATCHES_QTY,
                        deviceType = i.DEVICE_TYPE,
                        status = i.STATUS,
                        orderGroup = i.ORDER_GROUP
                    })
                                .Select((p , o) => new
                                {
                                    LINE_NO = o + 1,
                                    ORDER_ID = p.Key.orderID,
                                    PROCESS_CODE = p.Key.processCode,
                                    BATCH_ID = p.Key.batchId,
                                    BATCH_CODE = p.Key.batchCode,
                                    BATCHES_QTY = p.Key.batchesQty,
                                    DEVICE_TYPE = p.Key.deviceType,
                                    STATUS = p.Key.status,
                                    ORDER_GROUP = p.Key.orderGroup,
                                    WEIGH_CONTENT = p.GroupBy(i => new {
                                        orderDetailId = i.ORDER_DETAIL_ID,
                                        orderId = i.ORDER_ID,
                                        batch = i.BATCH_ID,
                                        inventoryItemId = i.INVENTORY_ITEM_ID,
                                        itemName = i.ITEM_NAME,
                                        masterCode = i.MASTER_CODE,
                                        stWeigh = i.ST_WEIGH,
                                        packedQty = i.PACKAGE_QTY,
                                        totalWeigh = i.TOTAL_WEIGH,
                                        status = i.STATUS_DETAIL,
                                        subStartDate = i.SUB_START_DATE,
                                        subEndDate = i.SUB_END_DATE,
                                        subItemId = i.SUB_INVENTORY_ITEM_ID,
                                        subMasterCode = i.SUB_MASTER_CODE,
                                        subItemName = i.SUB_ITEM_NAME,
                                        subManualDescription = i.SUB_MANUAL_DESCRIPTION,
                                        subManualMode = i.SUB_MANUAL_MODE,
                                        co = i.CO
                                    }).Select(m => new
                                    {
                                        ORDER_DETAIL_ID = m.Key.orderDetailId,
                                        ORDER_ID = m.Key.orderId,
                                        BATCH_ID = m.Key.batch,
                                        INVENTORY_ITEM_ID = m.Key.inventoryItemId,
                                        ITEM_NAME = m.Key.itemName,
                                        MASTER_CODE = m.Key.masterCode,
                                        ST_WEIGH = m.Key.stWeigh,
                                        PACKAGE_QTY = m.Key.packedQty,
                                        TOTAL_WEIGH = m.Key.totalWeigh,
                                        STATUS = m.Key.status,
                                        SUB_START_DATE = m.Key.subStartDate,
                                        SUB_END_DATE = m.Key.subEndDate,
                                        SUB_INVENTORY_ITEM_ID = m.Key.subItemId,
                                        SUB_MASTER_CODE = m.Key.subMasterCode,
                                        SUB_ITEM_NAME = m.Key.subItemName,
                                        SUB_MANUAL_DESCRIPTION = m.Key.subManualDescription,
                                        SUB_MANUAL_MODE = m.Key.subManualMode,
                                        PACKED_QTY = m.Key.co,
                                        PACKAGE_LIST = m.GroupBy(i => new {
                                            tareWeigh = i.TARE_WEIGH,
                                            netWeigh = i.NET_WEIGH,
                                            itemSTWeigh = i.ITEM_ST_WEIGH,
                                            refBarCodeId = i.REF_BARCODE_ID,
                                            refTransactionId = i.REF_TRANSACTION_ID,
                                            barcodeId = i.BARCODE_ID,
                                            secondLot = i.SECOND_LOT,
                                            statusItemDetail = i.STATUS_ITEM_DETAIL
                                        }).Select(n => new
                                        {
                                            TARE_WEIGH = n.Key.tareWeigh,
                                            NET_WEIGH = n.Key.netWeigh,
                                            ITEM_ST_WEIGH = n.Key.itemSTWeigh,
                                            REF_BARCODE_ID = n.Key.refBarCodeId,
                                            REF_TRANSACTION_ID = n.Key.refTransactionId,
                                            BARCODE_ID = n.Key.barcodeId,
                                            SECOND_LOT = n.Key.secondLot,
                                            STATUS_ITEM_DETAIL = n.Key.statusItemDetail
                                        }).ToList()
                                    }).OrderBy(c => c.ORDER_DETAIL_ID).ToList()
                                }).ToList();
                    #endregion
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.Created(tmp));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion Tạo Lệnh Cân

        #region 8 - CREATE NEW WO -Tạo Lệnh Cân Moule
        [Route("create-wo")]
        [HttpPost]
        public HttpResponseMessage Create_WO(HttpRequestMessage request, [FromBody] WO_CREATION obj)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_BATCH_ID", obj.BATCH_ID);
                parameters.Add("P_BATCH_CODE", obj.BATCH_CODE);
                parameters.Add("P_BATCHES_QTY", obj.BATCHES_QTY);
                parameters.Add("P_DEVICE_TYPE", obj.DEVICE_TYPE);
                parameters.Add("P_CREATE_BY", obj.CREATE_BY);
                parameters.Add("P_PROCESS_CODE", obj.PROCESS_CODE);
                parameters.Add("P_ORDER_QTY", obj.ORDER_QTY);
                parameters.Add("P_ORGANIZATION_ID", obj.ORGANIZATION_ID);

                //DataSet ds = ExecSPToListOracle("PR_C1_INSERT_NEW_WO", parameters);
                DataSet ds = ExecSPToListOracle("PR_C1_CREATE_NEW_WO", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<WO_DATA> Received = DataHelper.DataTableToList<WO_DATA>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.Created(Received));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound());
                }

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion  Tạo Lệnh Cân

        #region 9 - DISPOSAL WEIGH ORDER - Tạo Lệnh Cân Module
        [Route("disposal-ava-wo")]
        [HttpPost]
        public HttpResponseMessage Remove_Available_WO(HttpRequestMessage request, WEIGH_ORDER obj)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_BATCH_ID", obj.BATCH_ID);
                parameters.Add("P_ORDER_ID", obj.ORDER_ID);

                ExecuteNonQueryOracle("PR_C1_DISPOSAL_WO_BY_ID", parameters);
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.Created("Đã xóa"));
            }
            catch (Exception ex)
            {

                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion Tạo Lệnh Cân - Xóa WO đã tạo 

        #region 10 - GET LIST MIXING MACHINE - Tạo Lệnh Cân Module
        [Route("get-mixing-machine-lst")]
        [HttpGet]
        public HttpResponseMessage Get_Mixing_Machine_List(HttpRequestMessage request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                DataSet ds = ExecSPToListOracle("PR_C1_GET_MIX_MCH_LST");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var tmp = DataHelper.DataTableToList<MIXING>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(tmp));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound("Không tìm thấy máy trộn!"));
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 11 - UPDATE MANUAL SUBSTITUTION
        [Route("update-manual-substitution")]
        [HttpPost]
        public HttpResponseMessage Update_Manual_Substitution(HttpRequestMessage request, MANUAL_SUBS obj)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_WOD", obj.WOD);
                parameters.Add("P_SUB_INVENTORY_ITEM_ID", obj.SUB_INVENTORY_ITEM_ID);
                parameters.Add("P_SUB_MANUAL_DESCRIPTION", obj.SUB_MANUAL_DESCRIPTION);
                parameters.Add("P_SUB_MANUAL_MODE", obj.SUB_MANUAL_MODE);
                parameters.Add("P_UPDATE_BY", obj.UPDATE_BY);

                ExecuteNonQueryOracle("PR_C1_UPDATE_SUB_MAN_INF", parameters);
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.OK());
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 12 - GET ITEM INFO BY MASTER CODE AND ORG ID - Tạo Lệnh Cân Module
        [Route("get-item-inf-by-code-and-org-id")]
        [HttpGet]
        public HttpResponseMessage Get_Item_Info_By_Code_And_Org_ID(HttpRequestMessage request, string MASTER_CODE, uint ORGANIZATION_ID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_MASTER_CODE", MASTER_CODE);
                parameters.Add("P_ORGANIZATION_ID", ORGANIZATION_ID);

                DataSet ds = ExecSPToListOracle("PR_GET_ITM_BY_CODE_AND_ORG", parameters);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var tmp = DataHelper.DataTableToList<ITEM_INFO>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(tmp.FirstOrDefault()));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound());
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        /// <summary>
        /// CÂN CHIA
        /// </summary>
        #region 1 - GET READY TO WEIGH BATCHs LIST BY ORG ID - Cân Chia NL Module
        [Route("get-rdy-wgh-batch-lst")]
        [HttpGet]
        public HttpResponseMessage Get_Ready_To_Weigh_Batch_List(HttpRequestMessage request, string ORGANIZATION_ID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_ORGANIZATION_ID", ORGANIZATION_ID);
                DataSet ds = ExecSPToListOracle("PR_C1_GET_RDY_WEIGH_BA_LST", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<BATCH_INFO> Received = DataHelper.DataTableToList<BATCH_INFO>(ds.Tables[0]);
                    #region LinQ statement
                    var tmp = Received
                            .GroupBy(i => new
                            {
                                batchId = i.BATCH_ID,
                                batchCode = i.BATCH_CODE,
                                productName = i.PRODUCT_NAME,
                                planStartDate = i.PLAN_START_DATE,
                                khsx = i.KHSX,
                            })
                            .Select((p, o) => new
                            {
                                STT = o + 1,
                                BATCH_ID = p.Key.batchId,
                                BATCH_CODE = p.Key.batchCode,
                                PRODUCT_NAME = p.Key.productName,
                                KHSX_DATE = p.Key.planStartDate,
                                KHSX = p.Key.khsx,

                                VOLUME_OF_A_BATCH = p.Select(c => c.QTY).Sum(),

                                RECIPE = p.GroupBy(i => new
                                {
                                    recipe_vers = i.RECIPE_VERS,
                                    recipe_no = i.RECIPE_NO
                                })
                                .Select(rp => new
                                {
                                    VERSION = rp.Key.recipe_vers,
                                    RECIPE_CODE = rp.Key.recipe_no
                                }).First(),
                                FORMULA = p.GroupBy(i => new
                                {
                                    formula_vers = i.FORMULA_VERS,
                                    formula_no = i.FORMULA_NO,
                                    creation_date = i.CREATION_DATE
                                })
                                .Select(fp => new
                                {
                                    VERSION = fp.Key.formula_vers,
                                    CODE = fp.Key.formula_no,
                                    VALIDATION_DATE = fp.Key.creation_date
                                }).First(),
                                INGREDIENTS = p.GroupBy(i => new
                                {
                                    line_no = i.LINE_NO,

                                    inventory_item_id = i.INVENTORY_ITEM_ID,
                                    master_code = i.MASTER_CODE,
                                    item_name = i.ITEM_NAME,
                                    qty = i.QTY,

                                    subInventoryItemId = i.SUB_INVENTORY_ITEM_ID,
                                    subMasterCode = i.SUB_MASTER_CODE,
                                    subItemName = i.SUB_ITEM_NAME,
                                    subQty = i.SUB_QTY,
                                    subStartDate = i.SUB_START_DATE,
                                    subEndDate = i.SUB_END_DATE,
                                    uom = i.UOM
                                })
                                .Select(ip => new
                                {
                                    LINE_NO = ip.Key.line_no,
                                    INVENTORY_ITEM_ID = ip.Key.inventory_item_id,
                                    MASTER_CODE = ip.Key.master_code,
                                    ITEM_NAME = ip.Key.item_name,
                                    QTY = ip.Key.qty,

                                    SUB_INVENTORY_ITEM_ID = ip.Key.subInventoryItemId,
                                    SUB_MASTER_CODE = ip.Key.subMasterCode,
                                    SUB_ITEM_NAME = ip.Key.subItemName,
                                    SUB_QTY = ip.Key.subQty,
                                    SUB_START_DATE = ip.Key.subStartDate,
                                    SUB_END_DATE = ip.Key.subEndDate,

                                    UOM = ip.Key.uom,
                                    VOLUME_OF_BATCH = ip.Sum(s=>s.QTY)
                                }).ToList()
                            }).ToList();
                    #endregion
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(tmp));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound(""));
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion Cân Chia NL - Lấy Danh Sách các Batch có thể cân

        #region 2 - GET WOs LIST BY BATCHID and IP - Cân Chia NL Module
        [Route("get-wo-by-batchid-and-ip")]
        [HttpGet]
        public HttpResponseMessage Get_WO_BatchID_And_IP(HttpRequestMessage request, string batch_id)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_BATCH_ID", batch_id);
                parameters.Add("P_IP_ADDRESS", request.Headers.Contains("ipaddress") ?
                                request.Headers.GetValues("ipaddress").FirstOrDefault() : clientAddress);
                DataSet ds = ExecSPToListOracle("PR_C1_GET_WO_BY_BA_ID_AND_IP", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<WO_DATA> Received = DataHelper.DataTableToList<WO_DATA>(ds.Tables[0]);
                    #region LinQ statement
                    var tmp = Received.GroupBy(i => new {
                                orderID = i.ORDER_ID,
                                processCode = i.PROCESS_CODE,
                                batchId = i.BATCH_ID,
                                batchCode = i.BATCH_CODE,
                                batchesQty = i.BATCHES_QTY,
                                deviceType = i.DEVICE_TYPE,
                                status = i.STATUS,
                                orderGroup = i.ORDER_GROUP
                                })
                                .Select(p => new
                                {
                                    ORDER_ID = p.Key.orderID,
                                    PROCESS_CODE = p.Key.processCode,
                                    BATCH_ID = p.Key.batchId,
                                    BATCH_CODE = p.Key.batchCode,
                                    BATCHES_QTY = p.Key.batchesQty,
                                    DEVICE_TYPE = p.Key.deviceType,
                                    STATUS = p.Key.status,
                                    ORDER_GROUP = p.Key.orderGroup,
                                    WEIGH_CONTENT = p.GroupBy(i => new {
                                        orderDetailId = i.ORDER_DETAIL_ID,
                                        orderId = i.ORDER_ID,
                                        batch = i.BATCH_ID,
                                        inventoryItemId = i.INVENTORY_ITEM_ID,
                                        itemName = i.ITEM_NAME,
                                        masterCode = i.MASTER_CODE,
                                        stWeigh = i.ST_WEIGH,
                                        packedQty = i.PACKAGE_QTY,
                                        totalWeigh = i.TOTAL_WEIGH,
                                        status = i.STATUS_DETAIL,
                                        subItemId = i.SUB_INVENTORY_ITEM_ID,
                                        subMasterCode = i.SUB_MASTER_CODE,
                                        subItemName = i.SUB_ITEM_NAME,
                                        subStartDate = i.SUB_START_DATE,
                                        subEndDate = i.SUB_END_DATE,
                                        subDescription = i.SUB_DESCRIPTION,
                                        subManualDescription = i.SUB_MANUAL_DESCRIPTION,
                                        co = i.CO
                                    }).Select((m,co) => new
                                    {
                                        LINE_NO = co + 1,
                                        ORDER_DETAIL_ID = m.Key.orderDetailId,
                                        ORDER_ID = m.Key.orderId,
                                        BATCH_ID = m.Key.batch,
                                        INVENTORY_ITEM_ID = m.Key.inventoryItemId,
                                        ITEM_NAME = m.Key.itemName,
                                        MASTER_CODE = m.Key.masterCode,
                                        ST_WEIGH = m.Key.stWeigh,
                                        PACKAGE_QTY = m.Key.packedQty,
                                        TOTAL_WEIGH = m.Key.totalWeigh,
                                        STATUS = m.Key.status,
                                        SUB_INVENTORY_ITEM_ID = m.Key.subItemId,
                                        SUB_MASTER_CODE = m.Key.subMasterCode,
                                        SUB_ITEM_NAME = m.Key.subItemName,
                                        SUB_START_DATE = m.Key.subStartDate,
                                        SUB_END_DATE = m.Key.subEndDate,
                                        SUB_DESCRIPTION = m.Key.subDescription,
                                        SUB_MANUAL_DESCRIPTION = m.Key.subManualDescription,
                                        PACKED_QTY = m.Key.co,
                                        PACKAGE_LIST = m.GroupBy(i => new {
                                            tareWeigh = i.TARE_WEIGH,
                                            netWeigh = i.NET_WEIGH,
                                            itemSTWeigh = i.ITEM_ST_WEIGH,
                                            refBarCodeId = i.REF_BARCODE_ID,
                                            refTransactionId = i.REF_TRANSACTION_ID,
                                            barcodeId = i.BARCODE_ID,
                                            secondLot = i.SECOND_LOT,
                                            statusItemDetail = i.STATUS_ITEM_DETAIL
                                        }).Select(n => new
                                        {
                                            TARE_WEIGH = n.Key.tareWeigh,
                                            NET_WEIGH = n.Key.netWeigh,
                                            ST_WEIGH = n.Key.itemSTWeigh,
                                            REF_BARCODE_ID = n.Key.refBarCodeId,
                                            REF_TRANSACTION_ID = n.Key.refTransactionId,
                                            BARCODE_ID = n.Key.barcodeId,
                                            SECOND_LOT = n.Key.secondLot,
                                            STATUS_ITEM_DETAIL = n.Key.statusItemDetail
                                        }).Where(x => x.BARCODE_ID != 0 && x.STATUS_ITEM_DETAIL != 0).ToList()
                                    }).OrderBy(c => c.ORDER_DETAIL_ID).ToList()
                                }).ToList();
                    #endregion
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(tmp));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion 

        #region 3 - CREATE GROUP WOs - Cân Chia NL Module
        [Route("create-weigh-order-group")]
        [HttpPost]
        public HttpResponseMessage Create_WO_Group(HttpRequestMessage request, [FromBody]WEIGH_ORDER_GROUP obj)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_BATCH_ID", obj.BATCH_ID);
                parameters.Add("P_ORDER_COUNT", obj.ORDER_IDs.Count);
                parameters.Add("P_ORDER_LIST", string.Join(",", obj.ORDER_IDs.Select(x => x.ToString()).ToArray()));
                parameters.Add("P_LOCATION_ID", obj.LOCATION_ID);
                parameters.Add("P_IP_ADDRESS", request.Headers.Contains("ipaddress") ?
                                request.Headers.GetValues("ipaddress").FirstOrDefault() : clientAddress);

                DataSet ds = ExecSPToListOracle("PR_C1_CRE_GRP_WO", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    var Received = DataHelper.DataTableToList<WEIGH_ORDER_GROUP_DATA>(ds.Tables[0]);
                    var tmp = Received.GroupBy(a => new
                    {
                        batchId = a.BATCH_ID,
                        orderGroup = a.ORDER_GROUP,
                        locationId = a.LOCATION_ID
                    }).Select(o => new
                    {
                        BATCH_ID = o.Key.batchId,
                        ORDER_GROUP = o.Key.orderGroup,
                        LOCATION_ID = o.Key.locationId,
                        ORDER_IDs = o.Select(a => a.ORDER_ID).ToArray()
                    }).ToList().FirstOrDefault();


                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK(tmp));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound());
                }
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 4 - DISPOSAL WO GROUP - Tạo Lệnh Cân Module
        [Route("disposal-wo-grp")]
        [HttpPost]
        public HttpResponseMessage Remove_WO_Group(HttpRequestMessage request, [FromBody]WEIGH_ORDER_GROUP obj)
        {
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_BATCH_ID", obj.BATCH_ID);
                parameters.Add("P_ORDER_GROUP", obj.ORDER_GROUP);
                parameters.Add("P_ORDER_COUNT", obj.ORDER_IDs.Count);
                parameters.Add("P_ORDER_LIST", string.Join(",", obj.ORDER_IDs.Select(x => x.ToString()).ToArray()));
                //parameters.Add("P_LOCATION_ID", obj.LOCATION_ID);

                ExecuteNonQueryOracle("PR_C1_DISPOSAL_GRP_WO", parameters);

                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK("Đã xóa"));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 5 - CREATE DETAIL ORDER PACKAGE - Cân Chia NL
        [Route("create-weigh-package")]
        [HttpPost]
        public HttpResponseMessage Create_Weigh_Package(HttpRequestMessage request, WEIGH_ORDER_ITEM_DETAIL obj)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();

                parameters.Add("P_ORDER_ID", obj.ORDER_ID);
                parameters.Add("P_ORDER_DETAIL_ID", obj.ORDER_DETAIL_ID);
                parameters.Add("P_INVENTORY_ITEM_ID", obj.INVENTORY_ITEM_ID);
                parameters.Add("P_ST_WEIGH", obj.ST_WEIGH);
                parameters.Add("P_TARE_WEIGH", obj.TARE_WEIGH);
                parameters.Add("P_NET_WEIGH", obj.NET_WEIGH);
                parameters.Add("P_BARCODE_ID", obj.BARCODE_ID);
                parameters.Add("P_SECOND_LOT", obj.SECOND_LOT ? 1 : 0);
                parameters.Add("P_REF_BARCODE_ID", obj.REF_BARCODE_ID);
                parameters.Add("P_REF_TRANSACTION_ID", obj.REF_TRANSACTION_ID);
                parameters.Add("P_CREATE_BY", obj.CREATE_BY);

                DataSet ds = ExecSPToListOracle("PR_C1_CRE_WEI_RCD", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<WEIGH_ORDER_ITEM_DETAIL> Received = DataHelper.DataTableToList<WEIGH_ORDER_ITEM_DETAIL>(ds.Tables[0]);

                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.Created(Received.FirstOrDefault()));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound());
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }

        [Route("create-weigh-pack")]
        [HttpPost]
        public HttpResponseMessage Create_Weigh_Pack(HttpRequestMessage request, WEIGH_ORDER_ITEM_DETAIL obj)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();

                parameters.Add("P_ORDER_ID", obj.ORDER_ID);
                parameters.Add("P_ORDER_DETAIL_ID", obj.ORDER_DETAIL_ID);
                parameters.Add("P_INVENTORY_ITEM_ID", obj.INVENTORY_ITEM_ID);
                parameters.Add("P_ST_WEIGH", obj.ST_WEIGH);
                parameters.Add("P_TARE_WEIGH", obj.TARE_WEIGH);
                parameters.Add("P_NET_WEIGH", obj.NET_WEIGH);
                parameters.Add("P_BARCODE_ID", obj.BARCODE_ID);
                parameters.Add("P_SECOND_LOT", obj.SECOND_LOT ? 1 : 0);
                parameters.Add("P_REF_BARCODE_ID", obj.REF_BARCODE_ID);
                parameters.Add("P_REF_TRANSACTION_ID", obj.REF_TRANSACTION_ID);
                parameters.Add("P_CREATE_BY", obj.CREATE_BY);

                DataSet ds = ExecSPToListOracle("PR_C1_CRE_WEI_RCD", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<WEIGH_ORDER_ITEM_DETAIL> Received = DataHelper.DataTableToList<WEIGH_ORDER_ITEM_DETAIL>(ds.Tables[0]);

                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.Created(Received.FirstOrDefault()));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound());
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 6 - DISPOSAL WEIGHED ITEM - Cân Chia NL
        [Route("disposal-weighed-item")]
        [HttpPost]
        public HttpResponseMessage Disposal_Weighed_item(HttpRequestMessage request, BARCODE obj)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_BARCODE_ID", obj.BARCODE_ID);
                parameters.Add("P_PROCESS_CODE", obj.PROCESS_CODE);
                parameters.Add("P_USERNAME", obj.USERNAME);

                DataSet ds = ExecSPToListOracle("PR_C1_DISPOSAL_WEIGHED_ITEM", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<BARCODE> Received = DataHelper.DataTableToList<BARCODE>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.Created(Received.FirstOrDefault()));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound());
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion Tạo Lệnh Cân

        #region 7 - CHECK BARCODE ID WITH INVENTORY_ITEM_ID
        [Route("check-bm-brc-itm")]
        [HttpGet]
        public HttpResponseMessage Check_BM_Barcode_Item(HttpRequestMessage request, string PROCESS_CODE,  UInt32 BARCODE_ID,
                                                                                     UInt32 WOD_ID,
                                                                                     UInt32 REQUIRED_CHECK,
                                                                                     decimal REQUIRED_QUANTITY)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_PROCESS_CODE", PROCESS_CODE);
                parameters.Add("P_BARCODE_ID", BARCODE_ID);
                parameters.Add("P_WOD_ID", WOD_ID);
                parameters.Add("P_REQUIRED_CHECK", REQUIRED_CHECK);
                parameters.Add("P_REQUIRED_QUANTITY", REQUIRED_QUANTITY);

                DataSet ds = ExecSPToListOracle("PR_C1_CHK_BM_BAR_ITM", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<BARCODE_ITEM_DATA> Received = DataHelper.DataTableToList<BARCODE_ITEM_DATA>(ds.Tables[0]);
                    var tmp = Received.FirstOrDefault();
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.OK(tmp));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound("Không tìm thấy"));
                }
            }
            catch (OracleException ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 00 - Cân Chia NL - Kiểm tra thông tin BARCODE Bột Mẹ  V1
        //[Route("check-barcode-bmnl")]
        //[HttpPost]
        //public HttpResponseMessage Check_Barcode_BMNL(HttpRequestMessage request, BARCODE obj)
        //{
        //    HttpResponseMessage response = new HttpResponseMessage();
        //    try
        //    {
        //        Dictionary<string, object> parameters = new Dictionary<string, object>();
        //        parameters.Add("P_BARCODE", obj.BARCODE_ID);
        //        parameters.Add("P_PROCESS_CODE", obj.PROCESS_CODE);
        //        parameters.Add("P_ORGANIZATION_ID", 169);
        //        parameters.Add("P_INVENTORY_ITEM_ID", obj.INVENTORY_ITEM_ID);
        //        DataSet ds = ExecSPToListOracle("PR_CHECK_BARCOD_BMNL", parameters);

        //        if (ds.Tables[0].Rows.Count > 0)
        //        {

        //            response = request.CreateResponse(HttpStatusCode.OK, DataHelper.OK(""));
        //        }
        //        else
        //        {
        //            response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound());

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
        //    }
        //    return response;
        //}
        #endregion Cân Chia NL - Kiểm tra thông tin BARCODE Bột Mẹ

        #region 00 - UPDATE LOCATION FOR GROUP WOs - Cân Chia NL 
        //[Route("update-grouped-order-location")]
        //[HttpPost]
        //public HttpResponseMessage Update_Grouped_Order_Location(HttpRequestMessage request, [FromBody]WEIGH_ORDER_GROUP obj)
        //{
        //    HttpResponseMessage response = new HttpResponseMessage();
        //    try
        //    {
        //        Dictionary<string, object> parameters = new Dictionary<string, object>();
        //        parameters.Add("P_BATCH_ID", obj.BATCH_ID);
        //        parameters.Add("P_ORDER_GROUP", obj.ORDER_GROUP);
        //        parameters.Add("P_ORDER_COUNT", obj.ORDER_IDs.Count);
        //        parameters.Add("P_ORDER_LIST", string.Join(",", obj.ORDER_IDs.Select(x => x.ToString()).ToArray()));
        //        parameters.Add("P_LOCATION_ID", obj.LOCATION_ID);

        //        ExecuteNonQueryOracle("PR_REMOVE_GROUP_WEIGH_ORDER", parameters);

        //        response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.Created());
        //    }
        //    catch (Exception ex)
        //    {
        //        response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
        //    }
        //    return response;
        //}
        #endregion
    }
}
