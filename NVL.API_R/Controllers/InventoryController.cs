﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NVL.API_R.Base;
using NVL.API_R.Models;
using NVL.API_R.Helper;

namespace NVL.API_R.Controllers
{
    [RoutePrefix("api/inventory")]
    public class InventoryController : BaseController
    {
        #region 1 - GET ALL INVENTORY - inventory
        [Route("get-all-inventory")]
        [HttpGet]
        public HttpResponseMessage Get_all_inventory(HttpRequestMessage request, uint ORIGIN_ORGANIZATION_ID, 
                                                                                 string MASTER_CODE,
                                                                                 DateTime FROM_DATE,
                                                                                 DateTime TO_DATE)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_ORIGIN_ORGANIZATION_ID", ORIGIN_ORGANIZATION_ID);
                parameters.Add("P_MASTER_CODE", MASTER_CODE);
                parameters.Add("P_FROM_DATE", FROM_DATE);
                parameters.Add("P_TO_DATE", TO_DATE);

                DataSet ds = ExecSPToListOracle("PR_E1_GET_ALL_INVENTORY", parameters); //PR_E2_GET_ALL_INVENTORY

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<INVENTORY_DATA> Received = DataHelper.DataTableToList<INVENTORY_DATA>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound("Không tìm thấy!"));
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 2 - GET IN OUT TRANSACTION - inventory
        [Route("get-in-out-transaction")]
        [HttpGet]
        public HttpResponseMessage Get_In_Out_Transaction(HttpRequestMessage request, string ORGANIZATION_ID, 
                                                                                      int TRAN_OPS,
                                                                                      DateTime FROM_DATE, DateTime TO_DATE,
                                                                                      string MASTER_CODE)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_ORGANIZATION_ID", ORGANIZATION_ID);
                parameters.Add("P_TRAN_OPS", TRAN_OPS);
                parameters.Add("P_FROM_DATE", FROM_DATE);
                parameters.Add("P_TO_DATE", TO_DATE);
                parameters.Add("P_MASTER_CODE", MASTER_CODE);

                DataSet ds = ExecSPToListOracle("PR_E1_GET_NVL_OPS_TRANS", parameters); // PR_E1_GET_IN_OUT_TRANS

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<TRANSACTION_DATA> Received = DataHelper.DataTableToList<TRANSACTION_DATA>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound("Không tìm thấy!"));
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 3 - GET WEIGHED INFOMATION BY BATCH CODE - inventory
        [Route("get-weighed-info-by-batchcode")]
        [HttpGet]
        public HttpResponseMessage Get_Weighed_Information_By_BatchCode(HttpRequestMessage request, string PROCESS_CODE,
                                                                                      string BATCH_CODE)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_PROCESS_CODE", PROCESS_CODE);
                parameters.Add("P_BATCH_CODE", BATCH_CODE);

                DataSet ds = ExecSPToListOracle("PR_E2_GET_WEIGH_INF_BY_BA", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<WEIGHED_BY_BATCH_DATA> Received = DataHelper.DataTableToList<WEIGHED_BY_BATCH_DATA>(ds.Tables[0]);
                    #region LinQ statement
                    #endregion
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound("Không tìm thấy!"));
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 4 - GET MIXED INFORMATION BY BATCH CODE - inventory
        [Route("get-mixed-info-by-batchcode")]
        [HttpGet]
        public HttpResponseMessage Get_Mixed_Information_By_Batch_Code(HttpRequestMessage request, string PROCESS_CODE,
                                                                                                   string BATCH_CODE)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_PROCESS_CODE", PROCESS_CODE);
                parameters.Add("P_BATCH_CODE", BATCH_CODE);

                DataSet ds = ExecSPToListOracle("PR_E1_GET_MIX_INF_BY_BA", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<MIXED_INFO_DATA> Received = DataHelper.DataTableToList<MIXED_INFO_DATA>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound("Không tìm thấy!"));
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 5 - GET MTL TRANSACTIONS BY PXK_NO - inventory
        [Route("get-trans-by-pxk-no")]
        [HttpGet]
        public HttpResponseMessage Get_Transaction_By_PXK_No(HttpRequestMessage request, string SPXK,
                                                                                         uint ORGANIZATION_ID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_SPXK", SPXK);
                parameters.Add("P_ORGANIZATION_ID", ORGANIZATION_ID);

                DataSet ds = ExecSPToListOracle("PR_E1_GET_NVL_TRANS_BY_PXK", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<MTL_TRANS_INF> Received = DataHelper.DataTableToList<MTL_TRANS_INF>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound("Không tìm thấy!"));
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 6 - GET MTL TRANSACTIONS BY BRC_ID - inventory
        [Route("get-trans-by-brc-id")]
        [HttpGet]
        public HttpResponseMessage Get_Transaction_By_BARCODE_ID(HttpRequestMessage request, string PROCESS_CODE,
                                                                                             uint BARCODE_ID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_PROCESS_CODE", PROCESS_CODE);
                parameters.Add("P_BARCODE_ID", BARCODE_ID);

                DataSet ds = ExecSPToListOracle("PR_E1_GET_INV_BY_BRC_ID", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<MTL_TRANS_INF> Received = DataHelper.DataTableToList<MTL_TRANS_INF>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound("Không tìm thấy!"));
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 7 - GET MTL ACTIVE INVENTORY BY BRC_ID - inventory
        [Route("get-active-brc-id-inf")]
        [HttpGet]
        public HttpResponseMessage Get_Active_BRC_ID_INF(HttpRequestMessage request, string PROCESS_CODE,
                                                                                     string MASTER_CODE)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_PROCESS_CODE", PROCESS_CODE);
                parameters.Add("P_MASTER_CODE", MASTER_CODE);

                DataSet ds = ExecSPToListOracle("PR_E1_GET_ACT_BRC_ID", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<BRC_ID_INFO> Received = DataHelper.DataTableToList<BRC_ID_INFO>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound("Không tìm thấy!"));
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 8 - CREATE/UPDATE MTL MICS TRANSACTION FOR A BRC_ID - inventory
        [Route("crup-mics-brc-id-trans")]
        [HttpPost]
        public HttpResponseMessage Get_Create_Mics_For_BRC_ID(HttpRequestMessage request, MICS_CREATION MICS)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_PROCESS_CODE", MICS.PROCESS_CODE);
                parameters.Add("P_BARCODE_ID", MICS.BARCODE_ID);
                parameters.Add("P_MICS_QUANTITY", MICS.MICS_QUANTITY);
                parameters.Add("P_DESCRIPTION", MICS.DESCRIPTION);
                parameters.Add("P_CREATE_BY", MICS.CREATE_BY);

                ExecuteNonQueryOracle("PR_E1_CRE_INPUT_MICS_TRANS", parameters);

                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.Created());
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 9 - CREATE CLOSE MICS TRANSACTION FOR A BRC_ID - inventory
        [Route("cre-close-mics-brc-id-trans")]
        [HttpPost]
        public HttpResponseMessage Get_Create_Close_BRC_ID_Trans(HttpRequestMessage request, MICS_CREATION MICS)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_PROCESS_CODE", MICS.PROCESS_CODE);
                parameters.Add("P_BARCODE_ID", MICS.BARCODE_ID);
                parameters.Add("P_DESCRIPTION", MICS.DESCRIPTION);
                parameters.Add("P_CREATE_BY", MICS.CREATE_BY);

                ExecuteNonQueryOracle("PR_E1_CRE_CLOSE_MICS_TRANS", parameters);

                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.Created());
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 10 - GET INVENTORY INFOMATION BY BRC_ID - inventory
        [Route("get-inventory-by-brc-id")]
        [HttpGet]
        public HttpResponseMessage GET_INV_BY_BARCODE_ID(HttpRequestMessage request, string PROCESS_CODE, uint BARCODE_ID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_PROCESS_CODE", PROCESS_CODE);
                parameters.Add("P_BARCODE_ID", BARCODE_ID);

                DataSet ds = ExecSPToListOracle("PR_E1_GET_INV_BY_BRC_ID", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<BRC_ID_INFO> Received = DataHelper.DataTableToList<BRC_ID_INFO>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received.FirstOrDefault()));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound("Không tìm thấy!"));
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 11 - REACTIVE BRC_ID - inventory
        [Route("reactive-closed-brc-id")]
        [HttpPost]
        public HttpResponseMessage RE_ACTIVE_BARCODE_ID(HttpRequestMessage request, MICS_CREATION MICS)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_PROCESS_CODE", MICS.PROCESS_CODE);
                parameters.Add("P_BARCODE_ID", MICS.BARCODE_ID);
                parameters.Add("P_DESCRIPTION", MICS.DESCRIPTION);
                parameters.Add("P_CREATE_BY", MICS.CREATE_BY);

                ExecuteNonQueryOracle("PR_E1_RE_ACT_CLOSE_BRC_ID", parameters);

                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.Created());
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 00 - GET CLOSED BRC_ID INFOMATION - inventory
        [Route("get-closed-brc-id")]
        [HttpGet]
        public HttpResponseMessage GET_CLOSED_BARCODE_ID(HttpRequestMessage request, string PROCESS_CODE, uint BARCODE_ID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_PROCESS_CODE", PROCESS_CODE);
                parameters.Add("P_BARCODE_ID", BARCODE_ID);

                DataSet ds = ExecSPToListOracle("PR_E1_GET_CLOSE_BRC", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<BRC_ID_INFO> Received = DataHelper.DataTableToList<BRC_ID_INFO>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received.FirstOrDefault()));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound("Không tìm thấy!"));
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

    }
}
