﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NVL.API_R.Base;
using NVL.API_R.Models;
using NVL.API_R.Helper;

namespace NVL.API_R.Controllers
{
    [RoutePrefix("api/final-mix")]  //CONTROL CODE : F1 
    public class FinalMixController : BaseController
    {
        #region 1 - GET TP MIXING BATCH LIST - TP Mixing
        [Route("get-batch-fnl-mix-inf-lst")]
        [HttpGet]
        public HttpResponseMessage Get_batch_fnl_mix_info_lst(HttpRequestMessage request, uint ORGANIZATION_ID, string PROCESS_CODE)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();

                clientAddress = Request.Headers.Contains("IP_ADDRESS") ?
                Request.Headers.GetValues("IP_ADDRESS").FirstOrDefault() : clientAddress;

                parameters.Add("P_ORGANIZATION_ID", ORGANIZATION_ID);
                parameters.Add("P_PROCESS_CODE", PROCESS_CODE);
                parameters.Add("P_IP_ADDRESS", clientAddress);

                DataSet ds = ExecSPToListOracle("PR_F1_GET_BA_FNL_MIX_INFO_LST", parameters);

                if (ds.Tables.Count > 0)
                {
                    var Received = DataHelper.DataTableToList<FNL_BATCH_HDR_INF_DATA>(ds.Tables[0]);
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 2 - GET TP MIXING BATCH DETAIL BY ID - TP Mixing
        [Route("get-batch-fnl-mix-dtl-inf")]
        [HttpGet]
        public HttpResponseMessage Get_Batch_Info_To_Final_Mix(HttpRequestMessage request, uint ORGANIZATION_ID, uint BATCH_ID, int ME,string PROCESS_CODE)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();

                clientAddress = Request.Headers.Contains("IP_ADDRESS") ?
                Request.Headers.GetValues("IP_ADDRESS").FirstOrDefault() : clientAddress;

                parameters.Add("P_ORGANIZATION_ID", ORGANIZATION_ID);
                parameters.Add("P_BATCH_ID", BATCH_ID);
                parameters.Add("P_ME", ME);
                parameters.Add("P_PROCESS_CODE", PROCESS_CODE);
                parameters.Add("P_IP_ADDRESS", clientAddress);

                DataSet ds = ExecSPToListOracle("PR_F1_GET_BA_FNL_MIX_DTL", parameters);

                if (ds.Tables.Count > 0)
                {
                    var Received = DataHelper.DataTableToList<FINAL_BATCH_DTL_INFO_DATA>(ds.Tables[0]);
                    #region LinQ Statement
                    //var tmp = Received.GroupBy(i => new
                    //{
                    //    batchId = i.BATCH_ID,
                    //    batchCode = i.BATCH_CODE,
                    //    batchStatus = i.BATCH_STATUS,
                    //    formulaId = i.FORMULA_ID,
                    //    formulaVers = i.FORMULA_VERS,
                    //    formulaDesc = i.FORMULA_DESC,
                    //    recipeNo = i.RECIPE_NO,
                    //    recipeVers = i.RECIPE_VERS,
                    //    recipeDesc = i.RECIPE_DESC
                    //})
                    //            .Select((p, o) => new FNL_BATCH_DTL_INFO_MD
                    //            {
                    //                STT = o + 1,
                    //                BATCH_ID = p.Key.batchId,
                    //                BATCH_CODE = p.Key.batchCode,
                    //                BATCH_STATUS = p.Key.batchStatus,
                    //                FORMULA_ID = p.Key.formulaId,
                    //                FORMULA_VERS = p.Key.formulaVers,
                    //                FORMULA_DESC = p.Key.formulaDesc,
                    //                RECIPE_NO = p.Key.recipeNo,
                    //                RECIPE_VERS = p.Key.recipeVers,
                    //                RECIPE_DESC = p.Key.recipeDesc,
                    //                MIX_DTL_LIST = p.GroupBy(i => new
                    //                {
                    //                    lineNo = i.LINE_NO,
                    //                    inventoryItemId = i.INVENTORY_ITEM_ID,
                    //                    masterCode = i.MASTER_CODE,
                    //                    itemName = i.ITEM_NAME,
                    //                    primaryQty = i.PRIMARY_QTY,
                    //                    quiCach = i.QUI_CACH,
                    //                    //usingQty = i.USING_QTY,
                    //                    //duong = i.DUONG,
                    //                    inventoryItemIdSub = i.INVENTORY_ITEM_ID_SUB,
                    //                    masterCodeSub = i.MASTER_CODE_SUB,
                    //                    itemNameSub = i.ITEM_NAME_SUB,
                    //                    substitutionDesc = i.SUBSTITUTION_DESC
                    //                    //nlQty = i.NL_QTY,
                    //                    //nlUom = i.NL_UOM,
                    //                    //nlWeigh = i.NL_WEIGH,
                    //                    //bsQty = i.BS_QTY,
                    //                    //bsUom = i.BS_UOM,
                    //                    //bsWeigh = i.BS_WEIGH
                                       
                    //                })
                    //                .Select(n => new MIX_DTL_INFO_MD
                    //                {
                    //                    LINE_NO = n.Key.lineNo,
                    //                    INVENTORY_ITEM_ID = n.Key.inventoryItemId,
                    //                    MASTER_CODE = n.Key.masterCode,
                    //                    ITEM_NAME = n.Key.itemName,
                    //                    PRIMARY_QTY = n.Key.primaryQty,
                    //                    //QUI_CACH = n.Key.quiCach,
                    //                    //USING_QTY = n.Key.usingQty,
                    //                    //DUONG = n.Key.duong,
                    //                    INVENTORY_ITEM_ID_SUB = n.Key.inventoryItemIdSub,
                    //                    MASTER_CODE_SUB = n.Key.masterCodeSub,
                    //                    ITEM_NAME_SUB = n.Key.itemNameSub,
                    //                    SUBSTITUTION_DESC = n.Key.substitutionDesc
                    //                    //NL_QTY = n.Key.nlQty,
                    //                    //NL_UOM =  n.Key.nlUom,
                    //                    //NL_WEIGH = n.Key.nlWeigh,
                    //                    //BS_QTY = n.Key.bsQty,
                    //                    //BS_UOM = n.Key.bsUom,
                    //                    //BS_WEIGH = n.Key.bsWeigh
                    //                }).ToList()
                    //            }).ToList();
                    #endregion LinQ Statement
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK(Received.FirstOrDefault()));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.NotFound, DataHelper.NoDataFound());
                }

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 3 - CREATE TP MIXING RECORD - TP Mixing
        [Route("create-fnl-mix-record")]
        [HttpPost]
        public HttpResponseMessage Create_Final_Mixing_Record(HttpRequestMessage request, MIXING_CREATION obj)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();

                clientAddress = Request.Headers.Contains("IP_ADDRESS") ?
                Request.Headers.GetValues("IP_ADDRESS").FirstOrDefault() : clientAddress;

                parameters.Add("P_BATCH_ID", obj.BATCH_ID);
                parameters.Add("P_ORGANIZATION_ID", obj.ORGANIZATION_ID);
                parameters.Add("P_BATCH_QTY", obj.BATCH_QTY);
                parameters.Add("P_PROCESS_CODE", obj.PROCESS_CODE);
                parameters.Add("P_CREATE_BY", obj.CREATE_BY);
                parameters.Add("p_LOCATION_ID", obj.LOCATION_ID);

                DataSet ds = ExecSPToListOracle("PR_F1_CREATE_TP_MIXING_RECORD", parameters);

                if (ds.Tables.Count > 0)
                {
                    var Received = DataHelper.DataTableToList<FNL_MIX_RECORD_DATA>(ds.Tables[0]);
                    #region LinQ Statement
                    var tmp = Received.GroupBy(i => new
                    {
                        mixingId = i.MIXING_ID,
                        batchId = i.BATCH_ID,
                        formulaId = i.FORMULA_ID,
                        recipeId = i.RECIPE_ID,
                        batchQty = i.BATCH_QTY,
                        mStatus = i.M_STATUS

                    })
                        .Select(p => new FNL_MIX_MD
                        {
                            MIXING_ID = p.Key.mixingId,
                            BATCH_ID = p.Key.batchId,
                            FORMULA_ID = p.Key.formulaId,
                            RECIPE_ID = p.Key.recipeId,
                            BATCH_QTY = p.Key.batchQty,
                            MIX_DTL_LIST = p.GroupBy(i => new
                            {
                                lineNo = i.LINE_NO,
                                mixingDtlId = i.MIXING_DTL_ID,
                                inventoryItemId = i.INVENTORY_ITEM_ID,
                                masterCode = i.MASTER_CODE,
                                itemName = i.ITEM_NAME,
                                primaryQty = i.PRIMARY_QTY,
                                totalQty = i.TOTAL_QTY,
                                numberAllItems = i.NUMBER_ALL_ITEMS,
                                numberEvenItems = i.NUMBER_EVEN_ITEMS,
                                numberOddItems = i.NUMBER_ODD_ITEMS,
                                dStatus = i.D_STATUS

                            })
                            .Select(n => new FNL_MIX_DTL_MD
                            {
                                LINE_NO = n.Key.lineNo,
                                MIXING_DTL_ID = n.Key.mixingDtlId,
                                INVENTORY_ITEM_ID = n.Key.inventoryItemId,
                                MASTER_CODE = n.Key.masterCode,
                                ITEM_NAME = n.Key.itemName,
                                PRIMARY_QTY = n.Key.primaryQty,
                                TOTAL_QTY = n.Key.totalQty,
                                NUMBER_ALL_ITEMS = n.Key.numberAllItems,
                                NUMBER_EVEN_ITEMS = n.Key.numberEvenItems,
                                NUMBER_ODD_ITEMS = n.Key.numberOddItems,
                                D_STATUS = n.Key.dStatus,
                                MIX_ITEM_DTL_LIST = n.GroupBy(a => new
                                {
                                    mixingItemDtl = a.MIXING_ITEM_ID,
                                    inventoryItemId = a.INVENTORY_ITEM_ID,
                                    barcodeId = a.BARCODE_ID,
                                    itemType = a.ITEM_TYPE,
                                    iStatus = a.I_STATUS
                                })
                                .Select(b => new FNL_MIX_ITEM_DTL_MD
                                {
                                    MIXING_ITEM_ID = b.Key.mixingItemDtl,
                                    INVENTORY_ITEM_ID = b.Key.inventoryItemId,
                                    BARCODE_ID = b.Key.barcodeId,
                                    ITEM_TYPE = b.Key.itemType,
                                    I_STATUS = b.Key.iStatus
                                }).ToList()
                            }).ToList()
                        }).ToList().FirstOrDefault();
                    #endregion LinQ Statement
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK(tmp));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 4 - GET TP MIXING LIST - TP Mixing
        [Route("get-fnl-mix-record-lst")]
        [HttpGet]
        public HttpResponseMessage Get_Final_Mixing_List(HttpRequestMessage request, uint BATCH_ID, uint ORGANIZATION_ID, uint LOCATION_ID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();

                clientAddress = Request.Headers.Contains("IP_ADDRESS") ?
                Request.Headers.GetValues("IP_ADDRESS").FirstOrDefault() : clientAddress;

                parameters.Add("P_BATCH_ID", BATCH_ID);
                parameters.Add("P_ORGANIZATION_ID", ORGANIZATION_ID);
                parameters.Add("P_LOCATION_ID", LOCATION_ID);

                DataSet ds = ExecSPToListOracle("PR_F1_GET_TP_MIXING_RECORD_LST", parameters);

                if (ds.Tables.Count > 0)
                {
                    var Received = DataHelper.DataTableToList<FNL_MIX_RECORD_DATA>(ds.Tables[0]);
                    #region LinQ Statement
                    var tmp = Received.GroupBy(i => new
                    {
                        mixingId = i.MIXING_ID,
                        batchId = i.BATCH_ID,
                        formulaId = i.FORMULA_ID,
                        recipeId = i.RECIPE_ID,
                        batchQty = i.BATCH_QTY,
                        mStatus = i.M_STATUS

                    })
                        .Select(p => new FNL_MIX_MD
                        {
                            MIXING_ID = p.Key.mixingId,
                            BATCH_ID = p.Key.batchId,
                            FORMULA_ID = p.Key.formulaId,
                            RECIPE_ID = p.Key.recipeId,
                            BATCH_QTY = p.Key.batchQty,
                            MIX_DTL_LIST = p.GroupBy(i => new
                            {
                                lineNo = i.LINE_NO,
                                mixingDtlId = i.MIXING_DTL_ID,
                                inventoryItemId = i.INVENTORY_ITEM_ID,
                                masterCode = i.MASTER_CODE,
                                itemName = i.ITEM_NAME,
                                quiCach = i.QUI_CACH,
                                primaryQty = i.PRIMARY_QTY,
                                totalQty = i.TOTAL_QTY,
                                numberAllItems = i.NUMBER_ALL_ITEMS,
                                numberEvenItems = i.NUMBER_EVEN_ITEMS,
                                evenItemWeigh = i.EVEN_ITEM_WEIGH,
                                evenItemType = i.EVEN_ITEM_TYPE,
                                numberOddItems = i.NUMBER_ODD_ITEMS,
                                oddItemWeigh = i.ODD_ITEM_WEIGH,
                                oddItemType = i.ODD_ITEM_TYPE,
                                dStatus = i.D_STATUS

                            })
                            .Select(n => new FNL_MIX_DTL_MD
                            {
                                LINE_NO = n.Key.lineNo,
                                MIXING_DTL_ID = n.Key.mixingDtlId,
                                INVENTORY_ITEM_ID = n.Key.inventoryItemId,
                                MASTER_CODE = n.Key.masterCode,
                                ITEM_NAME = n.Key.itemName,
                                QUI_CACH = n.Key.quiCach,
                                PRIMARY_QTY = n.Key.primaryQty,
                                TOTAL_QTY = n.Key.totalQty,
                                NUMBER_ALL_ITEMS = n.Key.numberAllItems,
                                NUMBER_EVEN_ITEMS = n.Key.numberEvenItems,
                                EVEN_ITEM_WEIGH = n.Key.evenItemWeigh,
                                EVEN_ITEM_TYPE = n.Key.evenItemType,
                                NUMBER_ODD_ITEMS = n.Key.numberOddItems,
                                ODD_ITEM_WEIGH = n.Key.oddItemWeigh,
                                ODD_ITEM_TYPE = n.Key.oddItemType,
                                D_STATUS = n.Key.dStatus,
                                MIX_ITEM_DTL_LIST = n.GroupBy(a => new
                                {
                                    mixingItemDtl = a.MIXING_ITEM_ID,
                                    inventoryItemId = a.INVENTORY_ITEM_ID,
                                    barcodeId = a.BARCODE_ID,
                                    itemType = a.ITEM_TYPE,
                                    iStatus = a.I_STATUS
                                })
                                .Select(b => new FNL_MIX_ITEM_DTL_MD
                                {
                                    MIXING_ITEM_ID = b.Key.mixingItemDtl,
                                    INVENTORY_ITEM_ID = b.Key.inventoryItemId,
                                    BARCODE_ID = b.Key.barcodeId,
                                    ITEM_TYPE = b.Key.itemType,
                                    I_STATUS = b.Key.iStatus
                                }).ToList()
                            }).ToList()
                        }).ToList();
                    #endregion LinQ Statement
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK(tmp));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 5 - SUBMIT TP MIXING PACKAGE - TP Mixing
        [Route("submit-final-mixing-package")]
        [HttpGet]
        public HttpResponseMessage Submit_Final_Mixing_Package(HttpRequestMessage request, uint BATCH_ID, uint ORGANIZATION_ID, uint LOCATION_ID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();

                clientAddress = Request.Headers.Contains("IP_ADDRESS") ?
                Request.Headers.GetValues("IP_ADDRESS").FirstOrDefault() : clientAddress;

                parameters.Add("P_BATCH_ID", BATCH_ID);
                parameters.Add("P_ORGANIZATION_ID", ORGANIZATION_ID);
                parameters.Add("P_LOCATION_ID", LOCATION_ID);

                DataSet ds = ExecSPToListOracle("PR_F1_SUBMIT_FNL_PACKAGE", parameters);

                if (ds.Tables.Count > 0)
                {
                    var Received = DataHelper.DataTableToList<FNL_MIX_RECORD_DATA>(ds.Tables[0]);

                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK(Received));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

    }
}