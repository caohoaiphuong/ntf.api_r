﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NVL.API_R.Base;
using NVL.API_R.Models;
using NVL.API_R.Helper;
using System.Text.RegularExpressions;

namespace NVL.API_R.Controllers
{
    [RoutePrefix("api/user")]
    public class UserController : BaseController
    {
        /// <summary>
        /// USER
        /// </summary>
        #region 1 - Register new user
        [Route("user-register")]
        [HttpPost]
        public HttpResponseMessage User_Register(HttpRequestMessage request, UserModel obj)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                //Create username
                obj.FULLNAME = obj.FULLNAME.Trim(new char[] { ' ' });
                obj.NON_UNICODE = FormatHelper.RemoveUnicode(obj.FULLNAME).ToLower();
                string[] tmpStr = obj.NON_UNICODE.Split(new char[] { ' ' });
                if (tmpStr.Length > 0)
                {
                    string USERNAME = tmpStr[tmpStr.Length - 1].ToLower();
                    for (int i = 0; i < tmpStr.Length - 1; i++)
                    {
                        USERNAME = USERNAME + tmpStr[i].Substring(0, 1).ToLower();
                    }
                    parameters.Add("P_USERNAME", USERNAME);
                    parameters.Add("P_MSNV", obj.MSNV);
                    parameters.Add("P_FULLNAME", obj.FULLNAME);
                    parameters.Add("P_NON_UNICODE", obj.NON_UNICODE);
                    parameters.Add("P_MAIL", obj.MAIL);
                    parameters.Add("P_PHONE", obj.PHONE);
                    parameters.Add("P_CREATE_BY", obj.CREATE_BY);

                    DataSet ds = ExecSPToListOracle("PR_U1_CREATE_NEW_USR", parameters);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        var tmp = DataHelper.DataTableToList<UserModel>(ds.Tables[0]);
                        response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.Created(tmp.FirstOrDefault()));
                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound());
                    }
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.ParameterError("FullName không hợp lệ."));
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 2 - Login
        [Route("login")]
        [HttpPost]
        public HttpResponseMessage Do_Login(HttpRequestMessage request, UserModel obj)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                string MD5 = DataHelper.getMd5Hash(obj.PASSWORD);

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_USERNAME", obj.USERNAME);
                parameters.Add("P_PASSWORD", MD5);

                DataSet ds = ExecSPToListOracle("PR_U1_DO_LOGIN", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    var tmp = DataHelper.DataTableToList<UserModel>(ds.Tables[0]);
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK(tmp.FirstOrDefault()));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound(ConfigHelper.GetConnectionString()));
                }
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 3 - GET user list
        [Route("get-usrs-lst")]
        [HttpGet]
        public HttpResponseMessage Get_User_List(HttpRequestMessage request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                DataSet ds = ExecSPToListOracle("PR_U1_GET_USER_LIST");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var tmp = DataHelper.DataTableToList<UserModel>(ds.Tables[0]);
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(tmp));
                }
                else
                {
                    response= Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                response= Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 4 - UPDATE password 
        [Route("usr-chg-pass")]
        [HttpPost]
        public HttpResponseMessage CHG_USR_PASS(HttpRequestMessage Request, UserPass obj)
        {

            HttpResponseMessage response = new HttpResponseMessage();
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            try
            {
                parameters.Add("P_USERNAME", obj.USERNAME);
                parameters.Add("P_O_PASSWORD", obj.OLD_PASSWORD);
                parameters.Add("P_PASSWORD", obj.PASSWORD);
                parameters.Add("P_UPDATE_BY", obj.UPDATE_BY);

                ExecuteNonQueryOracle("PR_U1_CHG_USR_PSS", parameters);
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK());
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        /// <summary>
        /// USER APP
        /// </summary>
        /// 
        #region 1 - Assign application to User
        [Route("assign-app-2-user")]
        [HttpPost]
        public HttpResponseMessage Assign_App_2_User(HttpRequestMessage Request, APP_ASSIGNMENT app)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_ASSIGNER", app.ASSIGNER);
                parameters.Add("P_PASSWORD", app.PASSWORD);
                parameters.Add("P_USERNAME", app.USERNAME);
                parameters.Add("P_APP_ID", app.APP_ID);

                DataSet DS = ExecSPToListOracle("PR_U1_CREATE_APP_USR_MAP", parameters);
                if (DS.Tables[0].Rows.Count != 0)
                {
                    DataTable dt = DS.Tables[0];
                    USR_GET_APP_USER_ROLE tmp = DataHelper.DataTableToList<USR_GET_APP_USER_ROLE>(dt).FirstOrDefault();
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.Created(tmp));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 2 - Assign App Role to User
        [Route("assign-role-2-user")]
        [HttpPost]
        public HttpResponseMessage Assign_Role_2_User(HttpRequestMessage Request, APP_ASSIGNMENT app)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_ASSIGNER", app.ASSIGNER);
                parameters.Add("P_PASSWORD", app.PASSWORD);
                parameters.Add("P_USERNAME", app.USERNAME);
                parameters.Add("P_APP_ID", app.APP_ID);

                DataSet DS = ExecSPToListOracle("PR_U1_CREATE_ROLE_USR_MAP", parameters);
                if (DS.Tables[0].Rows.Count != 0)
                {
                    DataTable dt = DS.Tables[0];
                    USR_GET_APP_USER_ROLE tmp = DataHelper.DataTableToList<USR_GET_APP_USER_ROLE>(dt).FirstOrDefault();
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK(tmp));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound());
                }
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 3 - Unassign App Of User
        [Route("unassign-app-of-user")]
        [HttpPost]
        public HttpResponseMessage UnAssign_App_Of_User(HttpRequestMessage Request, APP_ASSIGNMENT app)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_ASSIGNER", app.ASSIGNER);
                parameters.Add("P_PASSWORD", app.PASSWORD);
                parameters.Add("P_USERNAME", app.USERNAME);
                parameters.Add("P_APP_ID", app.APP_ID);

                ExecuteNonQueryOracle("PR_U1_DISPOSAL_APP_USR_MAP", parameters);

                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK(""));

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 4 - Unassign-role-of-user
        [Route("unassign-role-of-user")]
        [HttpPost]
        public HttpResponseMessage UnAssign_Role_Of_User(HttpRequestMessage Request, APP_ASSIGNMENT app)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_ASSIGNER", app.ASSIGNER);
                parameters.Add("P_PASSWORD", app.PASSWORD);
                parameters.Add("P_USERNAME", app.USERNAME);
                parameters.Add("P_APP_ID", app.APP_ID);

                ExecuteNonQueryOracle("PR_U1_DISPOSAL_ROLE_USR_MAP", parameters);

                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK(""));

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 5 - GET app user role 
        [Route("app-usr-role")]
        [HttpGet]
        public HttpResponseMessage GetAppUserRole(HttpRequestMessage Request, string username)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            if (!string.IsNullOrEmpty(username))
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_USERNAME", username);
                try
                {
                    DataSet ds = ExecSPToListOracle("PR_U1_GET_APP_ROLE_BY_USR", parameters);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<USR_GET_APP_USER_ROLE> Recieved = DataHelper.DataTableToList<USR_GET_APP_USER_ROLE>(ds.Tables[0]);
                        #region LinQ Statement
                        var tmp = Recieved.GroupBy(k
                                                    => new
                                                    {
                                                        username = k.USERNAME,
                                                        appgroup = k.APP_GROUP
                                                    })
                                                        .Select(u =>
                                                        new
                                                        {
                                                            USERNAME = u.Key.username,
                                                            APP_GROUP = u.Key.appgroup,
                                                            LIST_APP_ROLE = u.GroupBy(k1 => new
                                                            {
                                                                appid = k1.APP_ID,
                                                                appname = k1.APP_NAME,
                                                                objrole = k1.OBJECT_ROLE,
                                                                sysrole = k1.SYSTEM_ROLE,
                                                                admrole = k1.ADMIN_ROLE
                                                            }).Select(r =>
                                                              new
                                                              {
                                                                  APP_ID = r.Key.appid,
                                                                  APP_NAME = r.Key.appname,
                                                                  OBJECT_ROLE = r.Key.objrole,
                                                                  SYSTEM_ROLE = r.Key.sysrole,
                                                                  ADMIN_ROLE = r.Key.admrole
                                                              }).ToList()
                                                        }).ToList();
                        #endregion
                        response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(tmp));
                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                    }
                }
                catch (Exception ex)
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
                }
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.ParameterError());
            }
            return response;
        }
        #endregion
    }
}
