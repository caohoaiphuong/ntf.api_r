﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NVL.API_R.Base;
using NVL.API_R.Models;
using NVL.API_R.Helper;

namespace NVL.API_R.Controllers
{
    [RoutePrefix("api/app")]
    public class ApplicationController : BaseController
    {
        #region 1- GET Application List
        [Route("get-apps-lst")]
        [HttpGet]
        public HttpResponseMessage Get_Application_Lst(HttpRequestMessage request)
        {
            try
            {
                DataSet ds = ExecSPToListOracle("PR_B1_GET_APP_LST");
                if (ds.Tables[0].Rows.Count != 0)
                {
                    List<APP> Recieved = DataHelper.DataTableToList<APP>(ds.Tables[0]);

                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK(Recieved));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 2- GET application groups List
        [Route("app-groups-lst")]
        [HttpGet]
        public HttpResponseMessage GetAppGroupList(HttpRequestMessage Request)
        {
            try
            {
                DataSet DS = ExecSPToListOracle("PR_B1_GET_APP_GRP_LST");
                if (DS.Tables[0].Rows.Count > 0)
                {
                    DataTable dt = DS.Tables[0];
                    List<string> tmp = new List<string>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        tmp.Add(dr["APP_GROUP"].ToString());
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(tmp));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 3- GET apps List by user
        [Route("get-apps-lst-by-usr")]
        [HttpGet]
        public HttpResponseMessage Get_App_List_by_User(HttpRequestMessage request, string USERNAME)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_USERNAME", USERNAME);

                DataSet ds = ExecSPToListOracle("PR_B1_GET_APP_LST_BY_USR", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    var tmp = DataHelper.DataTableToList<APP>(ds.Tables[0]);
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(tmp));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 4- GET apps List by user and ipaddress
        [Route("get-apps-lst-by-usr-and-ipaddr")]
        [HttpGet]
        public HttpResponseMessage Get_App_List_By_User_And_Ip(HttpRequestMessage request, string USERNAME)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_USERNAME", USERNAME);
                parameters.Add("P_IP_ADDRESS", request.Headers.Contains("IP_ADDRESS") ?
                request.Headers.GetValues("IP_ADDRESS").FirstOrDefault() : clientAddress);

                DataSet ds = ExecSPToListOracle("PR_B1_GET_APP_BY_USR_AND_IP", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    var tmp = DataHelper.DataTableToList<APP>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(tmp));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region Load Application List by User and Station
        //[Route("load-apps-by-user-and-station")]
        //[HttpGet]
        //public HttpResponseMessage loadListAppByUserAndStation(HttpRequestMessage request, string username)
        //{
        //    HttpResponseMessage response = new HttpResponseMessage();
        //    try
        //        {
        //            Dictionary<string, object> parameters = new Dictionary<string, object>();
        //            parameters.Add("P_USERNAME", username);
        //            parameters.Add("P_IP_ADDRESS", clientAddress);

        //            DataSet ds = ExecSPToListOracle("PR_GET_APPS_BY_USER_AND_IP", parameters);
        //            if (ds.Tables[0].Rows.Count != 0)
        //            {
        //                List<APP> Recieved = DataHelper.DataTableToList<APP>(ds.Tables[0]);

        //                response =  Request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Recieved));
        //            }
        //            else
        //            {
        //                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            response= Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
        //        }
        //    return response;
        //}
        #endregion
    }
}