﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NVL.API_R.Base;
using NVL.API_R.Models;
using NVL.API_R.Helper;


namespace NVL.API_R.Controllers
{
    [RoutePrefix("api/mix")]
    public class MixController : BaseController
    {
        #region 1 - GET READY TO MIX BATCH LIST - Trộn NL
        [Route("get-ready-to-mix-batch-lst")]
        [HttpGet]
        public HttpResponseMessage Get_Ready_To_Mix_Batch_List(HttpRequestMessage request,int ORGANIZATION_ID, string PROCESS_CODE,int LOCATION_ID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_ORGANIZATION_ID", ORGANIZATION_ID);
                parameters.Add("P_PROCESS_CODE", PROCESS_CODE);
                parameters.Add("P_LOCATION_ID", LOCATION_ID);

                DataSet ds = ExecSPToListOracle("PR_D1_GET_RDY_MIX_BA_LST", parameters);
                if (ds.Tables.Count > 0)
                {
                    var Received = DataHelper.DataTableToList<MIX_ITEM_DATA>(ds.Tables[0]);
                    #region LinQ Statement
                    var tmp = Received.GroupBy(i => new
                    {
                        batchId = i.BATCH_ID,
                        batchCode = i.BATCH_CODE,
                        productName = i.FORMULA_DESC1,
                    })
                                .Select(p => new
                                {
                                    BATCH_ID = p.Key.batchId,
                                    BATCH_CODE = p.Key.batchCode,
                                    PRODUCT_NAME = p.Key.productName,
                                    ORDERs = p.GroupBy(i => new
                                    {
                                        orderID = i.ORDER_ID,
                                        processCode = i.PROCESS_CODE,
                                        batchesQty = i.BATCHES_QTY,
                                        deviceType = i.DEVICE_TYPE,
                                        status = i.STATUS,
                                        orderGroup = i.ORDER_GROUP
                                    })
                                    .Select(n => new
                                    {
                                        ORDER_ID = n.Key.orderID,
                                        BATCHES_QTY = n.Key.batchesQty,
                                        DEVICE_TYPE = n.Key.deviceType,
                                        STATUS = n.Key.status,
                                        ORDER_GROUP = n.Key.orderGroup,
                                    }).ToList()
                                }).ToList();
                    #endregion LinQ Statement
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(tmp));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 2 - GET MIXING WO BY ID - Trộn NL
        [Route("get-wo-to-mix-by-id")]
        [HttpGet]
        public HttpResponseMessage Get_WO_To_Mix_By_Id(HttpRequestMessage request, string ORDER_ID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_ORDER_ID", ORDER_ID);

                DataSet ds = ExecSPToListOracle("PR_D1_GET_WO_TO_MIX_BY_ID", parameters);

                if (ds.Tables.Count > 0)
                {
                    List<MIX_WO_DATA> Received = DataHelper.DataTableToList<MIX_WO_DATA>(ds.Tables[0]);
                    #region LinQ statement
                    var tmp = Received
                                .GroupBy(i => new
                                {
                                    productName = i.FORMULA_DESC1,
                                    orderID = i.ORDER_ID,
                                    processCode = i.PROCESS_CODE,
                                    batchId = i.BATCH_ID,
                                    batchCode = i.BATCH_CODE,
                                    batchesQty = i.BATCHES_QTY,
                                    deviceId = i.DEVICE_ID,
                                    status = i.STATUS,
                                    createBy = i.CREATE_BY,
                                    createDate = i.CREATE_DATE,
                                    updateBy = i.UPDATE_BY,
                                    updateDate = i.UPDATE_DATE,
                                    orderGroup = i.ORDER_GROUP,
                                    qtyMix = i.QTY_MIX,
                                    qtyMixed = i.QTY_MIXED
                                })
                                .Select(p => new
                                {
                                    PRODUCT_NAME = p.Key.productName,
                                    ORDER_ID = p.Key.orderID,
                                    PROCESS_CODE = p.Key.processCode,
                                    BATCH_ID = p.Key.batchId,
                                    BATCH_CODE = p.Key.batchCode,
                                    BATCHES_QTY = p.Key.batchesQty,
                                    DEVICE_ID = p.Key.deviceId,
                                    STATUS = p.Key.status,
                                    CREATE_BY = p.Key.createBy,
                                    CREATE_DATE = p.Key.createDate,
                                    UPDATE_BY = p.Key.updateBy,
                                    UPDATE_DATE = p.Key.updateDate,
                                    ORDER_GROUP = p.Key.orderGroup,
                                    CONTENT_QTY = p.Key.qtyMix,
                                    MIXED_CONTENT = p.Key.qtyMixed,
                                    CONTENTS_LIST = p.GroupBy(i => new {
                                        orderDetailId = i.ORDER_DETAIL_ID,
                                        orderId = i.ORDER_ID,
                                        batch = i.BATCH_ID,
                                        inventoryItemId = i.INVENTORY_ITEM_ID,
                                        description = i.DESCRIPTION,
                                        segment1 = i.SEGMENT1,
                                        stWeigh = i.ST_WEIGH,
                                        packedQty = i.PACKED_QTY,
                                        totalWeigh = i.TOTAL_WEIGH,
                                        status = i.STATUS_DETAIL,
                                        subItemId = i.SUB_INVENTORY_ITEM_ID,
                                        subStartDate = i.SUB_START_DATE,
                                        subEndDate = i.SUB_END_DATE,
                                        co = i.CO,
                                        co_mix = i.CO_MIX
                                    }).Select(m => new
                                    {
                                        ORDER_DETAIL_ID = m.Key.orderDetailId,
                                        ORDER_ID = m.Key.orderId,
                                        BATCH_ID = m.Key.batch,
                                        INVENTORY_ITEM_ID = m.Key.inventoryItemId,
                                        ITEM_NAME = m.Key.description,
                                        MASTER_CODE = m.Key.segment1,
                                        ST_WEIGH = m.Key.stWeigh,
                                        PACKAGE_QTY = m.Key.packedQty,
                                        TOTAL_WEIGH = m.Key.totalWeigh,
                                        STATUS = m.Key.status,
                                        SUB_INVENTORY_ITEM_ID = m.Key.subItemId,
                                        SUB_START_DATE = m.Key.subStartDate,
                                        SUB_END_DATE = m.Key.subEndDate,
                                        PACKAGES_QTY = m.Key.co,
                                        MIXED_PACKAGES = m.Key.co_mix,
                                        PACKAGES_LIST = m.GroupBy(i => new {
                                            tareWeigh = i.TARE_WEIGH,
                                            netWeigh = i.NET_WEIGH,
                                            itemSTWeigh = i.ITEM_ST_WEIGH,
                                            refBarCodeId = i.REF_BARCODE_ID,
                                            refTransactionId = i.REF_TRANSACTION_ID,
                                            secondLot = i.SECOND_LOT,
                                            barcodeId = i.BARCODE_ID,
                                            statusItemDetail = i.STATUS_ITEM_DETAIL
                                        }).Select(n => new
                                        {
                                            TARE_WEIGH = n.Key.tareWeigh,
                                            NET_WEIGH = n.Key.netWeigh,
                                            ITEM_ST_WEIGH = n.Key.itemSTWeigh,
                                            REF_BARCODE_ID = n.Key.refBarCodeId,
                                            REF_TRANSACTION_ID = n.Key.refTransactionId,
                                            BARCODE_ID = n.Key.barcodeId,
                                            SECOND_LOT = n.Key.secondLot,
                                            STATUS_ITEM_DETAIL = n.Key.statusItemDetail
                                        }).OrderBy(o => o.BARCODE_ID).ToList()
                                    }).ToList()
                                }).ToList();
                    #endregion LinQ statement
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(tmp.FirstOrDefault()));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 3 - SUBMIT MIXED PAKAGE SCAN - Trộn NL
        [Route("submit-mixed-package")]
        [HttpPost]
        public HttpResponseMessage Submit_Mixed_Package (HttpRequestMessage request, BARCODE obj)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_PROCESS_CODE", obj.PROCESS_CODE);
                parameters.Add("P_BARCODE_ID", obj.BARCODE_ID);
                parameters.Add("P_CREATE_BY", obj.USERNAME);

                DataSet ds = ExecSPToListOracle("PR_D1_SUBMIT_MIX_PACK", parameters);

                if (ds.Tables.Count > 0)
                {
                    List<BARCODE> Received = DataHelper.DataTableToList<BARCODE>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.Created(Received.FirstOrDefault()));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion
    }
}