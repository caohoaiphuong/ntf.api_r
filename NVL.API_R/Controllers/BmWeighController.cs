﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NVL.API_R.Base;
using NVL.API_R.Models;
using NVL.API_R.Helper;

namespace NVL.API_R.Controllers
{
    [RoutePrefix("api/bm")]
    public class BmWeighController : BaseController
    {
        #region 1- GET BOTME WOs INFOR LIST - Chia BM
        [Route("get-bm-wo-info-lst")]
        [HttpGet]
        public HttpResponseMessage Get_BM_WO_Infor_List(HttpRequestMessage request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_IP_ADDRESS", request.Headers.Contains("IP_ADDRESS") ?
                                request.Headers.GetValues("IP_ADDRESS").FirstOrDefault() : clientAddress);

                DataSet ds = ExecSPToListOracle("PR_G1_GET_BM_WO_INF_LST", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<BM_WO_INFOR> Received = DataHelper.DataTableToList<BM_WO_INFOR>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 2 - GET BOTME WO DETAIL BY WOID - Chia BM
        [Route("get-bm-wo-by-id")]
        [HttpGet]
        public HttpResponseMessage Get_BM_WO_By_ID(HttpRequestMessage request, string ORDER_ID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_ORDER_ID", ORDER_ID);

                DataSet ds = ExecSPToListOracle("PR_G1_GET_BM_WO_BY_ID", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<BM_WO_DATA> Received = DataHelper.DataTableToList<BM_WO_DATA>(ds.Tables[0]);
                    #region LinQ Statement
                    var tmp = Received.GroupBy(a => new {
                                                    batchId = a.BATCH_ID,
                                                    batchCode = a.BATCH_CODE,
                                                    productName = a.PRODUCT_NAME,
                                                    khsxDate =a.KHSX_DATE,
                                                    khsx = a.KHSX,
                                                    volumeOfBatch = a.VOLUME_OF_A_BATCH,
                                                    batchCompleted = a.BATCHES_COMPLETED,
                                                    batchesQty = a.BATCHES_QTY,
                                                    orderId = a.ORDER_ID
                                                    }).Select(p => new BM_WO {
                                                        BATCH_CODE = p.Key.batchCode,
                                                        BATCH_ID = p.Key.batchId,
                                                        ORDER_ID = p.Key.orderId,
                                                        PRODUCT_NAME = p.Key.productName,
                                                        VOLUME_OF_A_BATCH = p.Key.volumeOfBatch,
                                                        BATCHES_COMPLETED = p.Key.batchCompleted,
                                                        BATCHES_QTY = p.Key.batchesQty,
                                                        KHSX = p.Key.khsx,
                                                        KHSX_DATE = p.Key.khsxDate,
                                                        RECIPE = p.GroupBy(e => new
                                                        {
                                                            Recipe_Version = e.RECIPE_VERSION,
                                                            Recipe_No = e.RECIPE_NO
                                                        }).Select(g => new RECIPE_MD
                                                        {
                                                            VERSION = g.Key.Recipe_Version,
                                                            RECIPE_CODE = g.Key.Recipe_No
                                                        }).First(),
                                                        FORMULA = p.GroupBy(e => new
                                                        {
                                                            Formula_Vers = e.FORMULA_VERS,
                                                            Formula_NO = e.FORMULA_NO,
                                                            CREATION_DATE = e.CREATION_DATE
                                                        })
                                                        .Select(g => new FORMULA_MD
                                                        {
                                                            VERSION = g.Key.Formula_Vers,
                                                            CODE = g.Key.Formula_NO,
                                                            VALIDATION_DATE = g.Key.CREATION_DATE
                                                        }).First(),
                                                        BOTME_WEIGHED_LIST = p.GroupBy(e => new
                                                        {
                                                            barcodeID = e.BARCODE_ID,
                                                            processCode = e.PROCESS_CODE,
                                                            stWeigh = e.ST_WEIGH,
                                                            netWeigh = e.NET_WEIGH,
                                                            TareWeigh = e.TARE_WEIGH,
                                                            uom = e.UOM,
                                                            status = e.STATUS,
                                                            expDate = e.EXP_DATE,
                                                            createBy = e.CREATE_BY,
                                                            createDate = e.CREATE_DATE,
                                                            updateBy = e.UPDATE_BY,
                                                            updateDate = e.UPDATE_DATE
                                                        }).Select(g => new BM_PACKAGE
                                                        {
                                                            BARCODE_ID = g.Key.barcodeID,
                                                            PROCESS_CODE = g.Key.processCode,
                                                            ST_WEIGH = g.Key.stWeigh,
                                                            NET_WEIGH = g.Key.netWeigh,
                                                            TARE_WEIGH = g.Key.TareWeigh,
                                                            UOM = g.Key.uom,
                                                            STATUS = g.Key.status,
                                                            EXP_DATE = g.Key.expDate,
                                                            CREATE_BY = g.Key.createBy,
                                                            CREATE_DATE = g.Key.createDate
                                                        }).Where(x=>x.BARCODE_ID > 0).ToList()
                                                    }).ToList();
                    #endregion
                    if (tmp.Count == 1)
                    {
                        response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(tmp.FirstOrDefault()));
                    }
                    else
                    {
                        response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound("Không duy nhất."));
                    }
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 3 - CREATE BM WEIGH PACKAGE - Chia BM
        [Route("create-bm-weigh-package")]
        [HttpPost]
        public HttpResponseMessage Create_Botme_Weigh_Package(HttpRequestMessage request, BM_PACKAGE obj)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();

                parameters.Add("P_ORDER_ID", obj.ORDER_ID);
                parameters.Add("P_PROCESS_CODE", obj.PROCESS_CODE);
                parameters.Add("P_ST_WEIGH", obj.ST_WEIGH);
                parameters.Add("P_NET_WEIGH", obj.NET_WEIGH);
                parameters.Add("P_TARE_WEIGH", obj.TARE_WEIGH);
                parameters.Add("P_UOM", obj.UOM);
                parameters.Add("P_CREATE_BY", obj.CREATE_BY);
                parameters.Add("P_IP_ADDRESS", request.Headers.Contains("IP_ADDRESS") ?
                request.Headers.GetValues("IP_ADDRESS").FirstOrDefault() : clientAddress);

                DataSet ds = ExecSPToListOracle("PR_G1_CREATE_BM_WEIGH_PACK", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<BM_PACKAGE> Received = DataHelper.DataTableToList<BM_PACKAGE>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.Created(Received.FirstOrDefault()));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion
    }
}
