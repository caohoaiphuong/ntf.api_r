﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NVL.API_R.Base;
using NVL.API_R.Models;
using NVL.API_R.Helper;

namespace NVL.API_R.Controllers
{
    [RoutePrefix("api/station")]
    public class StationController : BaseController
    {
        #region 1 - GET PROCESS LIST
        [Route("get-process-lst")]
        [HttpGet]
        public HttpResponseMessage Get_Process_List(HttpRequestMessage request)
        {
            try
            {
                DataSet ds = ExecSPToListOracle("PR_A1_GET_PRO_LST");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<PROCESS> Received = DataHelper.DataTableToList<PROCESS>(ds.Tables[0]);
                    return Request.CreateResponse(DataHelper.OK(Received));
                }
                else
                {
                    return Request.CreateResponse(DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 2 - GET STATION WITH APPS MAPPED LIST
        [Route("get-station-app-map-lst")]
        [HttpGet]
        public HttpResponseMessage Get_Station_Device_Mapped_List(HttpRequestMessage Request)
        {
            try
            {
                DataSet ds = ExecSPToListOracle("PR_A1_GET_STA_APP_MAP_LST");

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<STATION_APP_ITEM> Received = DataHelper.DataTableToList<STATION_APP_ITEM>(ds.Tables[0]);
                    #region LinQ Statement
                    var tmp = Received.GroupBy(i => new
                    {
                        _ID = i.ID,
                        _STATION_CODE = i.STATION_CODE,
                        _DESCRIPTION = i.DESCRIPTION,
                        _IP_ADDRESS = i.IP_ADDRESS,
                        _STATUS = i.STATUS,
                        _CREATE_BY = i.CREATE_BY,
                        _CREATE_DATE = i.CREATE_DATE,
                        _UPDATE_BY = i.UPDATE_BY,
                        _UPDATE_DATE = i.UPDATE_DATE
                    }).Select(p => new
                    {
                        ID = p.Key._ID,
                        STATION_CODE = p.Key._STATION_CODE,
                        DESCRIPTION = p.Key._DESCRIPTION,
                        IP_ADDRESS = p.Key._IP_ADDRESS,
                        STATUS = p.Key._STATUS,
                        CREATE_BY = p.Key._CREATE_BY,
                        CREATE_DATE = p.Key._CREATE_DATE,
                        UPDATE_BY = p.Key._UPDATE_BY,
                        UPDATE_DATE = p.Key._UPDATE_DATE,
                        APPs = p.GroupBy(i => new
                        {
                            _ID_APP = i.ID_APP,
                            _APP_NAME = i.APP_NAME,
                            _APP_URL = i.APP_URL,
                            _ICON = i.ICON,
                            _BG_COLOR = i.BG_COLOR,
                            _DESCRIPTION_APP = i.DESCRIPTION_APP,
                            _APP_GROUP = i.APP_GROUP,
                            _HARDWARE_CHECK = i.HARDWARE_CHECK,
                            _IS_ACTIVE = i.IS_ACTIVE
                        }).Select(n => new
                        {
                            ID = n.Key._ID_APP,
                            APP_NAME = n.Key._APP_NAME,
                            APP_URL = n.Key._APP_URL,
                            ICON = n.Key._ICON,
                            BG_COLOR = n.Key._BG_COLOR,
                            DESCRIPTION_APP = n.Key._DESCRIPTION_APP,
                            APP_GROUP = n.Key._APP_GROUP,
                            HARDWARE_CHECK = n.Key._HARDWARE_CHECK,
                            IS_ACTIVE = n.Key._IS_ACTIVE
                        }).ToList()
                    }).ToList();
                    #endregion
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK(tmp));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 3 - GET AVAILABLE STATIONS LIST
        [Route("get-ava-stations-lst")]
        [HttpGet]
        public HttpResponseMessage Get_Available_Stations_List(HttpRequestMessage request)
        {
            try
            {
                DataSet ds = ExecSPToListOracle("PR_A1_GET_STA_AVA_LST"); //PROC_GET_LIST_STA_AVA
                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<STATION> Received = DataHelper.DataTableToList<STATION>(ds.Tables[0]);
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK(Received));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 4 - GET STATION WITH DEVICE MAP LIST
        [Route("get-station-devices-map-lst")]
        [HttpGet]
        public HttpResponseMessage Get_Station_Devices_Map_List(HttpRequestMessage request)
        {
            try
            {
                DataSet ds = ExecSPToListOracle("PR_A1_GET_STA_DEV_MAP_LST");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<STATION_DEVICE_ITEM> Received = DataHelper.DataTableToList<STATION_DEVICE_ITEM>(ds.Tables[0]);
                    #region "LinQ statement"
                    var tmp = Received.GroupBy(i => new
                    {
                        station_id = i.ID,
                        station_code = i.STATION_CODE,
                        location_id = i.LOCATION_ID,
                        location_code = i.LOCATION_CODE,
                        description = i.DESCRIPTION,
                        ip_address = i.IP_ADDRESS,
                        status = i.STATUS
                    }).Select(p => new
                    {
                        ID = p.Key.station_id,
                        STATION_CODE = p.Key.station_code,
                        LOCATION_ID = p.Key.location_id,
                        LOCATION_CODE = p.Key.location_code,
                        DESCRIPTION = p.Key.description,
                        IP_ADDRESS = p.Key.ip_address,
                        STATUS = p.Key.status,
                        DEVICEs = p.GroupBy(e => new
                        {
                            deviceId = e.DEVICE_ID,
                            deviceTypeId = e.TYPE_ID,
                            typeCode = e.TYPE_CODE,
                            model = e.MODEL,
                            serialNo = e.SERIAL_NO,
                            des_device = e.DESCRIPTION_D,
                            canShare = e.CAN_SHARE,
                            capacity = e.CAPACITY,
                            minflow = e.MIN_FLOW,
                            maxflow = e.MAX_FLOW,
                            uom = e.UOM,
                            portname = e.PORTNAME,
                            baudrate = e.BAUDRATE,
                            databits = e.DATABITS,
                            parity = e.PARITY,
                            stopbit = e.STOPBIT,
                            net = e.NET,
                            ip = e.IP,
                            port = e.PORT,
                            newline = e.NEWLINE,
                            readtimeout = e.READ_TIMEOUT
                        })
                                        .Select(m => new DEVICE
                                        {
                                            ID = m.Key.deviceId,
                                            TYPE_ID = m.Key.deviceTypeId,
                                            TYPE_CODE = m.Key.typeCode,
                                            MODEL = m.Key.model,
                                            SERIAL_NO = m.Key.serialNo,
                                            DESCRIPTION = m.Key.des_device,
                                            CAN_SHARE = m.Key.canShare,
                                            CAPACITY = m.Key.capacity,
                                            MIN_FLOW = m.Key.minflow,
                                            MAX_FLOW = m.Key.maxflow,
                                            UOM = m.Key.uom,
                                            PORTNAME = m.Key.portname,
                                            BAUDRATE = m.Key.baudrate,
                                            DATABITS = m.Key.databits,
                                            PARITY = m.Key.parity,
                                            STOPBIT = m.Key.stopbit,
                                            NET = m.Key.net,
                                            IP = m.Key.ip,
                                            PORT = m.Key.port,
                                            NEWLINE = m.Key.newline,
                                            READ_TIMEOUT = m.Key.readtimeout
                                        }).Where(x => x.ID != 0).ToList()
                    }).ToList();
                    #endregion "end LinQ statement"
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK(tmp));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)    
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 5 - GET LOCATION STATIONs MAP LIST
        [Route("get-location-stations-map-lst")]
        [HttpGet]
        public HttpResponseMessage Get_Location_Stations_Map_List(HttpRequestMessage request)
        {
            try
            {
                DataSet ds = ExecSPToListOracle("PR_A1_GET_LOC_STA_MAP_LST");
                if (ds.Tables.Count > 0)
                {
                    List<LOCATION_STATION_ITEM> Received = DataHelper.DataTableToList<LOCATION_STATION_ITEM>(ds.Tables[0]);
                    #region "LinQ statement"
                    var tmp = Received.GroupBy(i => new
                    {
                        id = i.ID,
                        locationCode = i.LOCATION_CODE,
                        description = i.DESCRIPTION,
                        status = i.STATUS,
                        createBy = i.CREATE_BY,
                        createDate = i.CREATE_DATE,
                        updateBy = i.UPDATE_BY,
                        updateDate = i.CREATE_DATE
                    }).Select(p => new
                    {
                        ID = p.Key.id,
                        LOCATION_CODE = p.Key.locationCode,
                        DESCRIPTION = p.Key.description,
                        STATUS = p.Key.status,
                        CREATE_BY = p.Key.createBy,
                        CREATE_DATE = p.Key.createDate,
                        UPDATE_BY = p.Key.updateBy,
                        UPDATE_DATE = p.Key.createDate,
                        STATIONs = p.GroupBy(i => new
                        {
                            id = i.STATION_ID,
                            stationCode = i.STATION_CODE,
                            description = i.DESCRIPTION_STATION,
                            ipAddress = i.IP_ADDRESS,
                            status = i.STATUS_STATION
                        }).Select(n => new
                        {
                            ID = n.Key.id,
                            STATION_CODE = n.Key.stationCode,
                            DESCRIPTION = n.Key.description,
                            IP_ADDRESS = n.Key.ipAddress,
                            STATUS_STATION = n.Key.status
                        }).ToList()
                    }).ToList();
                    #endregion "end LinQ statement"
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(tmp));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK(ex));
            }
        }
        #endregion

        #region 6 - GET DEVICE TYPES LIST
        [Route("get-device-types-lst")] 
        [HttpGet]
        public HttpResponseMessage GetDeviceType(HttpRequestMessage request)
        {
            try
            {
                DataSet ds = ExecSPToListOracle("PR_A1_GET_DEV_TYPE_LST");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<DEVICE_TYPE> Received = DataHelper.DataTableToList<DEVICE_TYPE>(ds.Tables[0]);
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 7 - GET DEVICES LIST
        [Route("get-devices-lst")]
        [HttpGet]
        public HttpResponseMessage Get_Device_List(HttpRequestMessage request, bool available)
        {
            int isAvailable = available ? 1 : 0;
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("P_AVAILABLE", isAvailable);
            try
            {
                DataSet ds = ExecSPToListOracle("PR_A1_GET_DEV_AVA_LST", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<DEVICE> Received = DataHelper.DataTableToList<DEVICE>(ds.Tables[0]);
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 8 - CREATE NEW LOCATION
        [Route("create-new-location")]
        [HttpPost]
        public HttpResponseMessage Create_Location(HttpRequestMessage request, LOCATION obj)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("P_LOCATION_NAME", obj.LOCATION_CODE);
            parameters.Add("P_DESCRIPTION", obj.DESCRIPTION);
            parameters.Add("P_CREATE_BY", obj.CREATE_BY);

            try
            {
                DataSet ds = ExecSPToListOracle("PR_A1_INSERT_NEW_LOC", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<LOCATION> Received = DataHelper.DataTableToList<LOCATION>(ds.Tables[0]);
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.Created(Received));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 9 - CREATE NEW STATION
        [Route("create-new-station")]
        [HttpPost]
        public HttpResponseMessage Create_Station(HttpRequestMessage request, STATION obj)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("P_STATION_CODE", obj.STATION_CODE);
            parameters.Add("P_PROCESS_ID", obj.PROCESS_ID);
            parameters.Add("P_DESCRIPTION", obj.DESCRIPTION);
            parameters.Add("P_IP_ADDRESS", obj.IP_ADDRESS);
            parameters.Add("P_CREATE_BY", obj.CREATE_BY);
            try
            {
                DataSet ds = ExecSPToListOracle("PR_A1_INSERT_NEW_STA", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<STATION> Received = DataHelper.DataTableToList<STATION>(ds.Tables[0]);
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.Created(Received));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 10 - CREATE NEW DEVICE
        [Route("create-new-device")]
        [HttpPost]
        public HttpResponseMessage Create_Device(HttpRequestMessage request, [FromBody] DEVICE obj)
        {
            try
            {

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                //---- Device ----
                parameters.Add("P_TYPE_ID", obj.TYPE_ID);
                parameters.Add("P_TYPE_CODE", obj.TYPE_CODE);
                parameters.Add("P_MODEL", obj.MODEL);
                parameters.Add("P_SERIAL_NO", obj.SERIAL_NO);
                parameters.Add("P_DESCRIPTION", obj.DESCRIPTION);
                parameters.Add("P_CAN_SHARE", obj.CAN_SHARE ? 1 : 0);
                parameters.Add("P_CREATE_BY", obj.CREATE_BY);
                //---- Device Detail ----
                parameters.Add("P_CAPACITY", obj.CAPACITY);
                parameters.Add("P_MIN_FLOW", obj.MIN_FLOW);
                parameters.Add("P_MAX_FLOW", obj.MAX_FLOW);
                parameters.Add("P_UOM", obj.UOM);
                //---- Device Settings ----
                parameters.Add("P_PORTNAME", obj.PORTNAME);
                parameters.Add("P_BAUDRATE", obj.BAUDRATE);
                parameters.Add("P_DATABITS", obj.DATABITS);
                parameters.Add("P_PARITY", obj.PARITY);
                parameters.Add("P_STOPBIT", obj.STOPBIT);
                parameters.Add("P_NET", obj.NET ? 1 : 0);
                parameters.Add("P_IP", obj.IP);
                parameters.Add("P_PORT", obj.PORT);
                parameters.Add("P_NEWLINE", obj.NEWLINE);
                parameters.Add("P_READ_TIMEOUT", obj.READ_TIMEOUT);

                DataSet ds = ExecSPToListOracle("PR_A1_INSERT_NEW_DEV", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    DEVICE Received = DataHelper.DataTableToList<DEVICE>(ds.Tables[0]).FirstOrDefault();
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.Created(Received));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 00 - GET UOM LIST
        [Route("get-uom-lst")]
        [HttpGet]
        public HttpResponseMessage GetUOMList(HttpRequestMessage request)
        {
            try
            {
                DataSet ds = ExecSPToListOracle("PR_GET_UOM");

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<UOM> Received = DataHelper.DataTableToList<UOM>(ds.Tables[0]);
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK(ex));
            }
        }
        #endregion

        #region 11 - CREATE STATION TO LOCATION MAPPING
        [Route("create-location-station-map")]
        [HttpPost]
        public HttpResponseMessage Create_Location_Station_Map(HttpRequestMessage request, [FromBody] LOCATION_STATION_MAP obj)
        {
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_LOCATION_ID", obj.LOCATION_ID);
                parameters.Add("P_STATION_ID", obj.STATION_ID);
                parameters.Add("P_CREATE_BY", obj.CREATE_BY);

                DataSet ds = ExecSPToListOracle("PR_A1_CREATE_LOC_STA_MAP", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    LOCATION_STATION_MAP Received = DataHelper.DataTableToList<LOCATION_STATION_MAP>(ds.Tables[0]).FirstOrDefault();
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.Created(Received));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 12 - DISPOSAL STATION TO LOCATION MAPPING
        [Route("disposal-location-station-map")]
        [HttpPost]
        public HttpResponseMessage Remove_Location_Station_Map(HttpRequestMessage request, [FromBody] LOCATION_STATION_MAP obj)
        {
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_LOCATION_ID", obj.LOCATION_ID);
                parameters.Add("P_STATION_ID", obj.STATION_ID);
                parameters.Add("P_UPDATE_BY", obj.UPDATE_BY);

                ExecuteNonQueryOracle("PR_A1_DISPOSAL_LOC_STA_MAP", parameters);

                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK());
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 13 - CREATE APP TO STATION MAPPING
        [Route("create-station-app-map")]
        [HttpPost]
        public HttpResponseMessage Create_Station_App_Map(HttpRequestMessage request, [FromBody]APP_STATION_MAP obj)
        {
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_APP_ID", obj.APP_ID);
                parameters.Add("P_STATION_ID", obj.STATION_ID);
                parameters.Add("P_CREATE_BY", obj.CREATE_BY);

                DataSet ds = ExecSPToListOracle("PR_A1_CREATE_STA_APP_MAP", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<APP_STATION_MAP> Received = DataHelper.DataTableToList<APP_STATION_MAP>(ds.Tables[0]);
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.Created(Received));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 14 - DISPOSAL APP STATION MAP
        [Route("disposal-station-app-map")]
        [HttpPost]
        public HttpResponseMessage Disposal_Station_App_Map(HttpRequestMessage request, [FromBody]APP_STATION_MAP obj)
        {
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_STATION_ID", obj.STATION_ID);
                parameters.Add("P_APP_ID", obj.APP_ID);
                parameters.Add("P_UPDATE_BY", obj.CREATE_BY);

                ExecuteNonQueryOracle("PR_A1_DISPOSAL_STA_APP_MAP", parameters);

                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK());
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 15 - CREATE DEVICES TO STATION MAPPING
        [Route("create-station-device-map")]
        [HttpPost]
        public HttpResponseMessage Create_Station_Device_Map(HttpRequestMessage request, [FromBody]STATION_DEVICE_MAP obj)
        {
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_STATION_ID", obj.STATION_ID);
                parameters.Add("P_DEVICE_ID", obj.DEVICE_ID);
                parameters.Add("P_CREATE_BY", obj.CREATE_BY);

                DataSet ds = ExecSPToListOracle("PR_A1_CREATE_STA_DEV_MAP", parameters);

                if (ds.Tables.Count > 0)
                {
                    STATION_DEVICE_MAP Received = DataHelper.DataTableToList<STATION_DEVICE_MAP>(ds.Tables[0]).FirstOrDefault();
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.Created(Received));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 16 - DISPOSAL DEVICE STATION MAP
        [Route("disposal-station-device-map")]
        [HttpPost]
        public HttpResponseMessage Disposal_Station_Device_Map(HttpRequestMessage request, [FromBody]STATION_DEVICE_MAP obj)
        {
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_STATION_ID", obj.STATION_ID);
                parameters.Add("P_DEVICE_ID", obj.DEVICE_ID);
                parameters.Add("P_UPDATE_BY", obj.UPDATE_BY);

                ExecuteNonQueryOracle("PR_A1_DISPOSAL_STA_DEV_MAP", parameters);

                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK());

            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 17 - GET LOAD DEVICE LIST BY IP ADDR
        [Route("get-station-info-by-ipaddr")]
        [HttpGet]
        public HttpResponseMessage Get_Station_Info_By_IpAddr(HttpRequestMessage request)
        {
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_IP_ADDRESS", request.Headers.Contains("IP_ADDRESS")? 
                        request.Headers.GetValues("IP_ADDRESS").FirstOrDefault() : clientAddress); 

                DataSet ds = ExecSPToListOracle("PR_A1_GET_STA_INF_BY_IP_ADDR", parameters);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<STATION_DEVICE_ITEM> Received = DataHelper.DataTableToList<STATION_DEVICE_ITEM>(ds.Tables[0]);
                    #region 'LinQ statement'
                    var tmp = Received
                                    .GroupBy(i => new
                                    {
                                        station_id = i.ID,
                                        station_code = i.STATION_CODE,
                                        location_id = i.LOCATION_ID,
                                        location_code = i.LOCATION_CODE,
                                        description = i.DESCRIPTION,
                                        ip_address = i.IP_ADDRESS,
                                        status = i.STATUS
                                    })
                                    .Select(p => new
                                    {
                                        ID = p.Key.station_id,
                                        STATION_CODE = p.Key.station_code,
                                        LOCATION_ID = p.Key.location_id,
                                        LOCATION_CODE = p.Key.location_code,
                                        DESCRIPTION = p.Key.description,
                                        IP_ADDRESS = p.Key.ip_address,
                                        STATUS = p.Key.status,
                                        DEVICEs = p.GroupBy(e => new
                                        {
                                            deviceId = e.DEVICE_ID,
                                            deviceTypeId = e.TYPE_ID,
                                            typeCode = e.TYPE_CODE,
                                            model = e.MODEL,
                                            serialNo = e.SERIAL_NO,
                                            des_device = e.DESCRIPTION_D,
                                            canShare = e.CAN_SHARE,
                                            capacity = e.CAPACITY,
                                            minflow = e.MIN_FLOW,
                                            maxflow = e.MAX_FLOW,
                                            uom = e.UOM,
                                            portname = e.PORTNAME,
                                            baudrate = e.BAUDRATE,
                                            databits = e.DATABITS,
                                            parity = e.PARITY,
                                            stopbit = e.STOPBIT,
                                            net = e.NET,
                                            ip = e.IP,
                                            port = e.PORT,
                                            newline = e.NEWLINE,
                                            readtimeout = e.READ_TIMEOUT
                                        })
                                        .Select(m => new DEVICE
                                        {
                                            ID = m.Key.deviceId,
                                            TYPE_ID = m.Key.deviceTypeId,
                                            TYPE_CODE = m.Key.typeCode,
                                            MODEL = m.Key.model,
                                            SERIAL_NO = m.Key.serialNo,
                                            DESCRIPTION = m.Key.des_device,
                                            CAN_SHARE = m.Key.canShare,
                                            CAPACITY = m.Key.capacity,
                                            MIN_FLOW = m.Key.minflow,
                                            MAX_FLOW = m.Key.maxflow,
                                            UOM =m.Key.uom,
                                            PORTNAME = m.Key.portname,
                                            BAUDRATE = m.Key.baudrate,
                                            DATABITS = m.Key.databits,
                                            PARITY = m.Key.parity,
                                            STOPBIT = m.Key.stopbit,
                                            NET = m.Key.net,
                                            IP = m.Key.ip,
                                            PORT = m.Key.port,
                                            NEWLINE = m.Key.newline,
                                            READ_TIMEOUT = m.Key.readtimeout                                          
                                        }).Where(x=>x.ID!=0).ToList()
                                    }).ToList().First();
                    #endregion 'end LinQ statement'
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(tmp));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 18 - GET STATION TOLERANCE
        [Route("get-station-tolerance")]
        [HttpGet]
        public HttpResponseMessage Get_Station_Tolerance(HttpRequestMessage request,string code)
        {
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_TOLERANCE_CODE", code);

                DataSet ds = ExecSPToListOracle("PR_A1_GET_STA_TOLERANCE", parameters);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<TOLERANCE_POINT> Received = DataHelper.DataTableToList<TOLERANCE_POINT>(ds.Tables[0]);
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 19 - UPDATE STATION INFO
        [Route("update-station-info")]
        [HttpPost]
        public HttpResponseMessage Update_Station_Info(HttpRequestMessage request, STATION obj)
        {
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_ID", obj.ID);
                parameters.Add("P_STATION_CODE", obj.STATION_CODE);
                parameters.Add("P_PROCESS_ID", obj.PROCESS_ID);
                parameters.Add("P_DESCRIPTION", obj.DESCRIPTION);
                parameters.Add("P_IP_ADDRESS", obj.IP_ADDRESS);
                parameters.Add("P_UPDATE_BY", obj.UPDATE_BY);

                ExecuteNonQueryOracle("PR_A1_UPDATE_STA_INF", parameters);

                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK());
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

        #region 20 - UPDATE DEVICE INFO
        [Route("update-device-info")]
        [HttpPost]
        public HttpResponseMessage Update_Device_Info(HttpRequestMessage request, DEVICE obj)
        {
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_ID", obj.ID);
                parameters.Add("P_TYPE_ID", obj.TYPE_ID);
                parameters.Add("P_TYPE_CODE", obj.TYPE_CODE);
                parameters.Add("P_MODEL", obj.MODEL);
                parameters.Add("P_SERIAL_NO", obj.SERIAL_NO);
                parameters.Add("P_DESCRIPTION", obj.DESCRIPTION);
                parameters.Add("P_CAN_SHARE", obj.CAN_SHARE);

                parameters.Add("P_CAPACITY", obj.CAPACITY);
                parameters.Add("P_MIN_FLOW", obj.MIN_FLOW);
                parameters.Add("P_MAX_FLOW", obj.MAX_FLOW);
                parameters.Add("P_UOM", obj.UOM);

                parameters.Add("P_PORTNAME", obj.PORTNAME);
                parameters.Add("P_BAUDRATE", obj.BAUDRATE);
                parameters.Add("P_DATABITS", obj.DATABITS);
                parameters.Add("P_PARITY", obj.PARITY);
                parameters.Add("P_STOPBIT", obj.STOPBIT);
                parameters.Add("P_NEWLINE", obj.NEWLINE);
                parameters.Add("P_READ_TIMEOUT", obj.READ_TIMEOUT);

                parameters.Add("P_NET", obj.NET);
                parameters.Add("P_IP", obj.IP);
                parameters.Add("P_PORT", obj.PORT);


                parameters.Add("P_UPDATE_BY", obj.UPDATE_BY);

                ExecuteNonQueryOracle("PR_A1_UPDATE_DEV_INF", parameters);

                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK());
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }
        #endregion

    }
}
