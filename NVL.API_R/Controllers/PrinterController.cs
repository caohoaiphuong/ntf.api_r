﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Sockets;
using NVL.API_R.Base;
using NVL.API_R.Models;
using NVL.API_R.Helper;
using System.IO;

namespace NVL.API_R.Controllers
{
    [RoutePrefix("api/print")]
    public class PrinterController : BaseController
    {
        [Route("zpl-print-barcode")]
        [HttpPost]
        public HttpResponseMessage Zpl_Print_Bacode(HttpRequestMessage Request, [FromBody] PrintBarcode obj)
        {
            PrinterInfo printerInfo = new PrinterInfo();
            try
            {
                if (obj != null)
                {
                    clientAddress = Request.Headers.Contains("ipaddress") ?
                                Request.Headers.GetValues("ipaddress").FirstOrDefault() : clientAddress;


                    printerInfo = GetPrinterInfo(clientAddress);
                    if (printerInfo != null)
                    {
                        switch (obj.PROCESS_CODE.ToUpper())
                        {
                            case "BMNL":
                                {
                                    ITEM_BMNL_INFO printBMNL = GET_BARCODE_BMNL_INFO(obj.PROCESS_CODE.ToUpper(), obj.BARCODE);
                                    if (printBMNL != null)
                                    {
                                        int i=0;
                                        int tmp = obj.QTY_PRINT;
                                        try
                                        {
                                            obj.QTY_PRINT = i;
                                            for (i = 0; i < tmp; i++)
                                            {
#if !DEBUG
                                                Do_Print(printerInfo.IP, printerInfo.PORT, printBMNL.PRINTER_ZPL);
#endif
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            throw;
                                        }
                                        finally
                                        {
                                            if (i > 0)
                                            {
                                                try
                                                {
                                                    Dictionary<string, object> parameters = new Dictionary<string, object>();
                                                    parameters.Add("P_PROCESS_CODE", obj.PROCESS_CODE);
                                                    parameters.Add("P_BARCODE_ID", obj.BARCODE);
                                                    parameters.Add("P_PRINT_QTY", i);
                                                    parameters.Add("P_CREATE_BY", obj.USERNAME);
                                                    parameters.Add("P_USER_IPADDR", clientAddress);
                                                    parameters.Add("P_PRINTER_IPADDR", printerInfo.IP);

                                                    ExecuteNonQueryOracle("PR_P1_CRE_BRC_PRT_LOG", parameters);
                                                }
                                                catch (Exception)
                                                {
#if DEBUG
                                                    throw;
#endif
                                                    //Do no thing
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound("Not found BARCODE ID."));
                                    }
                                }
                                break;
                            case "BMWE":
                                {
                                    ITEM_BMWE_DATA printBMWE = GET_BARCODE_BMWE_INFO(obj.PROCESS_CODE.ToUpper(), 169, obj.BARCODE);
                                    if (printBMWE != null)
                                    {
                                        int i=0;
                                        int tmp = obj.QTY_PRINT;
                                        try
                                        {
                                            obj.QTY_PRINT = i;
                                            for (i = 0; i < tmp ; i++)
                                            {
#if !DEBUG
                                                Do_Print(printerInfo.IP, printerInfo.PORT, printBMWE.PRINTER_ZPL);
#endif
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            throw;
                                        }
                                        finally
                                        {
                                            if (i>0)
                                            {
                                                try
                                                {
                                                    Dictionary<string, object> parameters = new Dictionary<string, object>();
                                                    parameters.Add("P_PROCESS_CODE", obj.PROCESS_CODE);
                                                    parameters.Add("P_BARCODE_ID", obj.BARCODE);
                                                    parameters.Add("P_PRINT_QTY", i);
                                                    parameters.Add("P_CREATE_BY", obj.USERNAME);
                                                    parameters.Add("P_USER_IPADDR", clientAddress);
                                                    parameters.Add("P_PRINTER_IPADDR", printerInfo.IP);

                                                    ExecuteNonQueryOracle("PR_P1_CRE_BRC_PRT_LOG", parameters);
                                                }
                                                catch (Exception)
                                                {
#if DEBUG
                                                throw;
#endif
                                                    //Do no thing
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound("Not Found BARCODE ID."));
                                    }
                                }
                                break;
                            case "BMTP":
                                ITEM_BMTP_DATA printBMTP = GET_BARCODE_BMTP_INFO(obj.PROCESS_CODE.ToUpper(), obj.BARCODE);
                                if (printBMTP != null)
                                {
                                    int i = 0;
                                    int tmp = obj.QTY_PRINT;
                                    try
                                    {
                                        obj.QTY_PRINT = i;
                                        for (i = 0; i < tmp ; i++)
                                        {
#if !DEBUG
                                            Do_Print(printerInfo.IP, printerInfo.PORT, printBMTP.PRINTER_ZPL);
#endif
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        throw;
                                    }
                                    finally
                                    {
                                        if (i>0)
                                        {
                                            try
                                            {
                                                Dictionary<string, object> parameters = new Dictionary<string, object>();
                                                parameters.Add("P_PROCESS_CODE", obj.PROCESS_CODE);
                                                parameters.Add("P_BARCODE_ID", obj.BARCODE);
                                                parameters.Add("P_PRINT_QTY", i);
                                                parameters.Add("P_CREATE_BY", obj.USERNAME);
                                                parameters.Add("P_USER_IPADDR", clientAddress);
                                                parameters.Add("P_PRINTER_IPADDR", printerInfo.IP);

                                                ExecuteNonQueryOracle("PR_P1_CRE_BRC_PRT_LOG", parameters);
                                            }
                                            catch (Exception)
                                            {
#if DEBUG
                                            throw;
#endif
                                                //Do no thing
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound("Not Found BARCODE ID."));
                                }
                                break;
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK(obj));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound("Không tìm thấy máy in."));
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, DataHelper.NotFound("Json Object is null"));
                }
            }
            catch (TimeoutException ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(string.Format("Đã in {0} nhãn. Lỗi không thể kết nối đến máy in @ {1}:{2}", obj.QTY_PRINT ,printerInfo.IP, printerInfo.PORT)));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
        }

        private PrinterInfo GetPrinterInfo(string IP_ADDRESS)
        {
            if (!string.IsNullOrEmpty(IP_ADDRESS))
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_IP_ADDRESS", IP_ADDRESS);

                DataSet ds = ExecSPToListOracle("PR_GET_PRINTER_INFO", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    PrinterInfo printer = new PrinterInfo();
                    List<PrinterInfo> Received = DataHelper.DataTableToList<PrinterInfo>(ds.Tables[0]);
                    foreach (PrinterInfo prt in Received)
                    {
                        if (prt.TYPE_CODE == "PRINTER")
                        {
                            return prt;
                        }
                    }
                }
            }
            return null;
        }

        private ITEM_BMNL_INFO GET_BARCODE_BMNL_INFO(string ProcessCode, uint Barcode_Id)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("P_PROCESS_CODE", ProcessCode);
            parameters.Add("P_BARCODE", Barcode_Id);

            DataSet ds = ExecSPToListOracle("PR_P1_GET_BMNL_BRC_INF", parameters);
            
            if (ds.Tables.Count > 0)
            {
                List<ITEM_BMNL_INFO> Received = DataHelper.DataTableToList<ITEM_BMNL_INFO>(ds.Tables[0]);
                ITEM_BMNL_INFO ItemData = Received.FirstOrDefault();
                if (ItemData!=null)
                {
                    string ItemShortName = FormatHelper.GetShortName(FormatHelper.FilterVietkey(ItemData.ITEM_NAME));
                    string FirtStringPart = "";
                    string SecondStringPart = "";
                    FormatHelper.Make2LineString(ItemShortName, 32, 30, out FirtStringPart,out SecondStringPart);
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-MASTER_CODE-", ItemData.MASTER_CODE);
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-ITEM_SHORTNAME_LINE_1-", FirtStringPart);
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-ITEM_SHORTNAME_LINE_2-", SecondStringPart);
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-LOT_NO-", ItemData.LOT_NUMBER);
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-SPKT-", ItemData.SPKT_NO);
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-VOLUME-", (ItemData.VOLUME_OF_PACKAGE * 1000).ToString(FormatHelper.specifier, FormatHelper.culture));
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-UOM-", "g");
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-HDVSX-", ItemData.HSD.ToString("dd/MM/yyyy"));
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-BARCODE-", string.Format("{0}{1:D11}", ProcessCode, Barcode_Id));

                    return ItemData;
                }
            }

            return null;
        }

        private ITEM_BMWE_DATA GET_BARCODE_BMWE_INFO(string ProcessCode, uint Org_Id, uint Barcode_Id)
        {

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("P_PROCESS_CODE", ProcessCode);
            parameters.Add("P_BARCODE", Barcode_Id);

            DataSet ds = ExecSPToListOracle("PR_P1_GET_BMWE_BRC_INF", parameters); //PR_GET_BARCODE_BMWE_INFO

            if (ds.Tables[0].Rows.Count == 1)
            {
                List<ITEM_BMWE_DATA> Received = DataHelper.DataTableToList<ITEM_BMWE_DATA>(ds.Tables[0]);
                ITEM_BMWE_DATA ItemData = Received.FirstOrDefault();
                if (ItemData != null)
                {
                    string ItemShortName = FormatHelper.GetShortName(FormatHelper.FilterVietkey(ItemData.ITEM_NAME));
                    string FirtStringPart = "";
                    string SecondStringPart = "";
                    FormatHelper.Make2LineString(ItemShortName, 32, 30, out FirtStringPart, out SecondStringPart);
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-MASTER_CODE-", ItemData.MASTER_CODE);
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-ITEM_SHORTNAME_LINE_1-", FirtStringPart);
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-ITEM_SHORTNAME_LINE_2-", SecondStringPart);
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-CT_VOLUME-", (ItemData.ST_WEIGH * 1000).ToString(FormatHelper.specifier, FormatHelper.culture));
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-TT_VOLUME-", (ItemData.NET_WEIGH * 1000).ToString(FormatHelper.specifier, FormatHelper.culture));
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-UOM-", "g");
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-BATCH_CODE-", ItemData.BATCH_CODE);
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-ORDER_NO-", ItemData.ORDER_ID.ToString());
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-PACK_QTY-", ItemData.PACKAGE_QTY);
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-BARCODE-", string.Format("{0}{1:D11}", ProcessCode, Barcode_Id));

                    return ItemData;
                }
            }

            return null;
        }

        private ITEM_BMTP_DATA GET_BARCODE_BMTP_INFO(string ProcessCode, uint Barcode_Id)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("P_PROCESS_CODE", ProcessCode);
            parameters.Add("P_BARCODE", Barcode_Id);

            DataSet ds = ExecSPToListOracle("PR_P1_GET_BMTP_BRC_INF", parameters); // PR_GET_BARCODE_INFO_BMTP

            if (ds.Tables.Count > 0)
            {
                List<ITEM_BMTP_DATA> Received = DataHelper.DataTableToList<ITEM_BMTP_DATA>(ds.Tables[0]);
                ITEM_BMTP_DATA ItemData = Received.FirstOrDefault();
                if (ItemData != null)
                {
                    string ItemShortName = FormatHelper.GetShortName(FormatHelper.FilterVietkey(ItemData.PRODUCT_NAME));
                    string FirtStringPart = "";
                    string SecondStringPart = "";
                    FormatHelper.Make2LineString(ItemShortName, 32, 30, out FirtStringPart, out SecondStringPart);
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-PRODUCT_NAME_LINE_1-", FirtStringPart);
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-PRODUCT_NAME_LINE_2-", SecondStringPart);
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-VOLUME-", (ItemData.NET_WEIGH * 1000).ToString(FormatHelper.specifier, FormatHelper.culture));
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-UOM-", "g");
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-HSDBOTME-", ItemData.EXP_DATE.ToString("dd/MM/yyyy"));
                    ItemData.PRINTER_ZPL = Regex.Replace(ItemData.PRINTER_ZPL, "-BARCODE-", string.Format("{0}{1:D11}", ProcessCode, Barcode_Id));

                    return ItemData;
                }
            }

            return null;
        }

        private bool Do_Print(string _IP_ADDRESS, int _PORT, string _CONTENT)
        {
            try
            {
                TcpClient ZebraClient = new TcpClient();
                int PORT = _PORT;

                ZebraClient.SendTimeout = 1000;
                ZebraClient.ReceiveTimeout = 1000;
                if (ZebraClient.ConnectAsync(_IP_ADDRESS, PORT).Wait(3000))
                {
                    NetworkStream myNetStream = ZebraClient.GetStream();
                    StreamReader myStreamReader = new StreamReader(myNetStream);
                    StreamWriter myStreamWriter = new StreamWriter(myNetStream);
                    myStreamWriter.WriteLine(_CONTENT);
                    myStreamWriter.Flush();
                    ZebraClient.Close();
                    return true;
                }
                else
                {
                    throw new TimeoutException();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}