﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NVL.API_R.Base;
using NVL.API_R.Models;
using NVL.API_R.Helper;
using NVL.API_R.common.Model;

namespace NVL.API_R.Controllers
{
    [RoutePrefix("api/common")]
    public class CommonController : BaseController
    {

        [Route("exp-test")]
        [HttpGet]
        public HttpResponseMessage GetException(HttpRequestMessage Request, int num)
        {
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_EXCEPTION_NO", num);
                DataSet DS = ExecSPToListOracle("PPROC_EXCEPTION_TEST", parameters);
                if (DS.Tables[0].Rows.Count != 0)
                {
                    DataTable dt = DS.Tables[0];
                    List<string> tmp = new List<string>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        tmp.Add(dr["APP_GROUP"].ToString());
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, Helper.DataHelper.OK(tmp));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, Helper.DataHelper.NotFound());
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Helper.DataHelper.InternalError(ex));
            }
        }

        #region Cân Chia NL - Lấy WO theo Batch Id và IP address
        [Route("get-dev-lst-by-ip")]
        [HttpGet]
        public HttpResponseMessage Get_Device_By_IP_ADDRESS(HttpRequestMessage request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_IP_ADDRESS", request.Headers.Contains("IP_ADDRESS") ?
                                request.Headers.GetValues("IP_ADDRESS").FirstOrDefault() : clientAddress);
                DataSet ds = ExecSPToListOracle("PR_Z1_GET_DEV_LST_BY_IP", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<DEVICE> Received = DataHelper.DataTableToList<DEVICE>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound(""));
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region Thông tin title 
        [Route("get-app-tile")]
        [HttpGet]
        public HttpResponseMessage Get_tile(HttpRequestMessage request, string TITLE_CODE)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_TILE_CODE", TITLE_CODE);

                DataSet ds = ExecSPToListOracle("PR_G1_GET_APP_TITLE", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<APP_TITLE> Received = DataHelper.DataTableToList<APP_TITLE>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received.FirstOrDefault().TITLE_CONTENT));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound(""));
                }
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

    }
}
