﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NVL.API_R.Base;
using NVL.API_R.Models;
using NVL.API_R.Helper;

namespace NVL.API_R.Controllers
{
    [RoutePrefix("api/nvl")]
    public class InputNVLController : BaseController
    {
        #region 1 - GET AVAILABLE PXK LIST
        [Route("get-ava-pxk-lst")]
        [HttpGet]
        public HttpResponseMessage Get_Available_PXK(HttpRequestMessage request, int org_id)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_ORGANIZATION_ID", org_id);
                DataSet ds = ExecSPToListOracle("PR_I1_GET_LST_PXK", parameters);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<PXK_INFO> Received = DataHelper.DataTableToList<PXK_INFO>(ds.Tables[0]);
                    var list = Received.GroupBy(i => new
                                                {
                                                    pxk = i.PXK_NO,
                                                    create_date = i.CREATION_DATE
                                                })
                                                .Select(p => new
                                                {
                                                    SPXK = p.Key.pxk,
                                                    CREATION_DATE = p.Key.create_date
                                                }).ToList();
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received));
                }
                else
                {
                    response =  Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                response =  Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

        #region 2 - GET NVL LIST OF PXK

        [Route("get-nvl-list-by-pxkno")]
        [HttpGet]
        public HttpResponseMessage Get_NVL_List_By_PXK(HttpRequestMessage request, string spxk, string organization_id)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_SPXK", spxk);
                parameters.Add("P_ORGANIZATION_ID", organization_id);

                DataSet ds = ExecSPToListOracle("PR_I1_GET_NVL_ITM_LST_BY_PXK", parameters);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<NVL_INFO> Received = DataHelper.DataTableToList<NVL_INFO>(ds.Tables[0]);
                    #region LinQ Statement
                    var pxk = Received.GroupBy(u => u.PXK_NO)
                                        .Select(p => new
                                        {
                                            PXK_CODE = p.Key,
                                            ITEMS = p.GroupBy(i => new
                                            {
                                                masterCode = i.MASTER_CODE,
                                                itemName = i.ITEM_NAME,
                                                inventoryItemId = i.INVENTORY_ITEM_ID,
                                            })
                                            .Select(m => new
                                            {
                                                MASTER_CODE = m.Key.masterCode,
                                                ITEM_NAME = m.Key.itemName,
                                                INVENTORY_ITEM_ID = m.Key.inventoryItemId,
                                                LOTs = m.GroupBy(l => new
                                                {
                                                    lotNumber = l.LOT_NUMBER,
                                                }).Select(a => new {
                                                    LOT_NO = a.Key.lotNumber,
                                                    SPKTs = a.GroupBy(n => new {
                                                        transVolume = n.TRANSACTION_QUANTITY,
                                                        inputVolume = n.INPUTED_VOLUME,
                                                        transactionErpId = n.TRANSACTION_ERP_ID,
                                                        spkt = n.SPKT_NO,
                                                        hdvsx = n.HDVSX,
                                                        hsd = n.HSD,
                                                        status = n.STATUS
                                                    }).Select(b => new {
                                                        TRANSACTION_QUANTITY = b.Key.transVolume,
                                                        INPUTED_VOLUME = b.Key.inputVolume,
                                                        TRANSACTION_ERP_ID = b.Key.transactionErpId,
                                                        SPKT_NO = b.Key.spkt,
                                                        HDVSX = b.Key.hdvsx,
                                                        HSD = b.Key.hsd,
                                                        STATUS = b.Key.status
                                                    }).ToList()
                                                }).ToList()
                                            }).ToList()
                                        }).FirstOrDefault();
                    #endregion
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.OK(pxk));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }

        #endregion

        #region 3 - INPUT NVL BOT ME SUA BOT

        [Route("input-nvl-bm-suabot")]
        [HttpPost]
        public HttpResponseMessage Create_MTL_Transaction(HttpRequestMessage request, NVL_MTL_TRANS_CREATION obj)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_PROCESS_CODE", obj.PROCESS_CODE);
                parameters.Add("P_TRANSACTION_ERP_ID", obj.TRANSACTION_ERP_ID);
                parameters.Add("P_INVENTORY_ITEM_ID", obj.INVENTORY_ITEM_ID);
                parameters.Add("P_PXK_NO", obj.PXK_NO);
                parameters.Add("P_LOT_NO", obj.LOT_NO);
                parameters.Add("P_SPKT_NO", obj.SPKT_NO);
                parameters.Add("P_FORM_TYPE", obj.FORM_TYPE ? 1 : 0);
                parameters.Add("P_FORM_QUANTITY", obj.FORM_QUANTITY);
                parameters.Add("P_FORM_COUNT", obj.FORM_COUNT);
                parameters.Add("P_TRANSACTION_QUANTITY", obj.TRANSACTION_QUANTITY);
                parameters.Add("P_TRANSACTION_ACTION", obj.TRANSACTION_ACTION);
                parameters.Add("P_TRANSACTION_TYPE", obj.TRANSACTION_TYPE);
                parameters.Add("P_TRANSACTION_STATUS", obj.TRANSACTION_STATUS);
                parameters.Add("P_OWNER_ORG_ID", obj.OWNER_ORG_ID);
                parameters.Add("P_TRANSFER_ORG_ID", obj.TRANSFER_ORG_ID);
                parameters.Add("P_CREATE_BY", obj.CREATE_BY);
                DataSet ds = ExecSPToListOracle("PR_I1_CREATE_MTL_TRANS", parameters);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<NVL_MTL_TRANS_ITEM> Recieved = DataHelper.DataTableToList<NVL_MTL_TRANS_ITEM>(ds.Tables[0]);
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.Created(Recieved.FirstOrDefault()));
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.OK, DataHelper.NotFound("db error"));
                }
            }

            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }

        #endregion

        #region 4 - GET INPUTED TRANSACTION BY DATE AND PROCESS
        [Route("get-trans-by-date-and-process")]
        [HttpGet]
        public HttpResponseMessage Get_TransactionBy_Date_And_Process(HttpRequestMessage request, DateTime FromDate, DateTime ToDate ,string PROCESS_CODE)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("P_FROM_DATE", FromDate);
                parameters.Add("P_TO_DATE", ToDate);
                parameters.Add("P_PROCESS_CODE", PROCESS_CODE);

                DataSet ds = ExecSPToListOracle("PR_I1_GET_MTL_TRANS_BY_DATE", parameters); //PR_I1_GET_NVL_BY_DATE
                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<NVL_MTL_TRANS_ITEM> Received = DataHelper.DataTableToList<NVL_MTL_TRANS_ITEM>(ds.Tables[0]);
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.LoadSuccess(Received));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.NoDataFound());
                }
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, DataHelper.InternalError(ex));
            }
            return response;
        }
        #endregion

    }
}
