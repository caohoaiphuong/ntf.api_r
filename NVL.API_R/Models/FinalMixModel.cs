﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NVL.API_R.Models
{
    public class FNL_BATCH_HDR_INF_DATA
    {
        public uint STT { get; set; }
        public string BATCH_CODE { get; set; }
        public uint BATCH_ID { get; set; }
        public int BATCH_STATUS { get; set; }
        public uint FORMULA_ID { get; set; }
        public string FORMULA_NO { get; set; }
        public int FORMULA_VERS { get; set; }
        public string FORMULA_DESC { get; set; }
        public string RECIPE_NO { get; set; }
        public uint RECIPE_VERS { get; set; }
        public string RECIPE_DESC { get; set; }
        public decimal KHSX { get; set; }
        public DateTime AFFECTED_DATE { get; set; }
        public DateTime PLAN_START_DATE { get; set; }
    }

    public class FINAL_BATCH_DTL_INFO_DATA
    {
        public string BATCH_CODE { get; set; }
        public uint BATCH_ID { get; set; }
        public int BATCH_STATUS { get; set; }
        public uint FORMULA_ID { get; set; }
        public int FORMULA_VERS { get; set; }
        public string FORMULA_DESC { get; set; }
        public string RECIPE_NO { get; set; }
        public uint RECIPE_VERS { get; set; }
        public string RECIPE_DESC{ get; set; }
        public uint LINE_NO { get; set; }
        public uint INVENTORY_ITEM_ID { get; set; }
        public string MASTER_CODE { get; set; }
        public string ITEM_NAME { get; set; }
        public decimal PRIMARY_QTY { get; set; }
        public decimal? QUI_CACH { get; set; }
        public uint INVENTORY_ITEM_ID_SUB { get; set; }
        public string MASTER_CODE_SUB { get; set; }
        public string ITEM_NAME_SUB { get; set; }
        public string SUBSTITUTION_DESC { get; set; }
    }

    public class FNL_BATCH_DTL_INFO_MD
    {
        public int STT { get; set; }
        public string BATCH_CODE { get; set; }
        public uint BATCH_ID { get; set; }
        public int BATCH_STATUS { get; set; }
        public uint FORMULA_ID { get; set; }
        public int FORMULA_VERS { get; set; }
        public string FORMULA_DESC { get; set; }
        public string RECIPE_NO { get; set; }
        public uint RECIPE_VERS { get; set; }
        public string RECIPE_DESC { get; set; }
        public int ITEMS_SUM
        {
            get
            {
                if (MIX_DTL_LIST == null)
                {
                    return 0;
                }
                else
                {
                    return MIX_DTL_LIST.Count();
                }
            }
        }
        public List<MIX_DTL_INFO_MD> MIX_DTL_LIST { get; set; }
    }

    public class MIX_DTL_INFO_MD
    {
        public uint LINE_NO { get; set; }
        public uint INVENTORY_ITEM_ID { get; set; }
        public string MASTER_CODE { get; set; }
        public string ITEM_NAME { get; set; }
        public decimal PRIMARY_QTY { get; set; }
        //public decimal? QUI_CACH { get; set; }
        //public decimal TOTAL_QTY { get; set; }
        //public string DUONG { get; set; }
        public uint INVENTORY_ITEM_ID_SUB { get; set; }
        public string MASTER_CODE_SUB { get; set; }
        public string ITEM_NAME_SUB { get; set; }
        public string SUBSTITUTION_DESC { get; set; }
        //public int NL_QTY { get; set; }
        //public string NL_UOM { get; set; }
        //public decimal NL_WEIGH { get; set; }
        //public int BS_QTY { get; set; }
        //public string BS_UOM { get; set; }
        //public decimal BS_WEIGH { get; set; }
    }


    /// <summary>
    /// CREATE MIX RECORD
    /// </summary>

    public class MIXING_CREATION
    {
        public uint BATCH_ID { get; set; }
        public uint ORGANIZATION_ID { get; set; }
        public uint BATCH_QTY { get; set; }
        public string PROCESS_CODE { get; set; }
        public string CREATE_BY { get; set; }
        public uint LOCATION_ID { get; set; }
    }

    public class FNL_MIX_RECORD_DATA
    {
        public uint MIXING_ID { get; set; }
        public uint BATCH_ID { get; set; }
        public uint FORMULA_ID { get; set; }
        public uint RECIPE_ID { get; set; }
        public uint BATCH_QTY { get; set; }
        public uint M_STATUS { get; set; }
        //==========================================
        public uint MIXING_DTL_ID { get; set; }
        public uint LINE_NO { get; set; }
        public uint INVENTORY_ITEM_ID { get; set; }
        public string MASTER_CODE { get; set; }
        public string ITEM_NAME { get; set; }
        public decimal QUI_CACH { get; set; }
        public decimal PRIMARY_QTY { get; set; }
        public decimal TOTAL_QTY { get; set; }
        public uint NUMBER_ALL_ITEMS { get; set; }
        public uint NUMBER_EVEN_ITEMS { get; set; }
        public decimal EVEN_ITEM_WEIGH { get; set; }
        public string EVEN_ITEM_TYPE { get; set; }

        public uint NUMBER_ODD_ITEMS { get; set; }
        public decimal ODD_ITEM_WEIGH { get; set; }
        public string ODD_ITEM_TYPE { get; set; }

        public uint D_STATUS { get; set; }
        //==========================================
        public uint MIXING_ITEM_ID { get; set; }
        public uint ITEM_TYPE { get; set; }
        public uint BARCODE_ID { get; set; }
        public uint I_STATUS { get; set; }
    }

    public class FNL_MIX_MD {
        public uint MIXING_ID { get; set; }
        public uint BATCH_ID { get; set; }
        public uint FORMULA_ID { get; set; }
        public uint RECIPE_ID { get; set; }
        public uint BATCH_QTY { get; set; }
        public uint M_STATUS { get; set; }
        public List<FNL_MIX_DTL_MD> MIX_DTL_LIST { get; set; }
    }

    public class FNL_MIX_DTL_MD {
        public uint MIXING_DTL_ID { get; set; }
        public uint LINE_NO { get; set; }
        public uint INVENTORY_ITEM_ID { get; set; }
        public string MASTER_CODE { get; set; }
        public string ITEM_NAME { get; set; }
        public decimal QUI_CACH { get; set; }
        public decimal PRIMARY_QTY { get; set; }
        public decimal TOTAL_QTY { get; set; }
        public uint NUMBER_ALL_ITEMS { get; set; }

        public uint NUMBER_EVEN_ITEMS { get; set; }
        public decimal EVEN_ITEM_WEIGH { get; set; }
        public string  EVEN_ITEM_TYPE { get; set; }

        public uint NUMBER_ODD_ITEMS { get; set; }
        public decimal ODD_ITEM_WEIGH { get; set; }
        public string ODD_ITEM_TYPE { get; set; }

        public uint D_STATUS { get; set; }
        public List<FNL_MIX_ITEM_DTL_MD> MIX_ITEM_DTL_LIST { get; set; }
    }

    public class FNL_MIX_ITEM_DTL_MD {
        public uint MIXING_ITEM_ID { get; set; }
        public uint INVENTORY_ITEM_ID { get; set; }
        public uint BARCODE_ID { get; set; }
        public uint ITEM_TYPE { get; set; }
        public uint I_STATUS { get; set; }
    }

}