﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NVL.API_R.Models
{
    public class APP
    {
        public int ID { get; set; }
        public string APP_NAME { get; set; }
        public string APP_URL { get; set; }
        public string ICON { get; set; }
        public string BG_COLOR { get; set; }
        public string DESCRIPTION { get; set; }
        public string APP_GROUP { get; set; }
        public bool HARDWARE_CHECK { get; set; }
        public int IS_ACTIVE { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
    }
}