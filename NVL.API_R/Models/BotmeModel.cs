﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NVL.API_R.Models
{
    public class BM_WO_INFOR
    {
        public uint ORDER_ID { get; set; }
        public uint BATCH_ID { get; set; }
        public string BATCH_CODE { get; set; }
        public string PRODUCT_NAME { get; set; }
        public int BATCHES_QTY { get; set; }
        public int DEVICE_TYPE { get; set; }
        public int STATUS { get; set; }
        public int ORDER_GROUP { get; set; }
    }

    public class BM_WO_DATA
    {
        public uint BATCH_ID { get; set; }
        public string BATCH_CODE { get; set; }
        public uint ORDER_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public DateTime KHSX_DATE { get; set; }
        public decimal KHSX { get; set; }
        public decimal VOLUME_OF_A_BATCH { get; set; }
        public int BATCHES_COMPLETED { get; set; }
        public int BATCHES_QTY { get; set; }
        public string RECIPE_NO { get; set; }
        public int RECIPE_VERSION { get; set; }
        public string RECIPE_DESCRIPTION { get; set; }
        public string FORMULA_NO { get; set; }
        public int FORMULA_VERS { get; set; }
        public DateTime CREATION_DATE { get; set; }
        public uint BARCODE_ID { get; set; }
        public decimal ST_WEIGH { get; set; }
        public decimal TARE_WEIGH { get; set; }
        public decimal NET_WEIGH { get; set; }
        public string UOM { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public DateTime UPDATE_DATE { get; set; }
        public string PROCESS_CODE { get; set; }
        public int STATUS { get; set; }
        public DateTime EXP_DATE { get; set; }

    }

    public class BM_WO
    {
        public uint BATCH_ID { get; set; }
        public string BATCH_CODE { get; set; }
        public uint ORDER_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public decimal VOLUME_OF_A_BATCH { get; set; }
        public int BATCHES_COMPLETED { get; set; }
        public int BATCHES_QTY { get; set; }
        public DateTime KHSX_DATE { get; set; }
        public decimal KHSX { get; set; }
        public RECIPE_MD RECIPE { get; set; }
        public FORMULA_MD FORMULA { get; set; }
        public List<BM_PACKAGE> BOTME_WEIGHED_LIST { get; set; }
    }

    public class RECIPE_MD
    {
        public int VERSION { get; set; }
        public string RECIPE_CODE { get; set; }
    }
    public class FORMULA_MD
    {
        public int VERSION { get; set; }
        public string CODE { get; set; }
        public DateTime VALIDATION_DATE { get; set; }
    }

    public class BM_PACKAGE
    {
        public uint ORDER_ID { get; set; }
        public uint BARCODE_ID { get; set; }
        public string PROCESS_CODE { get; set; }
        public decimal? ST_WEIGH { get; set; }
        public decimal? NET_WEIGH { get; set; }
        public decimal? TARE_WEIGH { get; set; }
        public string UOM { get; set; }
        public int STATUS { get; set; }
        public DateTime EXP_DATE { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
    }



}