﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NVL.API_R.Models
{
    public class PXK_INFO
    {
        public string PXK_NO { get; set; }
        public DateTime CREATION_DATE { get; set; }
    }

    public class WEIGH_ORDER_ITEM_DETAIL
    {
        public uint ORDER_ID { get; set; }
        public uint ORDER_DETAIL_ID { get; set; }
        public uint INVENTORY_ITEM_ID { get; set; }
        public decimal TARE_WEIGH { get; set; }
        public decimal NET_WEIGH { get; set; }
        public decimal ST_WEIGH { get; set; }
        public uint REF_BARCODE_ID { get; set; }
        public uint REF_TRANSACTION_ID { get; set; }
        public uint BARCODE_ID { get; set; }
        public bool SECOND_LOT { get; set; }
        public int STATUS { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public DateTime UPDATE_DATE { get; set; }
    }

    public class NVL_INFO
    {
        public int TRANSACTION_ERP_ID { get; set; }
        public string PXK_NO { get; set; }
        public uint INVENTORY_ITEM_ID { get; set; }
        public string MASTER_CODE { get; set; }
        public string ITEM_NAME { get; set; }
        public string LOT_NUMBER { get; set; }
        public decimal TRANSACTION_QUANTITY { get; set; }
        public decimal INPUTED_VOLUME { get; set; }
        public string SPKT_NO { get; set; }
        public DateTime HDVSX { get; set; }
        public DateTime HSD { get; set; }
        public string STATUS { get; set; }
    }

    #region
    public class NVL_MTL_TRANS_CREATION
    {
        public string PROCESS_CODE { get; set; }
        public uint TRANSACTION_ERP_ID { get; set; }
        public uint INVENTORY_ITEM_ID { get; set; }
        public string LOT_NO { get; set; }
        public string SPKT_NO { get; set; }
        public string PXK_NO { get; set; }
        public bool FORM_TYPE { get; set; }
        public decimal FORM_QUANTITY { get; set; }
        public uint FORM_COUNT { get; set; }
        public decimal TRANSACTION_QUANTITY { get; set; }
        public string TRANSACTION_UOM { get; set; }
        public uint TRANSACTION_ACTION { get; set; }
        public uint TRANSACTION_TYPE { get; set; }
        public uint TRANSACTION_STATUS { get; set; }
        public uint OWNER_ORG_ID { get; set; }
        public uint TRANSFER_ORG_ID { get; set; }
        public string CREATE_BY { get; set; }
    }

    class NVL_MTL_TRANS_ITEM
    {
        public uint TRANSACTION_ID { get; set; }
        public uint TRANSACTION_ERP_ID { get; set; }
        public uint TRANSACTION_SET_ID { get; set; }
        public uint OWNER_ORGANIZATION_ID { get; set; }
        public uint TRANSFER_ORGANIZATION_ID { get; set; }
        public uint INVENTORY_ITEM_ID { get; set; }
        public string MASTER_CODE { get; set; }
        public string ITEM_NAME { get; set; }
        public uint TRANSACTION_ACTION { get; set; }
        public uint TRANSACTION_TYPE { get; set; }
        public uint TRANSACTION_STATUS { get; set; }
        public uint FORM_TYPE { get; set; }
        public uint FORM_QUANTITY { get; set; }
        public uint FORM_COUNT { get; set; }
        public uint TRANSACTION_QUANTITY { get; set; }
        public string TRANSACTION_UOM { get; set; }
        public string PXK_NO { get; set; }
        public string LOT_NO { get; set; }
        public string SPKT_NO { get; set; }
        public uint BARCODE_ID { get; set; }
        public uint PROCESS_ID { get; set; }
        public string PROCESS_CODE { get; set; }
        public uint ORIGIN_BARCODE_ID { get; set; }
        public uint ORIGIN_PROCESS_ID { get; set; }
        public string ORIGIN_PROCESS_CODE { get; set; }
        public uint DEPARTMENT_ID { get; set; }
        public DateTime CREATION_DATE { get; set; }
        public string CREATION_BY { get; set; }
        public DateTime LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATE_BY { get; set; }
    }

    #endregion
}