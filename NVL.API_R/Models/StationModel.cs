﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NVL.API_R.Models
{
    public class PROCESS
    {
        public int PROCESS_ID { get; set; }
        public string PROCESS_CODE { get; set; }
        public uint ORGANIZATION_ID { get; set; }
        public string ORGANIZATION_CODE { get; set; }
        public string ORGANIZATION_NAME { get; set; }
        public string DESCRIPTION { get; set; }
    }

    public class STATION
    {
        public int ID { get; set; }
        public string STATION_CODE { get; set; }
        public int PROCESS_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string IP_ADDRESS { get; set; }
        public int STATUS { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public DateTime UPDATE_DATE { get; set; }
    }

    public class STATION_APP_ITEM
    {
        //----STATION
        public int ID { get; set; }
        public string STATION_CODE { get; set; }
        public string DESCRIPTION { get; set; }
        public string IP_ADDRESS { get; set; }
        public int STATUS { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public DateTime UPDATE_DATE { get; set; }
        //---- APP
        public int ID_APP { get; set; }
        public string APP_NAME { get; set; }
        public string APP_URL { get; set; }
        public string ICON { get; set; }
        public string BG_COLOR { get; set; }
        public string DESCRIPTION_APP { get; set; }
        public string APP_GROUP { get; set; }
        public bool HARDWARE_CHECK { get; set; }
        public bool IS_ACTIVE { get; set; }
    }

    public class STATION_DEVICE_ITEM
    {
        //----STATION
        public int ID { get; set; }
        public string STATION_CODE { get; set; }
        public int LOCATION_ID { get; set; }
        public string LOCATION_CODE { get; set; }
        public string DESCRIPTION { get; set; }
        public string IP_ADDRESS { get; set; }
        public bool STATUS { get; set; }
        //---DEVICE
        public int DEVICE_ID { get; set; }
        public int TYPE_ID { get; set; }
        public string TYPE_CODE { get; set; }
        public string MODEL { get; set; }
        public string SERIAL_NO { get; set; }
        public string DESCRIPTION_D { get; set; }
        public bool CAN_SHARE { get; set; }
        //---DEVICE DETAIL
        public decimal CAPACITY { get; set; }
        public decimal MIN_FLOW { get; set; }
        public decimal MAX_FLOW { get; set; }
        public string UOM { get; set; }
        //---DEVICE SETTING
        public string PORTNAME { get; set; }
        public int BAUDRATE { get; set; }
        public int DATABITS { get; set; }
        public int PARITY { get; set; }
        public int STOPBIT { get; set; }
        public bool NET { get; set; }
        public string IP { get; set; }
        public int PORT { get; set; }
        public string NEWLINE { get; set; }
        public int READ_TIMEOUT { get; set; }
    }

    public class LOCATION_STATION_ITEM
    {
        //---- LOCATION
        public int ID { get; set; }
        public string LOCATION_CODE { get; set; }
        public string DESCRIPTION { get; set; }
        public int STATUS { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public DateTime UPDATE_DATE { get; set; }
        //---- STATION
        public int STATION_ID { get; set; }
        public string STATION_CODE { get; set; }
        public string DESCRIPTION_STATION { get; set; }
        public string IP_ADDRESS { get; set; }
        public int STATUS_STATION { get; set; }
    }

    public class DEVICE_TYPE
    {
        public int ID { get; set; }
        public string TYPE_CODE { get; set; }
        public string DESCRIPTION { get; set; }
        public int STATUS { get; set; }
    }

    public class ITEM_DEVICE_DETAIL_SETTING
    {
        //---DEVICE
        public int ID { get; set; }
        public int TYPE { get; set; }
        public string TYPE_CODE { get; set; }
        public string MODEL { get; set; }
        public string SERIAL_NO { get; set; }
        public string DESCRIPTION { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public DateTime UPDATE_DATE { get; set; }
        public int STATUS { get; set; }
        public int CAN_SHARE { get; set; }
        //---DEVICE DETAIL
        public int ID_DETAIL { get; set; }
        public decimal CAPACITY { get; set; }
        public decimal MIN_FLOW { get; set; }
        public decimal MAX_FLOW { get; set; }
        public string UOM { get; set; }
        //---DEVICE SETTING
        public int ID_SETTING { get; set; }
        public string PORTNAME { get; set; }
        public int BAUDRATE { get; set; }
        public int DATABITS { get; set; }
        public int PARITY { get; set; }
        public int STOPBIT { get; set; }
        public int NET { get; set; }
        public string IP { get; set; }
        public int PORT { get; set; }
        public string NEWLINE { get; set; }
        public int READ_TIMEOUT { get; set; }
    }

    public class LOCATION
    {
        public int ID { get; set; }
        public string LOCATION_CODE { get; set; }
        public string DESCRIPTION { get; set; }
        public int STATUS { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public DateTime UPDATE_DATE { get; set; }
    }

    public class DEVICE
    {
        public int ID { get; set; }
        public int TYPE_ID { get; set; }
        public string TYPE_CODE { get; set; }
        public string MODEL { get; set; }
        public string SERIAL_NO { get; set; }
        public string DESCRIPTION { get; set; }
        public bool CAN_SHARE { get; set; }
        public decimal CAPACITY { get; set; }
        public decimal MIN_FLOW { get; set; }
        public decimal MAX_FLOW { get; set; }
        public string PORTNAME { get; set; }
        public int BAUDRATE { get; set; }
        public int DATABITS { get; set; }
        public int PARITY { get; set; }
        public int STOPBIT { get; set; }
        public bool NET { get; set; }
        public string IP { get; set; }
        public int PORT { get; set; }
        public string NEWLINE { get; set; }
        public int READ_TIMEOUT { get; set; }
        public string UOM { get; set; }
        public string CREATE_BY { get; set; }
        public string CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
    }

    public class UOM
    {
        public int ID { get; set; }
        public string UOM_CODE { get; set; }
        public string UOM_DESCRIPTION { get; set; }
        public string TYPE { get; set; }
        public bool BASE { get; set; }
        public int FROM_UOM { get; set; }
        public decimal CONVERTION { get; set; }
    }

    public class LOCATION_STATION_MAP
    {
        public int ID { get; set; }
        public int LOCATION_ID { get; set; }
        public int STATION_ID { get; set; }
        public int STATUS { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public DateTime UPDATE_DATE { get; set; }
    }

    public class APP_STATION_MAP
    {
        public int ID { get; set; }
        public int APP_ID { get; set; }
        public int STATION_ID { get; set; }
        public int STATUS { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public DateTime UPDATE_DATE { get; set; }
    }

    public class STATION_DEVICE_MAP
    {
        public int STATION_ID { get; set; }
        public int DEVICE_ID { get; set; }
        public int STATUS { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public DateTime UPDATE_DATE { get; set; }
    }

    public class TOLERANCE_POINT
    {
        public decimal START_POINT { get; set; }
        public decimal END_POINT { get; set; }
        public decimal ACCEPT_ERROR { get; set; }
    }
}