﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NVL.API_R.Models
{
    public class MIX_ITEM_DATA
    {
        public uint ORDER_ID { get; set; }
        public int PROCESS_CODE { get; set; }
        public uint BATCH_ID { get; set; }
        public string BATCH_CODE { get; set; }
        public int BATCHES_QTY { get; set; }
        public int DEVICE_TYPE { get; set; }
        public int STATUS { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public DateTime UPDATE_DATE { get; set; }
        public uint ORDER_GROUP { get; set; }
        public uint ORDER_DETAIL_ID { get; set; }
        public uint INVENTORY_ITEM_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string SEGMENT1 { get; set; }
        public decimal ST_WEIGH { get; set; }
        public int PACKAGE_QTY { get; set; }
        public decimal TOTAL_WEIGH { get; set; }
        public int STATUS_DETAIL { get; set; }
        public uint SUB_INVENTORY_ITEM_ID { get; set; }
        public DateTime SUB_START_DATE { get; set; }
        public DateTime SUB_END_DATE { get; set; }
        public string FORMULA_DESC1 { get; set; }
    }

    public class MIX_WO_DATA
    {
        public uint ORDER_ID { get; set; }
        public int PROCESS_CODE { get; set; }
        public uint BATCH_ID { get; set; }
        public string BATCH_CODE { get; set; }
        public int BATCHES_QTY { get; set; }
        public int DEVICE_ID { get; set; }
        public int STATUS { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public DateTime UPDATE_DATE { get; set; }
        public int ORDER_GROUP { get; set; }
        public uint ORDER_DETAIL_ID { get; set; }
        public uint INVENTORY_ITEM_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string SEGMENT1 { get; set; }
        public decimal ST_WEIGH { get; set; }
        public int PACKED_QTY { get; set; }
        public decimal TOTAL_WEIGH { get; set; }
        public int STATUS_DETAIL { get; set; }
        public uint SUB_INVENTORY_ITEM_ID { get; set; }
        public DateTime SUB_START_DATE { get; set; }
        public DateTime SUB_END_DATE { get; set; }
        public decimal TARE_WEIGH { get; set; }
        public decimal NET_WEIGH { get; set; }
        public decimal ITEM_ST_WEIGH { get; set; }
        public uint REF_BARCODE_ID { get; set; }
        public uint REF_TRANSACTION_ID { get; set; }
        public uint BARCODE_ID { get; set; }
        public int SECOND_LOT { get; set; }
        public int STATUS_ITEM_DETAIL { get; set; }
        public string FORMULA_DESC1 { get; set; }
        public int CO { get; set; }
        public int CO_MIX { get; set; }
        public int QTY_MIX { get; set; }
        public int QTY_MIXED { get; set; }
    }
}