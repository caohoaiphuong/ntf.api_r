﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NVL.API_R.Models
{
    public class UserModel
    {
        public uint ID { get; set; }
        public string USERNAME { get; set; }
        public string PASSWORD { get; set; }
        public string MSNV { get; set; }
        public string FULLNAME { get; set; }
        public string NON_UNICODE { get; set; }
        public string MAIL { get; set; }
        public string PHONE { get; set; }
        public string CREATE_BY { get; set; }
    }

    public class UserPass
    {
        public string USERNAME { get; set; }
        public string OLD_PASSWORD { get; set; }
        public string PASSWORD { get; set; }
        public DateTime UPDATE_BY { get; set; }
    }


    public class UserAppRole
    {
        public int ID { get; set; }
        public string APP_NAME { get; set; }
        public bool OBJECT_ROLE { get; set; }
        public bool SYSTEM_ROLE { get; set; }
        public bool ADMIN_ROLE { get; set; }
    }

    //=====USR GET_APP_USER_ROLE========
    public class USR_APP_USER_ROLE {
        public string USERNAME { get; set; }
        public string APP_GROUP { get; set; }
        List<UserAppRole> LIST_APP_ROLE { get; set; }
    }

    public class USR_GET_APP_USER_ROLE
    {
        public string USERNAME { get; set; }
        public int APP_ID { get; set; }
        public string APP_NAME { get; set; }
        public string APP_GROUP { get; set; }
        public bool OBJECT_ROLE { get; set; }
        public bool SYSTEM_ROLE { get; set; }
        public bool  ADMIN_ROLE { get; set; }
    }

    public class APP_ASSIGNMENT
    {
        public string ASSIGNER { get; set; }
        public string PASSWORD { get; set; }
        public string USERNAME { get; set; }
        public int APP_ID { get; set; }
    }
}