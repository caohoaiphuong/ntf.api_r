﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NVL.API_R.Models
{
    public class PrintBarcode
    {
        public string PROCESS_CODE { get; set; }
        public uint BARCODE { get; set; }
        public int QTY_PRINT { get; set; }
        public string USERNAME { get; set; }
    }

    public class PrinterInfo
    {
        public int ID { get; set; }
        public int TYPE_ID { get; set; }
        public string TYPE_CODE { get; set; }
        public string IP { get; set; }
        public int PORT { get; set; }
    }

    public class ITEM_BMNL_DATA
    {
        public string PXK_NO { get; set; }  // pxk no
        public uint INVENTORY_ITEM_ID { get; set; }
        public string MASTER_CODE { get; set; }  // master id
        public string ITEM_NAME { get; set; }  // item name
        public string LOT_NUMBER { get; set; }
        public string SPKT { get; set; }   // spkt
        public DateTime HSD { get; set; }   // HSD
        public string STATUS_ID { get; set; }
        public decimal VOLUME_OF_PACKAGE { get; set; }
        public string PRINTER_ZPL { get; set; }
    }

    public class ITEM_BMWE_DATA
    {
        public uint ORDER_ID { get; set; }  // pxk no
        public uint BATCH_ID { get; set; }
        public string BATCH_CODE { get; set; }  // master id
        public string PACKAGE_QTY { get; set; }  // item name
        public uint INVENTORY_ITEM_ID { get; set; }
        public decimal NET_WEIGH { get; set; }   // spkt
        public decimal ST_WEIGH { get; set; }   // HSD
        public string MASTER_CODE { get; set; }
        public string ITEM_NAME { get; set; }
        public string PRINTER_ZPL { get; set; }
    }

    public class ITEM_BMTP_DATA
    {
        public string PRODUCT_NAME { get; set; } // PRODUCT_NAME
        public uint INVENTORY_ITEM_ID { get; set; } //MA ITEM
        public decimal NET_WEIGH { get; set; } // KHOI LUONG
        public string UOM { get; set; } // DON VI
        public DateTime EXP_DATE { get; set; } // NGAY HET HAN
        public uint BARCODE_ID { get; set; } // BARCODE
        public string PROCESS_CODE { get; set; } //PROCESS_CODE
        public string PRINTER_ZPL { get; set; }
    }

    //==========nNEW MODEL================//
    public class ITEM_BMNL_INFO
    { 
        public uint INVENTORY_ITEM_ID { get; set; }
        public string MASTER_CODE { get; set; }  // master id
        public string ITEM_NAME { get; set; }  // item name
        public string PXK_NO { get; set; }  // pxk no
        public string LOT_NUMBER { get; set; }
        public string SPKT_NO { get; set; }   // spkt
        public DateTime HSD { get; set; }   // HSD
        public string STATUS_ID { get; set; }
        public decimal VOLUME_OF_PACKAGE { get; set; }
        public string PRINTER_ZPL { get; set; }
    }
}