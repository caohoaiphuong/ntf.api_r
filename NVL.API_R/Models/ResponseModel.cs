﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace NVL.API_R.Models
{
    public enum errCode
    {
        [Description("OK")]
        OK,
        [Description("LOAD SUCCESS")]
        LoadSuccess,
        [Description("CREATED")]
        Created,
        [Description("UPDATED")]
        Updated,
        [Description("INTERNAL SERVER ERROR")]
        InternalError,
        [Description("NO ROW DATA FOUND")]
        NoRowFound,
        [Description("PARAMETER UNACCEPTABLE")]
        ParameterError,
        [Description("ORACLE APPLICATION ERROR")]
        OracleCustomExp,
        [Description("NO DATA FOUND")]
        NotFound,
    }

    public class Response<T>
    {
        public bool success { get; set; } = true;
        public T payload { get; set; }
        public errorMD error { get; set; } = new errorMD();
    }

    public class errorMD
    {
        public UInt16 code { get; set; } = (int)errCode.OK;
        public string message { get; set; } = "OK";
        public string description { get; set; } = Enum.GetName(typeof(errCode), errCode.OK);
    }
}