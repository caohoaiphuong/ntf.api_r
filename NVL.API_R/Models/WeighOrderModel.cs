﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NVL.API_R.Models
{

    public class WO_CREATION
    {
        public uint ORDER_ID { get; set; }
        public string PROCESS_CODE { get; set; }
        public uint BATCH_ID { get; set; }
        public string BATCH_CODE { get; set; }
        public decimal BATCHES_QTY { get; set; }
        public int DEVICE_TYPE { get; set; }
        public int STATUS { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public int ORDER_GROUP { get; set; }

        public uint? ORGANIZATION_ID { get; set; }
        public List<int> ORDER_IDs { get; set; }
        public int ORDER_QTY { get; set; }
    }

    public class WEIGH_ORDER_GROUP
    {
        public uint BATCH_ID { get; set; }
        public int ORDER_GROUP { get; set; }
        public List<int> ORDER_IDs { get; set; }
        public int LOCATION_ID { get; set; }
    }

    public class WEIGH_ORDER_GROUP_DATA
    {
        public uint BATCH_ID { get; set; }
        public int ORDER_GROUP { get; set; }
        public uint ORDER_ID { get; set; }
        public int LOCATION_ID { get; set; }
    }

    public class WEIGH_ORDER
    {
        public uint BATCH_ID { get; set; }
        public uint ORDER_ID { get; set; }
    }

    public class BARCODE
    {
        public uint BARCODE_ID { get; set; }
        public uint INVENTORY_ITEM_ID { get; set; }
        public string PROCESS_CODE { get; set; }
        public string USERNAME { get; set; }
    }

    public class VERTICAL_WEIGH_LIMIT
    {
        public uint BATCH_ID { get; set; }
        public string BATCH_CODE { get; set; }
        public string PROCESS_CODE { get; set; }
        public int VER_WEIGH_LIMIT { get; set; }
        public string USERNAME { get; set; }
    }

    public class BATCH_INFO
    {
        public uint BATCH_ID { get; set; }
        public string BATCH_CODE { get; set; }
        public string KHSX { get; set; }
        public string PRODUCT_NAME { get; set; }
        public uint ORGANIZATION_ID { get; set; }
        public DateTime PLAN_START_DATE { get; set; }
        public DateTime CREATION_DATE { get; set; }
        public DateTime ACTUAL_START_DATE { get; set; }

        public int? FORMULA_ID { get; set; }
        public string FORMULA_NO { get; set; }
        public string FORMULA_VERS { get; set; }
        public int? RECIPE_ID { get; set; }
        public string RECIPE_NO { get; set; }
        public string RECIPE_VERS { get; set; }
        public string RECIPE_DESCRIPTION { get; set; }
        public DateTime RECIPE_START_DATE { get; set; }
        public DateTime RECIPE_END_DATE { get; set; }

        public string LINE_NO { get; set; }
        public uint INVENTORY_ITEM_ID { get; set; }
        public string MASTER_CODE { get; set; }
        public string ITEM_NAME { get; set; }
        public decimal? QTY { get; set; }

        public string SUBSTITUTION_VERS { get; set; }
        public uint? SUB_INVENTORY_ITEM_ID { get; set; }
        public string SUB_MASTER_CODE { get; set; }
        public string SUB_ITEM_NAME { get; set; }
        public decimal SUB_QTY { get; set; }

        public string UOM { get; set; }
        public DateTime SUB_START_DATE { get; set; }
        public DateTime SUB_END_DATE { get; set; }

    }

    public class WO_DATA
    {
        public uint ORDER_ID { get; set; }
        public string BATCH_CODE { get; set; }
        public uint BATCH_ID { get; set; }
        public string PROCESS_CODE { get; set; }
        public int BATCHES_QTY { get; set; }
        public int DEVICE_TYPE { get; set; }
        public int STATUS { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public DateTime UPDATE_DATE { get; set; }
        public int ORDER_GROUP { get; set; }
        public uint ORDER_DETAIL_ID { get; set; }
        public uint INVENTORY_ITEM_ID { get; set; }
        public string ITEM_NAME { get; set; }
        public string MASTER_CODE { get; set; }
        public decimal ST_WEIGH { get; set; }
        public int PACKAGE_QTY { get; set; }
        public decimal TOTAL_WEIGH { get; set; }
        public int STATUS_DETAIL { get; set; }
        public uint SUB_INVENTORY_ITEM_ID { get; set; }
        public string SUB_ITEM_NAME { get; set; }
        public string SUB_MASTER_CODE { get; set; }
        public DateTime SUB_START_DATE { get; set; }
        public DateTime SUB_END_DATE { get; set; }
        public decimal TARE_WEIGH { get; set; }
        public decimal NET_WEIGH { get; set; }
        public decimal ITEM_ST_WEIGH { get; set; }
        public string SUB_DESCRIPTION { get; set; }
        public string SUB_MANUAL_DESCRIPTION { get; set; }
        public uint SUB_MANUAL_MODE { get; set; }
        public uint REF_BARCODE_ID { get; set; }
        public uint REF_TRANSACTION_ID { get; set; }
        public uint BARCODE_ID { get; set; }
        public int SECOND_LOT { get; set; }
        public int STATUS_ITEM_DETAIL { get; set; }
        public int CO { get; set; }
        public int CO_MIX { get; set; }
    }

    public class MIXING
    {
        public uint ID { get; set; }
        public int TYPE_ID { get; set; }
        public string SERIAL_NO { get; set; }
        public string DESCRIPTION { get; set; }
        public decimal CAPACITY { get; set; }
        public decimal MIN_FLOW { get; set; }
        public decimal MAX_FLOW { get; set; }
        public string UOM { get; set; }
    }

    public class BARCODE_ITEM_DATA
    {
        public uint BARCODE_ID { get; set; }
        public uint INVENTORY_ITEM_ID { get; set; }
        public string LOT_NO { get; set; }
        public string SPKT_NO { get; set; }
        public string PROCESS_CODE { get; set; }
        public int PROCESS_ID { get; set; }
        public uint ORGANIZATION_ID { get; set; }
        public uint TRANSACTION_ID { get; set; }
        public DateTime HDVSX { get; set; }
        public int STATUS { get; set; }
    }

    public class BATCH_STATUS
    {
        public uint STT { get; set; }
        public uint BATCH_ID { get; set; }
        public string BATCH_CODE { get; set; }
        public uint ORGANIZATION_ID { get; set; }
        public string PROCESS_CODE { get; set; }
        public bool STATUS { get; set; }
        public string CREATE_BY { get; set; }
    }

    public class MANUAL_SUBS {
        public uint WOD{ get; set; }
        public uint INVENTORY_ITEM_ID { get; set; }
        public uint SUB_INVENTORY_ITEM_ID { get; set; }
        public string SUB_MANUAL_DESCRIPTION { get; set; }
        public uint SUB_MANUAL_MODE { get; set; }
        public string UPDATE_BY { get; set; }
    }

    public class ITEM_INFO {
        public uint INVENTORY_ITEM_ID { get; set; }
        public string MASTER_CODE { get; set; }
        public string ITEM_NAME { get; set; }
    }
}