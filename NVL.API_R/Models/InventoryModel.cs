﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NVL.API_R.Models
{
    public class INVENTORY_DATA
    {
        public int STT { get; set; }
        public string MASTER_CODE { get; set; }
        public string ITEM_NAME { get; set; }
        public string LOT_NO { get; set; }
        public string SPKT_NO { get; set; }
        public string UOM { get; set; }
        public decimal TON_DAU { get; set; }    
        public decimal NHAP { get; set; }
        public decimal XUAT { get; set; }
        public decimal RESERVED { get; set; }
        public decimal TON_AFTER_CAN { get; set; }
        public decimal TON_CUOI { get; set; }
        public DateTime HDVSX { get; set; }
        public DateTime HSD { get; set; }
    }

    public class TRANSACTION_DATA
    {
        public int STT { get; set; }
        public string MASTER_CODE { get; set; }
        public string UOM { get; set; }
        public decimal TOTAL_VOLUME { get; set; }
        public string ITEM_NAME { get; set; }
        public string SPKT_NO { get; set; }
        public string LOT_NO { get; set; }
        public DateTime HDVSX { get; set; }
        public DateTime HSD { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string PXK_NO { get; set; }
        public string CREATE_BY { get; set; }
        public int BARCODE_ID { get; set; }
        public string PROCESS_CODE { get; set; }
    }

    public class WEIGHED_BY_BATCH_DATA
    {
        public string BATCH_CODE { get; set; }
        public uint ORDER_ID { get; set; }
        public uint ORDER_DETAIL_ID { get; set; }
        public uint INVENTORY_ITEM_ID { get; set; }
        public string MASTER_CODE { get; set; }
        public string ITEM_NAME { get; set; }
        public string UOM { get; set; }
        public decimal USED_VOLUME { get; set; }
        public string SPKT_NO { get; set; }
        public string LOT_NO { get; set; }
        public string BARCODE { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
    }

    public class MIXED_INFO_DATA
    {
        public string BATCH_CODE { get; set; }
        public uint ORDER_ID { get; set; }
        public uint ORDER_DETAIL_ID { get; set; }
        public int PACKAGE_QTY { get; set; }
        public uint INVENTORY_ITEM_ID { get; set; }
        public string MASTER_CODE { get; set; }
        public string ITEM_NAME { get; set; }
        public DateTime START_TIME { get; set; }
        public string UPDATE_BY { get; set; }
    }

    public class MTL_TRANS_INF
    {
        public uint TRANSACTION_ID { get; set; }
        public uint TRANSACTION_ERP_ID { get; set; }
        public uint TRANSACTION_SET_ID { get; set; }
        public string MASTER_CODE { get; set; } 
        public string ITEM_NAME { get; set; }
        public string LOT_NO { get; set; }
        public string SPKT_NO { get; set; }
        public decimal TRANSACTION_QUANTITY { get; set; }
        public uint TRANSACTION_ACTION { get; set; }
        public uint TRANSACTION_TYPE { get; set; }
        public uint TRANSACTION_STATUS { get; set; }
        public string ACTION_DESCRIPTION { get; set; }
        public string TYPE_DESCRIPTION { get; set; }
        public string PROCESS_CODE { get; set; }
        public uint BARCODE_ID { get; set; }
        public string BARCODE { get; set; }
    }

    public class BRC_ID_INFO {
        public uint TRANSACTION_ERP_ID { get; set; }
        public uint TRANSACTION_SET_ID { get; set; }
        public uint PROCESS_ID { get; set; }
        public uint BARCODE_ID { get; set; }
        public string PROCESS_CODE { get; set; }
        public uint INVENTORY_ITEM_ID { get; set; }
        public string MASTER_CODE { get; set; }
        public string ITEM_NAME { get; set; }
        public string LOT_NO { get; set; }
        public string SPKT_NO { get; set; }
        public decimal REMAIN_QUANTITY { get; set; }

        public uint STATUS { get; set; }
        public string STATUS_DESCRIPTION { get; set; }
    }

    public class MICS_CREATION{
        public string PROCESS_CODE { get; set; }
        public uint BARCODE_ID { get; set; }
        public decimal MICS_QUANTITY { get; set; }
        public string CREATE_BY { get; set; }
        public string DESCRIPTION { get; set; }
    }

}