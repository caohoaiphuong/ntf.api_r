﻿

namespace NVL.API_R.common.Model {
    class APP_TITLE
    {
        public string TITLE_CODE { get; set; }
        public string TITLE_CONTENT { get; set; }
        public string DESCRIPTION { get; set; }
    }
}