﻿using NVL.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using NVL.API_R.Helper;
using System.Net.Http;
using System.Net;

namespace NVL.API_R.Base
{
    public class BaseController : ApiController
    {
        protected string clientAddress;

        #region constructor
        public BaseController()
        {
            clientAddress = HttpContext.Current.Request.UserHostAddress;
        }
#endregion constructor


#region Method
        //Excute StoreProcedure with parameter and return DataSet
        protected DataSet ExecSPToListOracle(string proc, Dictionary<string, object> parameters)
        {
            try
            {
                string cons = ConfigHelper.GetConnectionString();
                var DataAcc = DataAccessLayerFactory.GetDataAccessLayer(DataProviderType.Oracle, cons);
                if (parameters != null && parameters.Count > 0)
                {
                    foreach (KeyValuePair<string, object> parameter in parameters)
                    {
                        DataAcc.AddParameter(parameter.Key, parameter.Value);
                    }
                }
                DataSet reader = DataAcc.ExecuteDataSet(proc, CommandType.StoredProcedure);
                return reader;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Excute StoreProcedure with no parameter and return DataSet
        protected DataSet ExecSPToListOracle(string proc)
        {
            try
            {
                string cons = ConfigHelper.GetConnectionString();
                var DataAcc = DataAccessLayerFactory.GetDataAccessLayer(DataProviderType.Oracle, cons);
                DataSet reader = DataAcc.ExecuteDataSet(proc, CommandType.StoredProcedure);
                return reader;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Excute Non-Query StoreProcedure with paramete
        protected void ExecuteNonQueryOracle(string proc, Dictionary<string, object> parameters)
        {
            try
            {
                string cons = ConfigHelper.GetConnectionString();
                var DataAcc = DataAccessLayerFactory.GetDataAccessLayer(DataProviderType.Oracle, cons);
                if (parameters != null && parameters.Count > 0)
                {
                    foreach (KeyValuePair<string, object> parameter in parameters)
                    {
                        DataAcc.AddParameter(parameter.Key, parameter.Value);
                    }
                }
                DataAcc.ExecuteNonQuery(proc, CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Create HttpResponseMessage
        protected HttpResponseMessage CreateHttpResponse(HttpRequestMessage requestMessage, Func<HttpResponseMessage> function)
        {
            HttpResponseMessage response = null;
            try
            {
                response = function.Invoke();
            }
            catch (Exception ex)
            {
                response = requestMessage.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }

            return response;
        }

#endregion Method
    }

}