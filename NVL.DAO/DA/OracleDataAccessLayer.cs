﻿using System.Data;
using Oracle.ManagedDataAccess.Client;


namespace NVL.DataAccess
{
    /// <summary>
    /// The SQLDataAccessLayer contains the data access layer for Oracle data provider. 
    /// This class implements the abstract methods in the DataAccessLayerBase class.
    /// </summary>
    public class OracleDataAccessLayer : DataAccessLayerBase
    {
        // Provide class constructors
        public OracleDataAccessLayer() { }
        public OracleDataAccessLayer(string connectionString) { this.ConnectionString = connectionString; }

        // DataAccessLayerBaseClass Members
        internal override IDbConnection GetDataProviderConnection()
        {
            return new OracleConnection();
        }
        internal override IDbCommand GetDataProviderCommand()
        {
            return new OracleCommand();
        }
        internal override IDbDataAdapter GetDataProviderDataAdapter()
        {
            return new OracleDataAdapter();
        }
        internal override IDataParameter GetDataProviderParameter(string parameterName, object data)
        {
            OracleParameter para = new OracleParameter();
            para.ParameterName = parameterName;
            if (data != null)
            {
                switch (data.GetType().Name)
                {
                    case "Boolean":
                        para.OracleDbType = OracleDbType.Boolean;
                        break;
                    case "DateTime":
                        para.OracleDbType = OracleDbType.Date;
                        break;
                    case "Decimal":
                        para.OracleDbType = OracleDbType.Decimal;
                        break;
                    case "Double":
                        para.OracleDbType = OracleDbType.Double;
                        break;
                    case "Int16":
                        para.OracleDbType = OracleDbType.Int16;
                        break;
                    case "Int32":
                        para.OracleDbType = OracleDbType.Int32;
                        break;
                    case "UInt32":
                        para.OracleDbType = OracleDbType.Int64;
                        break;
                    case "Int64":
                        para.OracleDbType = OracleDbType.Int64;
                        break;
                    case "TimeSpan":
                        para.OracleDbType = OracleDbType.TimeStamp;
                        break;
                    case "String":
                        para.OracleDbType = OracleDbType.Varchar2;
                        break;
                    default:
                        para.OracleDbType = OracleDbType.Varchar2;
                        break;
                }
            }
            para.Value = data;
            return  para; 
        }
        internal override IDataParameter GetDataProviderParameter(string parameterName, object data, ParameterDirection dir)
        {
            OracleParameter para = new OracleParameter();
            para.ParameterName = parameterName;
            para.Value = data;
            para.Direction = dir;
            if (data != null)
            {
                switch (data.GetType().Name)
                {
                    case "Boolean":
                        para.OracleDbType = OracleDbType.Boolean;
                        break;
                    case "DateTime":
                        para.OracleDbType = OracleDbType.Date;
                        break;
                    case "Decimal":
                        para.OracleDbType = OracleDbType.Decimal;
                        break;
                    case "Double":
                        para.OracleDbType = OracleDbType.Double;
                        break;
                    case "Int16":
                        para.OracleDbType = OracleDbType.Int16;
                        break;
                    case "Int32":
                        para.OracleDbType = OracleDbType.Int32;
                        break;
                    case "Int64":
                        para.OracleDbType = OracleDbType.Int64;
                        break;
                    case "TimeSpan":
                        para.OracleDbType = OracleDbType.TimeStamp;
                        break;
                    case "String":
                        para.OracleDbType = OracleDbType.Varchar2;
                        break;
                    default:
                        para.OracleDbType = OracleDbType.Varchar2;
                        break;
                }
            }
            return para;
        }
    }
}
