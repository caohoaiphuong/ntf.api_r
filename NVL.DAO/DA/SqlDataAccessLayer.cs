﻿using System.Data;
using System.Data.SqlClient;

namespace NVL.DataAccess
{
    /// <summary>
    /// The SQLDataAccessLayer contains the data access layer for Microsoft SQL Server. 
    /// This class implements the abstract methods in the DataAccessLayerBase class.
    /// </summary>
    public class SqlDataAccessLayer : DataAccessLayerBase
    {
        // Provide class constructors
        public SqlDataAccessLayer() { }
        public SqlDataAccessLayer(string connectionString) { this.ConnectionString = connectionString; }

        // DataAccessLayerBaseClass Members
        internal override IDbConnection GetDataProviderConnection()
        {
            return new SqlConnection();
        }
        internal override IDbCommand GetDataProviderCommand()
        {
            return new SqlCommand();
        }
        internal override IDbDataAdapter GetDataProviderDataAdapter()
        {
            return new SqlDataAdapter();
        }

        internal override IDataParameter GetDataProviderParameter(string parameterName, object data)
        {
            return new SqlParameter();
        }
        internal override IDataParameter GetDataProviderParameter(string parameterName, object data, ParameterDirection dir)
        {
            return new SqlParameter();
        }
    }
}
