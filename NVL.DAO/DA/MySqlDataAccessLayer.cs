﻿using System.Data;
using MySql.Data.MySqlClient;

namespace NVL.DataAccess
{
    /// <summary>
    /// The MySqlDataAccessLayer contains the data access layer for Microsoft SQL Server. 
    /// This class implements the abstract methods in the DataAccessLayerBase class.
    /// </summary>
    public class MySqlDataAccessLayer : DataAccessLayerBase
    {
        // Provide class constructors
        public MySqlDataAccessLayer() { }
        public MySqlDataAccessLayer(string connectionString) { this.ConnectionString = connectionString; }

        // DataAccessLayerBaseClass Members
        internal override IDbConnection GetDataProviderConnection()
        {
            return new MySqlConnection();
        }
        internal override IDbCommand GetDataProviderCommand()
        {
            return new MySqlCommand();
        }
        internal override IDbDataAdapter GetDataProviderDataAdapter()
        {
            return new MySqlDataAdapter();
        }
        internal override IDataParameter GetDataProviderParameter(string parameterName, object data)
        {
            return new MySqlParameter();
        }
        internal override IDataParameter GetDataProviderParameter(string parameterName, object data, ParameterDirection dir)
        {
            return new MySqlParameter();
        }
    }
}
