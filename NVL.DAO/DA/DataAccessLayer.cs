﻿using System;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;

namespace NVL.DataAccess
{
    /// <summary>
    /// Defines the DataAccessLayer implemented data provider types.
    /// </summary>
    public enum DataProviderType
    {
        Oracle,
        Sql,
        MySql
    }
    ///<summary>
    /// The DataAccessLayerBase lists all the abstract methods 
    /// that each data access layer provider (SQL Server, OleDb, etc.) must implement.
    ///</summary>
    public abstract class DataAccessLayerBase
    {
        #region private data members, methods & constructors
        // private member
        private string strConnectionString;
        private IDbConnection connection;
        private IDbCommand command;
        private List<IDataParameter> parameters = new List<IDataParameter>();
        private IDbTransaction transaction;

        //properties
        public string ConnectionString
        {
            get {
                if (strConnectionString == String.Empty || strConnectionString.Length == 0)
                    throw new ArgumentException("Invalid database connection string.");
                return strConnectionString;
            }
            set { strConnectionString = value; }
        }

        // Since this is an abstract class, for better documentation and readability of source code, 
        // class is defined with an explicit protected constructor
        protected DataAccessLayerBase() { }

        /// <summary>
        /// This method opens (if necessary) and assigns a connection, transaction, command type and parameters 
        /// to the provided command.
        /// </summary>

        private void PrepareCommand(CommandType commandType, string commandText)
        {
            try
            {
                if (connection == null)
                {
                    connection = GetDataProviderConnection();
                    connection.ConnectionString = this.ConnectionString;
                }

                //if the pprovided connection is not open, then open it

                if (connection.State != ConnectionState.Open)
                {
                    Console.WriteLine(connection.State.ToString());
                    connection.Open();
                }


                //Provide the specific data provider command object, if the command object is null
                if (command == null)
                    command = GetDataProviderCommand();

                //associate the connection with the command
                command.Connection = connection;
                //set the command text (stored procedure name or sql statement)
                command.CommandText = commandText;
                //set command type
                command.CommandType = commandType;

                //if transaction is provided, then assign it.
                if (transaction != null)
                    command.Transaction = transaction;

                //attach command parameters if they are provided
                if (parameters.Count > 0)
                    foreach (IDataParameter param in parameters)
                        command.Parameters.Add(param);
                string _type = connection.GetType().Name;

                if (connection.GetType().Name == "OracleConnection" && command.CommandType == CommandType.StoredProcedure)
                {
                    OracleParameter _para = new OracleParameter("PRC", OracleDbType.RefCursor);
                    _para.Direction = ParameterDirection.Output;
                    command.Parameters.Add(_para);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                    //command.Dispose();
                }
            }
        }

        private void PrepareCommandNonQuery(CommandType commandType, string commandText)
        {
            try
            {
                if (connection == null)
                {
                    connection = GetDataProviderConnection();
                    connection.ConnectionString = this.ConnectionString;
                }

                //if the pprovided connection is not open, then open it

                if (connection.State != ConnectionState.Open)
                {
                    //Console.WriteLine(connection.State.ToString());
                    connection.Open();
                }


                //Provide the specific data provider command object, if the command object is null
                if (command == null)
                    command = GetDataProviderCommand();

                //associate the connection with the command
                command.Connection = connection;
                //set the command text (stored procedure name or sql statement)
                command.CommandText = commandText;
                //set command type
                command.CommandType = commandType;

                //if transaction is provided, then assign it.
                if (transaction != null)
                    command.Transaction = transaction;

                //attach command parameters if they are provided
                if (parameters.Count > 0)
                    foreach (IDataParameter param in parameters)
                        command.Parameters.Add(param);
                string _type = connection.GetType().Name;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                    //command.Dispose();
                }
            }
        }

        #endregion

        #region Abstract Methods
        /// Data provider specific implementation for accessing relational databases.
        internal abstract IDbConnection GetDataProviderConnection();
        /// Data provider specific implementation for executing SQL statement while connected to a data source.
        internal abstract IDbCommand GetDataProviderCommand();
        /// Data provider specific implementation for filling the DataSet.
        internal abstract IDbDataAdapter GetDataProviderDataAdapter();
        /// Data provider specific implimentation for creating the parameter.
        internal abstract IDataParameter GetDataProviderParameter(string parameterName, object data);

        internal abstract IDataParameter GetDataProviderParameter(string parameterName, object data, ParameterDirection dir);
        #endregion

        /// <summary>
        // Generic methods implementation
        /// <summary>
        #region AddParameter
        public void AddParameter(string parameterName, object data)
        {
            parameters.Add(GetDataProviderParameter(parameterName, data));
        }
        public void AddParameter(string parameterName, object data, ParameterDirection dir)
        {
            parameters.Add(GetDataProviderParameter(parameterName, data, dir));
        }
        #endregion

        #region Database Transaction

        /// Begins a database transaction.
        public void BeginTransaction()
        {
            if (transaction!=null)
                return;

            try
            {
                //instantiate a connection onject
                connection = GetDataProviderConnection();
                connection.ConnectionString = this.ConnectionString;
                //open connection
                connection.Open();
                //begin a database transaction with a read committed isolation level
                transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted);
            }
            catch (Exception)
            {
                connection.Close();
                throw;
            }
        }

        /// Commits the database transaction.
        public void CommitTransaction()
        {
            if (transaction == null)
                return;

            try
            {
                //commit transaction
                transaction.Commit();
            }
            catch (Exception)
            {
                //rollback transaction
                RollbackTransaction();
                throw;
            }
            finally
            {
                connection.Close();
                transaction = null;
            }
        }

        /// Rolls back a transaction from a pending state.
        public void RollbackTransaction()
        {
            if (transaction == null)
                return;

            try
            {
                transaction.Rollback();
            }
            catch { }
            finally
            {
                connection.Close();
                transaction = null;
            }
        }
        #endregion

        #region ExecuteDataReader
        /// Executes the CommandText against the Connection and builds an IDataReader.
        public IDataReader ExecuteDataReader( string commandText)
        {
            return this.ExecuteDataReader(commandText, CommandType.Text);
        }
        /// Executes a stored procedure against the Connection and builds an IDataReader.
        public IDataReader ExecuteDataReader(string commandText, CommandType commandType)
        {
            try
            {
                PrepareCommand(commandType, commandText);

                IDataReader dr;

                if (transaction == null)
                    // Generate the reader. CommandBehavior.CloseConnection causes the
                    // the connection to be closed when the reader object is closed
                    dr = command.ExecuteReader(CommandBehavior.CloseConnection);
                else
                    dr = command.ExecuteReader();

                return dr;
            }
            catch
            {
                if (transaction == null)
                {
                    connection.Close();
                    command.Dispose();
                }
                else
                    RollbackTransaction();
                throw;
            }
        }
        #endregion

        #region ExecuteDataSet
        /// Adds or refreshes rows in the DataSet to match those in the data source using the DataSet name, and creates a DataTable named "Table".
        public DataSet ExecuteDataSet(string commandText)
        {
            return this.ExecuteDataSet(commandText, CommandType.Text);
        }
        public DataSet ExecuteDataSet(string commandText, CommandType commandType)
        {
            try
            {
                PrepareCommand(commandType, commandText);
                //create the DataAdapter & DataSet
                IDbDataAdapter da = GetDataProviderDataAdapter();
                da.SelectCommand = command;
                DataSet ds = new DataSet();

                //fill the DataSet using default values for DataTable names, etc.
                da.Fill(ds);

                //return the dataset
                return ds;
            }
            catch
            {
                if (transaction == null)
                    connection.Close();
                else
                    RollbackTransaction();
                throw;
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                    command.Dispose();
                }
            }
        }
        #endregion

        #region ExecuteNonQuery
        public void ExecuteNonQuery(string commandText)
        {
            this.ExecuteNonQuery(commandText, CommandType.Text);
        }

        public void ExecuteNonQuery(string commandText, CommandType commandType)
        {
            try
            {
                PrepareCommandNonQuery(commandType, commandText);
                //create the DataAdapter & DataSet
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch
            {
                if (transaction == null)
                    connection.Close();
                else
                    RollbackTransaction();
                throw;
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                    command.Dispose();
                }
            }

        }
        #endregion

        #region ExecuteQuery
        /// Executes an SQL statement against the Connection object of a .NET Framework data provider, and returns the number of rows affected.
        public int ExecuteQuery(string commandText)
        {
            return this.ExecuteQuery(commandText, CommandType.Text);
        }
        /// Executes a stored procedure against the Connection object of a .NET Framework data provider, and returns the number of rows affected.
        public int ExecuteQuery(string commandText, CommandType commandType)
        {
            try
            {
                PrepareCommandNonQuery(commandType, commandText);

                // execute command
                int intAffectedRows = command.ExecuteNonQuery();
                // return no of affected records
                return intAffectedRows;
            }
            catch
            {
                if (transaction != null)
                    RollbackTransaction();

                throw;
            }
            finally
            {
                if (transaction == null)
                {
                    connection.Close();
                    command.Dispose();
                }
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                    command.Dispose();
                }
            }
        }
        #endregion

        #region ExecuteScalar
        /// Executes the query, and returns the first column of the first row in the resultset returned by the query. Extra columns or rows are ignored.
		public object ExecuteScalar(string commandText)
        {
            return this.ExecuteScalar(commandText, CommandType.Text);
        }
        /// Executes a stored procedure, and returns the first column of the first row in the resultset returned by the query. Extra columns or rows are ignored.
        public object ExecuteScalar(string commandText, CommandType commandType)
        {
            try
            {
                PrepareCommand(commandType, commandText);

                // execute command
                object objValue = command.ExecuteScalar();
                // check on value
                if (objValue != DBNull.Value)
                    // return value
                    return objValue;
                else
                    // return null instead of dbnull value
                    return null;
            }
            catch
            {
                if (transaction != null)
                    RollbackTransaction();

                throw;
            }
            finally
            {
                if (transaction == null)
                {
                    connection.Close();
                    command.Dispose();
                }
            }
        }
        #endregion


    }

    /// Loads different data access layer provider depending on the configuration settings file or the caller defined data provider type.
    public class DataAccessLayerFactory
    {
        // Since this class provides only static methods, make the default constructor private to prevent 
        // instances from being created with "new DataAccessLayerFactory()"
        private DataAccessLayerFactory() { }

        /// <summary>
        /// Constructs a data access layer data provider.
        /// </summary>
        public static DataAccessLayerBase GetDataAccessLayer(DataProviderType dataProviderType, string connectionString)
        {
            // construct specific data access provider class
            switch (dataProviderType)
            {
                case DataProviderType.Oracle:
                    return new OracleDataAccessLayer(connectionString);

                case DataProviderType.Sql:
                    return new SqlDataAccessLayer(connectionString);

                case DataProviderType.MySql:
                    return new MySqlDataAccessLayer(connectionString);

                default:
                    throw new ArgumentException("Invalid data access layer provider type.");
            }
        }

    }
}