﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NVL.DAO
{
    class Global
    {
        public enum DATATYPE
        {
            NUMBER,
            CHAR,
            VARCHAR,
            NVARCHAR,
            NTEXT,
            BINARY,
            BLOB,
            CLOB,
            NCLOB,
            SMALLINT,
            TIMESTAMP,
            BOOLEAN,
            BIGINT,
            INTEGER,
            TEXT,
            NUMERIC,
            DATE,
            DATETIME,
            REFCURSOR,
            BIT,
            TIME,
        }
    }
}
